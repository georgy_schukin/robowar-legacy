#include "Robot.h"
#include <math.h>

namespace robo
{

/////////////////////// Buffer //////////////////////////////
/*void Buffer::Init(unsigned int sz) // init buf
{
	if(data) delete[] data;
	size = sz;
	data = (void*)(new char[size]);
}

void Buffer::Copy(void* dt, unsigned int sz) // copy data
{
	Init(sz);
	memcpy(data, dt, sz);
}

void Buffer::SetInt(unsigned int pos, int val) // write int val
{
	if(pos*sizeof(int) < size)
	{
		*((int*)data + pos) = val;
	}
}

int Buffer::GetInt(unsigned int pos) // read val
{
	if(pos*sizeof(int) < size)
	{
		return *((int*)data + pos);
	}
return 0;
}*/

/////////////////////// Robot State //////////////////////////
RobotState::RobotState() : 
	memory(0), message(0), shoot(false), transmit(false), hitpoints(0), cooldown(0), move_direction(0), mem_size(0), msg_size(0) {}	

RobotState::RobotState(unsigned int mem_sz, unsigned int msg_sz) : mem_size(mem_sz), msg_size(msg_sz),
	shoot(false), transmit(false), hitpoints(0), cooldown(0), move_direction(0)
{
	memory = (void*)(new char[mem_size]);
	memset(memory, 0, mem_size);
	message = (void*)(new char[msg_size]);
	memset(message, 0, msg_size);
}

RobotState::~RobotState()
{
	if(memory) delete[] memory;
	if(message) delete[] message;

	for(vector<void*>::iterator it = messages.begin();it != messages.end();it++)
	{
		delete[] (*it);
	}
}

void RobotState::AddMessage(const void* src/*, unsigned int msg_sz*/)
{
	void* msg = (void*)(new char[msg_size]);
	memcpy(msg, src, msg_size);
	messages.push_back(msg);
}

void RobotState::CopyToMessage(unsigned int msg/*, unsigned int msg_sz*/) 
{
	if(msg < messages.size()) memcpy(message, messages[msg], msg_size);
}

void RobotState::SetMemoryInt(unsigned int pos, int val)
{
	if(pos*sizeof(int) < mem_size) *((int*)memory + pos) = val;
}

void RobotState::SetMessageInt(unsigned int pos, int val)
{
	if(pos*sizeof(int) < msg_size) *((int*)message + pos) = val;
}

void RobotState::SetMessagesInt(unsigned int msg, unsigned int pos, int val)
{
	if((msg < messages.size()) && (pos*sizeof(int) < msg_size)) *((int*)(messages[msg]) + pos) = val;
}

int RobotState::GetMemoryInt(unsigned int pos) const
{
	return (pos*sizeof(int) < mem_size) ? *((int*)memory + pos) : 0;
}

int RobotState::GetMessageInt(unsigned int pos) const
{
	return (pos*sizeof(int) < msg_size) ? *((int*)message + pos) : 0;
}

int RobotState::GetMessagesInt(unsigned int msg, unsigned int pos) const
{
	return (msg < messages.size()) && (pos*sizeof(int) < msg_size) ? *((int*)(messages[msg]) + pos) : 0;
}

/////////////////////// Environment //////////////////////////
bool Environment::IsOnField(int x, int y) const
{
	return ((x >= 0) && (x < field_width) && (y >= 0) && (y < field_height));
}

bool Environment::Shift(int x, int y, int dir, int& dx, int& dy) const
{
	if(!IsOnField(x, y)) return false;
	switch(dir % 7)
	{
		case 0: dx = x; dy = y; break;
		case 1: dx = x + 1; dy = y; break;
		case 2: dx = x + 1 - y%2; dy = y + 1; break;
		case 3: dx = x - y%2; dy = y + 1; break;
		case 4: dx = x - 1; dy = y; break;
		case 5: dx = x - y%2; dy = y - 1; break;
		case 6: dx = x + 1 - y%2; dy = y-1; break;
		default: return false;
	}
	return IsOnField(dx, dy);
}

float Environment::FDistance(int x1, int y1, int x2, int y2) const
{
	float rx1 = x1 + 0.5f*((y1 + 1)%2);
	float rx2 = x2 + 0.5f*((y2 + 1)%2);
	return sqrt((rx2 - rx1)*(rx2 - rx1) + (y2 - y1)*(y2 - y1));
}

int Environment::GetDirection(int x1, int y1, int x2, int y2) const
{
	if(!IsOnField(x1, y1) || !IsOnField(x2, y2)) return -1;
	int dir = 0;
	float mindist = FDistance(x1, y1, x2, y2);
	for(int i = 1;i <= 6;i++)
	{
		int dx, dy;
		if(Shift(x1, y1, i, dx, dy) && (FDistance(dx, dy, x2, y2) < mindist))
		{
			dir = i;
			mindist = FDistance(dx, dy, x2, y2);
		}
	}
return dir;
}

/*int dist1(int x1, int y1, int x2, int y2)
{
	if(x1<0 || x2<0 || y1<0 || y2<0
			|| x1>=FIELD_WIDTH || x2>=FIELD_WIDTH || y1>=FIELD_HEIGHT || y2>=FIELD_HEIGHT)
		return -1;

	int x=x1, y=y1;
	int d=0;
	while(x!=x2 || y!=y2)
	{
		shift(x, y, get_dir(x, y, x2, y2), x, y);
		d++;
	}
	return d;
}*/

int Environment::Distance(int x1, int y1, int x2, int y2) const
{
	int ax, ay, az, bx, by, bz;
	ax = y1;
	ay = x1 - (y1/2 + y1%2);
	az = x1 + y1/2;
	bx = y2;
	by = x2 - (y2/2 + y2%2);
	bz = x2 + y2/2;

	int v1 = abs(ax - bx) + abs(ay - by);
	int v2 = abs(ay - by) + abs(az - bz);
	int v3 = abs(az - bz) + abs(ax - bx);
	
	return min(v1, min(v2, v3));
}

bool Environment::IsInRange(int x1, int y1, int x2, int y2, int range) // simple check for range
{
	return ((abs(x1 - x2) <= range) && (abs(y1 - y2) <= range));
}

}