#include "IVisualizer.h"
#include <vector>

using namespace std;

namespace robo_vis
{

	MinimapData::MinimapData(int mapWidth, int mapHeight, int minimapWidth, int minimapHeight, int teamAmount/*, const vector<RobotData> &robots, const vector<BaseData> &bases*/)
	{		
		if (minimapWidth > mapWidth)
		{
			minimapWidth = mapWidth;
		}
		if (minimapHeight > mapHeight)
		{
			minimapHeight = mapHeight;
		}

		mini_width = minimapWidth;
		mini_height = minimapHeight;

		map_width = mapWidth;
		map_height = mapHeight;

		points.reserve(minimapWidth);
		points.resize(minimapWidth);
	
		for (vector<vector<MinimapPoint> >::iterator it = points.begin(); it != points.end(); ++it)
		{
			it->reserve(minimapHeight);
			it->resize(minimapHeight);
			for (vector<MinimapPoint>::iterator crowdsIt = it->begin(); crowdsIt != it->end(); ++crowdsIt)
			{				
				crowdsIt->crowds.reserve(teamAmount);
				crowdsIt->crowds.resize(teamAmount, 0);
			}
		}	
		
		/*for (vector<RobotData>::const_iterator it = robots.begin(); it != robots.end(); ++it)
		{
			int xCoord = (it->x*minimapWidth)/mapWidth;
			int yCoord = (it->y*minimapHeight)/mapHeight;
			points[xCoord][yCoord].crowds[it->team]++;
		}

		for (vector<BaseData>::const_iterator it = bases.begin(); it != bases.end(); ++it)
		{
			int xCoord = (it->x*minimapWidth)/mapWidth;
			int yCoord = (it->y*minimapHeight)/mapHeight;
			if (it->team != -1)
			{
				points[xCoord][yCoord].crowds[it->team]++;
			}
		}*/
	}

	void MinimapData::Init(int teamAmount, const vector<RobotData> &robots, const vector<BaseData> &bases)
	{
		for (vector<vector<MinimapPoint> >::iterator it = points.begin(); it != points.end(); ++it)
		{
			for (vector<MinimapPoint>::iterator crowdsIt = it->begin(); crowdsIt != it->end(); ++crowdsIt)
			{	
				for(vector<unsigned int>::iterator it2 = (*crowdsIt).crowds.begin();it2 != (*crowdsIt).crowds.end();it2++)
				{
					(*it2) = 0;
				}
			}
		}	
		for (vector<RobotData>::const_iterator it = robots.begin(); it != robots.end(); ++it)
		{
			int xCoord = it->x;//(it->x*mini_width)/map_width;
			int yCoord = it->y;//(it->y*mini_height)/map_height;
			points[xCoord][yCoord].crowds[it->team]++;
		}

		for (vector<BaseData>::const_iterator it = bases.begin(); it != bases.end(); ++it)
		{
			int xCoord = it->x;//(it->x*mini_width)/map_width;
			int yCoord = it->y;//(it->y*mini_height)/map_height;
			if (it->team != -1)
			{
				points[xCoord][yCoord].crowds[it->team]++;
			}
		}
	}

	MinimapData::MinimapData(void *buf, size_t buf_size)
	{
		if (buf_size < sizeof(size_t)*3)
		{
			throw DeserializationException();
		}
		size_t* sizesPtr = (size_t*)buf;
		size_t width = sizesPtr[0];
		size_t height = sizesPtr[1];
		size_t teamAmount = sizesPtr[2];
		if (buf_size < sizeof(size_t) * 3 + width * height * teamAmount * sizeof(unsigned int))
		{
			throw DeserializationException();
		}
		unsigned int* countPtr = (unsigned int*)(sizesPtr + 3);
		points.resize(width);
		for (size_t i = 0; i < width; ++i)
		{
			points[i].resize(height);
			for (size_t j = 0; j < height; ++j)
			{
				points[i][j].crowds.resize(teamAmount, 0);
				for (size_t k = 0; k < teamAmount; ++k)
				{
					points[i][j].crowds[k] = countPtr[i*height*teamAmount + j*teamAmount + k];
				}
			}
		}
	}

	size_t MinimapData::serialize(void *buf, size_t buf_size) const
	{
		//map is empty or buffer is too small
		size_t requiredSize = requiredSerializationSize();
		if (points.size() == 0 || points[0].size() == 0 || buf_size < requiredSize)
		{
			return 0;
		}
		size_t teamAmount = points[0][0].crowds.size();
		size_t* sizesPtr = (size_t*)buf;
		sizesPtr[0] = width();
		sizesPtr[1] = height();
		sizesPtr[2] = teamAmount;
		unsigned int* countPtr = (unsigned int*)(sizesPtr + 3);
		for (size_t i = 0; i < width(); ++i)
		{
			for (size_t j = 0; j < height(); ++j)
			{
				for (size_t k = 0; k < teamAmount; ++k)
				{
					countPtr[i*height()*teamAmount + j*teamAmount + k] = points[i][j].crowds[k];
				}
			}
		}
		return requiredSize;
	}

	size_t MinimapData::requiredSerializationSize() const
	{
		size_t requiredSize = sizeof(size_t) * 3 + points.size() * points[0].size() * points[0][0].crowds.size() * sizeof(unsigned int);
		return requiredSize;
	}

	MinimapData& MinimapData::merge(const MinimapData& other)
	{
		if (points.size() != 0 && other.points.size() != 0)
		{
			size_t height = this->height() <= other.height() ? this->height() : other.height();
			size_t width = this->width() <= other.width() ? this->width() : other.width();
			for (size_t i = 0; i < width; ++i)
			{
				for (size_t j = 0; j < height; ++j)
				{
					for (size_t k = 0; k < points[i][j].crowds.size(); ++k)
					{
						points[i][j].crowds[k] += other.points[i][j].crowds[k];
					}
				}
			}
		}
		return *this;
	}

	MinimapData& MinimapData::operator+=(const MinimapData& other) 
	{
		return merge(other);
	}

	size_t MinimapData::width() const
	{
		return points.size();
	}

	size_t MinimapData::height() const
	{
		if (points.size() == 0)
		{
			return 0;
		}
		return points[0].size();
	}
	
	ScreenCoordinates::ScreenCoordinates(void *buf, size_t buf_size)
	{
		if (buf_size < sizeof(int)*2)
		{
			throw DeserializationException();
		}
		int *coordPtr = (int *)buf;
		x = coordPtr[0];
		y = coordPtr[1];
	}

	size_t ScreenCoordinates::serialize(void *buf, size_t buf_size) const
	{
		size_t requiredSize = requiredSerializationSize();
		if (buf_size < requiredSize)
		{
			return 0;
		}
		int *coordPtr = (int *)buf;
		coordPtr[0] = x;
		coordPtr[1] = y;
		return requiredSize;
	}

	size_t ScreenCoordinates::requiredSerializationSize() const
	{
		return sizeof(int)*2;
	}
}