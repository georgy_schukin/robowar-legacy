#include "TerrainGen.h"
#include "MyMath.h"

using namespace mymath;

//////////////////// Terrain Generator /////////////////
void TerrainGenerator::Init(int sz_x, int sz_y, float s_x, float s_y, const Point2D& beg)
{
	size_x = sz_x;
	size_y = sz_y;
	step_x = s_x;
	step_y = s_y;
	begin = beg;

	heights.clear();
	heights.insert(heights.begin(), size_x*size_y, 0);	// add heights
}

void TerrainGenerator::GenFault(int iter_num, float max_height)
{
	const float min_height = float(max_height/iter_num);
	//float d = sqrt(size_x*size_x*step_x*step_x + size_y*size_y*step_y*step_y);

	for(int iter = 0;iter < iter_num;iter++) // for each iter
	{		
		const float height = max_height + float(iter/iter_num)*(min_height - max_height);

		const float x1 = begin.x + (float(rand() % 10000)/10000.0f)*step_x*size_x; // generate two random points
		const float x2 = begin.x + (float(rand() % 10000)/10000.0f)*step_x*size_x;

		const float y1 = begin.y - (float(rand() % 10000)/10000.0f)*step_y*size_y;
		const float y2 = begin.y - (float(rand() % 10000)/10000.0f)*step_y*size_y;
	
		const float a = (y2 - y1);
		const float b = -(x2 - x1);
		const float c = -x1*(y2 - y1) + y1*(x2 - x1);

		for(int i = 0;i < size_y;i++) // for each point in terrain
		{
			const float y = begin.y - i*step_y;
			for(int j = 0;j < size_x;j++)
			{
				const float x = begin.x + j*step_x;
				float& h = heights[i*size_x + j];

				if (a*x + b*y - c > 0) 
				{
					h += height; // add height
				}
				else
				{
					h -= height; // substract height
				}
			}
		}
	}
}

void TerrainGenerator::GenFaultCircle(int iter_num, float max_height, float min_rad, float max_rad) // gen terrain with max changes between min_rad and max_rad
{
	const float min_height = float(max_height/iter_num);
	//float d = sqrt(size_x*size_x*step_x*step_x + size_y*size_y*step_y*step_y);

	const float min_r2 = min_rad*min_rad;
	const float max_r2 = max_rad*max_rad;

	float height, x1, x2, y1, y2, a, b, c, x, y, r2, hgt;

	const float coeff = mymath::PI/(max_r2 - min_r2);
	const float coeff_x = 0.0001f*step_x*size_x;
	const float coeff_y = 0.0001f*step_y*size_y;

	for(int iter = 0;iter < iter_num;iter++) // for each iter
	{		
		height = max_height + float(iter/iter_num)*(min_height - max_height);

		x1 = begin.x + float(rand() % 10000)*coeff_x; // generate two random points
		x2 = begin.x + float(rand() % 10000)*coeff_x;

		y1 = begin.y - float(rand() % 10000)*coeff_y;
		y2 = begin.y - float(rand() % 10000)*coeff_y;
	
		a = (y2 - y1);
		b = -(x2 - x1);
		c = -x1*(y2 - y1) + y1*(x2 - x1);

		for(int i = 0;i < size_y;i++) // for each point in terrain
		{
			y = begin.y - i*step_y;
			for(int j = 0;j < size_x;j++)
			{
				x = begin.x + j*step_x;
				float& h = heights[i*size_x + j];

				r2 = (x*x + y*y); // get radius

				if(r2 < min_r2) // we are inside ring
				{
					hgt = height*0.15f;
				}
				else if(r2 > max_r2) // we are outside of ring
				{
					hgt = height*0.15f;
				}
				else // we are in rign
				{
					hgt = height*Sin((r2 - min_r2)*coeff) + height*0.15f; // big change with max in ring center
				}				

				if (a*x + b*y - c > 0) 
				{
					h += hgt; // add height
				}
				else
				{
					h -= hgt; // substract height
				}
			}
		}
	}
}

void TerrainGenerator::Normalize() // lower or upper terrain to ground level
{
	float h_min = 1000000.0f;
	for(vector<float>::const_iterator it = heights.begin();it != heights.end();it++)
	{
		if((*it) < h_min) h_min = (*it);
	}
	for(vector<float>::iterator it = heights.begin();it != heights.end();it++) // move up or down
	{
		(*it) -= h_min;
	}	
}

void TerrainGenerator::Inverse() // lower or upper terrain to ground level
{	
	for(vector<float>::iterator it = heights.begin();it != heights.end();it++) // move up or down
	{
		(*it) = fabs(*it);
	}	
}

void TerrainGenerator::Move(float h) // move height by h
{	
	for(vector<float>::iterator it = heights.begin();it != heights.end();it++) // move up or down
	{
		(*it) += h;
	}	
}

void TerrainGenerator::Smooth(float coeff)
{	
	const float c2 = (1.0f - coeff)/8.0f;

	for(int i = 1;i < size_y - 1;i++)
	for(int j = 1;j < size_x - 1;j++)
	{
		heights[i*size_x + j] = coeff*heights[i*size_x + j] + c2*(heights[(i - 1)*size_x + j] + heights[(i + 1)*size_x + j] + heights[i*size_x + j - 1] +
			heights[i*size_x + j + 1] + heights[(i - 1)*size_x + j - 1] + heights[(i - 1)*size_x + j + 1] + 
			heights[(i + 1)*size_x + j - 1] + heights[(i + 1)*size_x + j + 1]);
	}
}
