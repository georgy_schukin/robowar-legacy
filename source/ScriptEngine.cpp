#include "ScriptEngine.h"
#include "scriptarray.h"
#include "scriptstdstring.h"
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <vector>
#include <string>

////////////////////////// Script Engine //////////////////////////
void print_int(int n)
{
	printf("%d", n);
};

void print(std::string &msg)
{
	printf("%s", msg.c_str());
}

void ScriptEngine::MessageCallback(const asSMessageInfo* msg)
{
	cerr << "[" << msg->section << "]: " << msg->message 
		<< " (row: " << msg->row << ", col: " << msg->col << ")" << endl;
}

int ScriptEngine::Init() // init engine
{
	engine = asCreateScriptEngine(ANGELSCRIPT_VERSION);
	if(!engine) 
	{
		cerr << "Error: failed to create engine" << endl;
		return -1;
	}

	engine->SetEngineProperty(asEP_ALLOW_UNSAFE_REFERENCES, 1);

	engine->SetMessageCallback(asMETHOD(ScriptEngine, MessageCallback), this, asCALL_THISCALL); // callback for msg display

	RegisterStdString(engine);
	RegisterScriptArray(engine, true);

	// register global functions for scripts
	engine->RegisterGlobalFunction("void print(int n)", asFUNCTION(print_int), asCALL_CDECL);
	engine->RegisterGlobalFunction("void print(const string &in)", asFUNCTION(print), asCALL_CDECL);
	engine->RegisterGlobalFunction("int rand()", asFUNCTION(rand), asCALL_CDECL); 
	
	// register Unit type
	engine->RegisterObjectType("Unit", sizeof(Unit), asOBJ_VALUE | asOBJ_APP_CLASS | asOBJ_POD | asOBJ_APP_CLASS_COPY_CONSTRUCTOR);
	//engine->RegisterObjectType("Unit", 0,  asOBJ_REF); 

	engine->RegisterObjectProperty("Unit", "int x", offsetof(Unit, x));
	engine->RegisterObjectProperty("Unit", "int y", offsetof(Unit, y));
	engine->RegisterObjectProperty("Unit", "int team", offsetof(Unit, team));
	engine->RegisterObjectProperty("Unit", "int dist", offsetof(Unit, dist));

	//engine->RegisterObjectBehaviour("Unit", asBEHAVE_ADDREF, "void f()", asMETHOD(Unit, AddRef), asCALL_THISCALL); 
	//engine->RegisterObjectBehaviour("Unit", asBEHAVE_RELEASE, "void f()", asMETHOD(Unit, Release), asCALL_THISCALL);

	// register RobotState type
	engine->RegisterObjectType("RobotState", 0,  asOBJ_REF); 
	//engine->RegisterObjectType("RobotState", sizeof(RobotState), asOBJ_VALUE | asOBJ_APP_CLASS | asOBJ_POD);

	engine->RegisterObjectProperty("RobotState", "const int x", offsetof(RobotState, x));
	engine->RegisterObjectProperty("RobotState", "const int y", offsetof(RobotState, y));
	engine->RegisterObjectProperty("RobotState", "const int team", offsetof(RobotState, team));
	engine->RegisterObjectProperty("RobotState", "const int hitpoint", offsetof(RobotState, hitpoints));
	engine->RegisterObjectProperty("RobotState", "const int cooldown", offsetof(RobotState, cooldown));
	engine->RegisterObjectProperty("RobotState", "bool shoot", offsetof(RobotState, shoot));
	engine->RegisterObjectProperty("RobotState", "bool transmit", offsetof(RobotState, transmit));
	engine->RegisterObjectProperty("RobotState", "int move_direction", offsetof(RobotState, move_direction));
	engine->RegisterObjectProperty("RobotState", "int shoot_x", offsetof(RobotState, shoot_x));
	engine->RegisterObjectProperty("RobotState", "int shoot_y", offsetof(RobotState, shoot_y));

	engine->RegisterObjectMethod("RobotState", "uint messages_num()", asMETHOD(RobotState, GetMessagesNum), asCALL_THISCALL);
	engine->RegisterObjectMethod("RobotState", "uint robots_num()", asMETHOD(RobotState, GetRobotsNum), asCALL_THISCALL);
	engine->RegisterObjectMethod("RobotState", "uint bases_num()", asMETHOD(RobotState, GetBasesNum), asCALL_THISCALL);
	engine->RegisterObjectMethod("RobotState", "void set_mem_int(uint, int)", asMETHOD(RobotState, SetMemoryInt), asCALL_THISCALL);
	engine->RegisterObjectMethod("RobotState", "void set_msg_int(uint, int)", asMETHOD(RobotState, SetMessageInt), asCALL_THISCALL);
	engine->RegisterObjectMethod("RobotState", "void set_msgs_int(uint, uint, int)", asMETHOD(RobotState, SetMessagesInt), asCALL_THISCALL);
	engine->RegisterObjectMethod("RobotState", "int get_mem_int(uint)", asMETHOD(RobotState, GetMemoryInt), asCALL_THISCALL);
	engine->RegisterObjectMethod("RobotState", "int get_msg_int(uint)", asMETHOD(RobotState, GetMessageInt), asCALL_THISCALL);
	engine->RegisterObjectMethod("RobotState", "int get_msgs_int(uint, uint)", asMETHOD(RobotState, GetMessagesInt), asCALL_THISCALL);
	engine->RegisterObjectMethod("RobotState", "Unit robot(uint)", asMETHOD(RobotState, GetRobot), asCALL_THISCALL);
	engine->RegisterObjectMethod("RobotState", "Unit base(uint)", asMETHOD(RobotState, GetBase), asCALL_THISCALL);
	engine->RegisterObjectMethod("RobotState", "void copy_to_msg(uint)", asMETHOD(RobotState, CopyToMessage), asCALL_THISCALL);

	engine->RegisterObjectBehaviour("RobotState", asBEHAVE_ADDREF, "void f()", asMETHOD(RobotState, AddRef), asCALL_THISCALL); 
	engine->RegisterObjectBehaviour("RobotState", asBEHAVE_RELEASE, "void f()", asMETHOD(RobotState, Release), asCALL_THISCALL);
	
	// register Environment type
	engine->RegisterObjectType("Environment", 0,  asOBJ_REF); 
	//engine->RegisterObjectType("Environment", sizeof(Environment), asOBJ_VALUE | asOBJ_APP_CLASS | asOBJ_POD);

	engine->RegisterObjectProperty("Environment", "int bases_count", offsetof(Environment, bases_count));
	engine->RegisterObjectProperty("Environment", "int field_width", offsetof(Environment, field_width));
	engine->RegisterObjectProperty("Environment", "int field_height", offsetof(Environment, field_height));
	engine->RegisterObjectProperty("Environment", "int msg_size", offsetof(Environment, msg_size));
	engine->RegisterObjectProperty("Environment", "int memory_size", offsetof(Environment, memory_size));
	engine->RegisterObjectProperty("Environment", "int shoot_range", offsetof(Environment, shoot_range));
	engine->RegisterObjectProperty("Environment", "int robot_hp", offsetof(Environment, robot_hp));
	engine->RegisterObjectProperty("Environment", "int base_hp", offsetof(Environment, base_hp));
	engine->RegisterObjectProperty("Environment", "int reload_time", offsetof(Environment, reload_time));
	engine->RegisterObjectProperty("Environment", "int view_range", offsetof(Environment, view_range));
	engine->RegisterObjectProperty("Environment", "int transmit_range", offsetof(Environment, transmit_range));
	engine->RegisterObjectProperty("Environment", "int robot_construction_time", offsetof(Environment, robot_construction_time));
	engine->RegisterObjectProperty("Environment", "int last_iter", offsetof(Environment, last_iter));
	engine->RegisterObjectProperty("Environment", "int iter", offsetof(Environment, iter));
	engine->RegisterObjectProperty("Environment", "int timeout", offsetof(Environment, timeout));

	engine->RegisterObjectMethod("Environment", "bool shift(int, int, int, int&, int&) const", asMETHOD(Environment, Shift), asCALL_THISCALL);
	engine->RegisterObjectMethod("Environment", "int get_dir(int, int, int, int) const", asMETHOD(Environment, GetDirection), asCALL_THISCALL);
	engine->RegisterObjectMethod("Environment", "int dist(int, int, int, int) const", asMETHOD(Environment, Distance), asCALL_THISCALL);	

	engine->RegisterObjectBehaviour("Environment", asBEHAVE_ADDREF, "void f()", asMETHOD(Environment, AddRef), asCALL_THISCALL); 
	engine->RegisterObjectBehaviour("Environment", asBEHAVE_RELEASE, "void f()", asMETHOD(Environment, Release), asCALL_THISCALL);	
return 0;
}

int ScriptEngine::Free() 
{
	for(map<string, ScriptInfo>::iterator it = scripts.begin();it != scripts.end();it++)
	{
		if(it->second.module) engine->DiscardModule(it->first.c_str()); // release module
		if(it->second.context) it->second.context->Release(); // relese script context
	}
	scripts.clear();

	engine->Release(); // release virtual machine
	engine = 0;

return 0;
}

int ScriptEngine::ReadFile(const string& file, string& res) // load script from file
{
	ifstream f_in(file.c_str(), ios::binary | ios::in);
	if(!f_in.is_open()) return -1;

	f_in.seekg(0, ios::end);
	int length = f_in.tellg();
	f_in.seekg(0, ios::beg);

	char* buf = new char[length];

	f_in.read(buf, length);  
	f_in.close();

	res.clear();
	res.append(buf, length);
	delete[] buf;
return 0;
}

bool ScriptEngine::Exists(const string& name)
{
	return (scripts.find(name) != scripts.end());
}

int ScriptEngine::LoadScript(const string& name, const string& path) // load script 
{
	if(Exists(name))
	{
		cout << "Error: script " << name << " already exists" << endl;
		return -1;
	}

	string file = path + name + string(".as");
	string script;

	if(ReadFile(file, script) == -1)
	{
		cerr << "Error: failed to load script " << name << " from file " << file << endl;
		return -1;
	}	
	if(!(scripts[name].module = engine->GetModule(name.c_str(), asGM_ALWAYS_CREATE))) // get module
	{
		cerr << "Error: failed to create module for script " << name << endl;
		return -1;
	}
	scripts[name].module->SetName(name.c_str());
	if(scripts[name].module->AddScriptSection(name.c_str(), script.c_str(), script.size(), 0) == -1)	
	{
		cerr << "Error: failed to add script " << name << endl;
		engine->DiscardModule(name.c_str());
		scripts.erase(name);
		return -1;
	}
return 0;
}

int ScriptEngine::CompileScript(const string& name) // compile script
{	
	if(!Exists(name))
	{
		cerr << "Error: script " << name << " doesn't exist" << endl;
		return -1;
	}
	ScriptInfo& s_info = scripts[name];	
	if(s_info.module->Build() == -1)
	{
		cerr << "Error: failed to compile script " << name << endl;
		return -1;
	}
	if(!(s_info.context = engine->CreateContext()))
	{
		cerr << "Error: failed to create context for script " << name << endl;
		return -1;
	}	
	if((s_info.func_id = s_info.module->GetFunctionIdByDecl("void Execute(RobotState&, const Environment&)")) < 0) // find "Execute" function in robot's script
	{
		cerr << "Error: failed to find 'void Execute(RobotState&, const Environment&)' function for " << name << endl;
		return -1;
	}
return 0;
}

int ScriptEngine::ExecScript(const string& name, RobotState* rs, Environment* env) // execute script for robot
{	
	//int ret;
	ScriptInfo& s_info = scripts[name];	
	s_info.context->Prepare(s_info.func_id);
	s_info.context->SetArgObject(0, (void*)rs); // add argument
	s_info.context->SetArgObject(1, (void*)env); // add argument
	s_info.context->Execute(); // execute script
/*if( ret == asEXECUTION_FINISHED )
{
  // The return value is only valid if the execution finished successfully
  ret = s_info.context->GetReturnDWord();
}*/
return 0;
}

int ScriptEngine::DeleteScript(const string& name)
{
	if(Exists(name))
	{
		ScriptInfo& s_info = scripts[name];
		if(s_info.module) engine->DiscardModule(name.c_str()); // release module
		if(s_info.context) s_info.context->Release(); // relese script context
		scripts.erase(name);
		return 0;
	}
return -1;
}
