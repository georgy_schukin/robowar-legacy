#include "NoiseGen.h"
#include <math.h>

void NoiseGenerator::Init(int sz_x, int sz_y)
{
	size_x = sz_x;
	size_y = sz_y;

	noise.clear();

	noise.reserve(size_x*size_y);
	noise.resize(size_x*size_y);
}

/*float NoiseGenerator::Noise(int x, int y) // gen random noise
{
    const int nn = x + y*57;		
	const int n = (nn << 13)^nn;
return (1.0f - float((n * (n*n*15731 + 789221) + 1376312589) & 0x7fffffff)/1073741824.0f);
}*/

float NoiseGenerator::Interpolate(float a, float b, float x) // cosine interpolation
{	
	const float f = (1 - cos(x*3.1415927f))*0.5f;
return a*(1 - f) + b*f;
//	return a*(1 - x) + b*x;
}
 
/*float NoiseGenerator::SmoothedNoise(int x, int y)
{
    const float corners = (Noise(x - 1, y - 1) + Noise(x + 1, y - 1) + Noise(x - 1, y + 1) + Noise(x + 1, y + 1))/16.0f;
    const float sides = (Noise(x - 1, y) + Noise(x + 1, y) + Noise(x, y - 1) + Noise(x, y + 1))/8.0f;
    const float center = Noise(x, y)/4.0f;	
return corners + sides + center;
}

float NoiseGenerator::InterpolatedNoise(float x, float y)
{	
	const float v1 = SmoothedNoise(int(x), int(y));
	const float v2 = SmoothedNoise(int(x) + 1, int(y));
	const float v3 = SmoothedNoise(int(x), int(y) + 1);
	const float v4 = SmoothedNoise(int(x) + 1, int(y) + 1);

	const float i1 = Interpolate(v1, v2, x - int(x));
	const float i2 = Interpolate(v3, v4, x - int(x));
return Interpolate(i1, i2, y - int(y));
}*/

void NoiseGenerator::SmoothNoise(int oct, const vector<float>& base, vector<float>& res) // generate smooth noise for octave
{
   const int period = 1 << oct; // calculates 2 ^ k
   const float freq = 1.0f/period;

   //res.reserve(size_x*size_y);
   //res.resize(size_x*size_y);

   int i0, i1, j0, j1;
   float hor, vert;
   float top, bottom;
 
   for(int i = 0;i < size_y;i++)
   {
      //calculate the horizontal sampling indices
      i0 = int(i/period)*period;
      i1 = (i0 + period) % size_y; //wrap around
      hor = (i - i0)*freq;
 
      for(int j = 0;j < size_x;j++)
      {
         //calculate the vertical sampling indices
         j0 = int(j/period)*period;
         j1 = (j0 + period) % size_x; //wrap around
         vert = (j - j0)*freq;
 
         //blend the top two corners
         top = Interpolate(base[i0*size_x + j0], base[i1*size_x + j0], hor);
 
         //blend the bottom two corners
         bottom = Interpolate(base[i0*size_x + j1], base[i1*size_x + j1], hor);
 
         //final blend
         res[i*size_x + j] = Interpolate(top, bottom, vert);
      }
   }
}

void NoiseGenerator::GenPerlin(int oct_num, float persistence, bool circle)
{
	vector<float> base_noise(size_x*size_y);

	const int size = size_x*size_y;

	if(!circle)
	{
		for(int i = 0;i < size;i++) // gen random noise
		{
			base_noise[i] = float(rand() % 10000)*0.0001f; // from 0 to 1
		}
	}
	else // gen circle chaped noise
	{		
		const int r2_max = size_x*size_x/4; // max rad
		//const float coeff = 0.0001f/r2_max;
		int x, y, r2;
		for(int i = 0;i < size_y;i++) // gen random circle noise
		for(int j = 0;j < size_x;j++) // gen random circle noise
		{
			x = size_x/2 - j;
			y = size_y/2 - i;
			r2 = x*x + y*y; // dist to center
			base_noise[i*size_x + j] = 
				(r2 <= r2_max) ? float(rand() % 10000)*0.0001f : 0.0f;
		}
	}

	for(int i = 0;i < size;i++) // init noise
	{
		noise[i] = 0.0f;
	}

    float amplitude = 1.0f;
    float total_amp = 0.0f;

	vector<float> smooth;
	smooth.reserve(size);
	smooth.resize(size);
 
    for(int oct = oct_num - 1;oct >= 0;oct--) // for each octave
    {
		amplitude *= persistence;
		total_amp += amplitude;
		
		SmoothNoise(oct, base_noise, smooth); // gen smooth noise for octave
 
		for(int i = 0;i < size;i++) // add octave to final res
		{          
			noise[i] += smooth[i]*amplitude;
		}
	}

	const float coeff = 1.0f/total_amp;

	for(int i = 0;i < size;i++) // normalize
	{      
         noise[i] *= coeff; 
	}
}
