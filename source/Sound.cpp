#include "Sound.h"

#include <stdlib.h>
#include <time.h>

#include <windows.h>
#include <Vfw.h>

#include <strstream>

namespace sound_engine
{

string SoundEngine::IntToStr(int num)
{
	char tmp[50];
	sprintf(tmp, "%d", num);
return string(tmp);
}

string SoundEngine::GetSoundFile(const string& base_str, const string& pref_str, int suffix) // get sound file
{
	int rnd_num = (rand() % suffix) + 1;	
return pref_str + base_str + IntToStr(rnd_num) + string(".wav");	
}

void SoundEngine::Play(const string& file, unsigned int time_to_live)
{
	if(is_enabled)
	{
		string filename = prefix + file;

		//int rnd_num = rand() % 10000;

		string alias = file + IntToStr(rand());

		string open_str = string("open \"") + filename + string("\" type mpegvideo alias ") + alias + string(" shareable");
		string play_str = string("play ") + alias + string(" from 0");
		
		mciSendString(open_str.c_str(), NULL, 0, 0);
		mciSendString(play_str.c_str(), NULL, 0, 0);
		
		curr_sounds.push_back(SoundInfo(alias, time(NULL), (time_t)time_to_live)); // add to curr_sounds		
	}
}

void SoundEngine::Clear(bool all) // close some or all sounds
{	
	string stop_s, close_s;

	if(all)
	{	
		if(!curr_sounds.empty())
		for(list<SoundInfo>::iterator it = curr_sounds.begin();it != curr_sounds.end();it++) // close all sounds
		{
			stop_s = string("stop ") + (*it).alias;
			close_s = string("close ") + (*it).alias;			
			mciSendString(stop_s.c_str(), NULL, 0, 0);
			mciSendString(close_s.c_str(), NULL, 0, 0);			
		}
		curr_sounds.clear();
	}
	else
	{
		time_t t = time(NULL);
		for(list<SoundInfo>::iterator it = curr_sounds.begin();it != curr_sounds.end();) // close sounds with zero iter
		{			
			if((*it).start_time + (*it).time_to_live <= t) // time is out
			{
				stop_s = string("stop ") + (*it).alias;
				close_s = string("close ") + (*it).alias;			
				mciSendString(stop_s.c_str(), NULL, 0, 0);
				mciSendString(close_s.c_str(), NULL, 0, 0);
				std::list<SoundInfo>::iterator it2 = it++;				
				curr_sounds.erase(it2); // delete from list
			}				
			else it++;
		}
	}
}

}