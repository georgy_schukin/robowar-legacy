#include "Animation.h"

namespace animation
{

void ProcessBulletAnimationList(list<BulletAnimation*>& lst)
{
	ClearAnimationList<BulletAnimation>(lst);
	for(list<BulletAnimation*>::iterator it = lst.begin();it != lst.end();it++)
	{
		BulletAnimation& b = *(*it);
		if(b.curr_frame >= b.start_frame)
		{
			b.curr_pos.x += b.move.x;
			b.curr_pos.y += b.move.y;
			b.curr_pos.z += b.move.z;
		}
		b.curr_frame++;
	}
}

void ProcessMoveAnimationList(list<MoveAnimation*>& lst)
{
	ClearAnimationList<MoveAnimation>(lst);
	for(list<MoveAnimation*>::iterator it = lst.begin();it != lst.end();it++)
	{
		MoveAnimation& m = *(*it);
		if(m.curr_frame >= m.start_frame)
		{
			m.curr_pos.x += m.move.x;
			m.curr_pos.y += m.move.y;
			m.curr_pos.z += m.move.z;
		}
		m.curr_frame++;
	}
}

void ProcessHitAnimationList(list<HitAnimation*>& lst)
{
	ClearAnimationList<HitAnimation>(lst);
	for(list<HitAnimation*>::iterator it = lst.begin();it != lst.end();it++)
	{
		HitAnimation& h = *(*it);
		if(h.curr_frame >= h.start_frame)
		{
			for(unsigned int i = 0;i < h.particles.size();i++)
			{
				h.particles[i].x += h.p_moves[i].x;
				h.particles[i].y += h.p_moves[i].y;
				h.particles[i].z += h.p_moves[i].z;
				h.p_rads[i] *= 0.8f; // decrease radius
			}
		}
		h.curr_frame++;
	}
}

void ProcessParticleAnimationList(list<ParticleAnimation*>& lst)
{
	ClearAnimationList<ParticleAnimation>(lst);
	for(list<ParticleAnimation*>::iterator it = lst.begin();it != lst.end();it++)
	{
		ParticleAnimation& p = *(*it);
		//if(p.curr_frame >= p.start_frame) // always start with 0 - may skip check
		{
			p.curr_pos.x += p.move.x;
			p.curr_pos.y += p.move.y;
			p.curr_pos.z += p.move.z;
			p.size += p.size_dec;
			p.transparency += p.trnp_dec;
		}
		p.curr_frame++;
	}
}

void HitAnimation::Generate(const Point3D& pos, int p_num, float max_rad, float max_len)
{
	for(int i = 0;i < p_num;i++)
	{
		const float l = max_len*(1 - (float(rand()%4)/8.0f))/float(end_frame - start_frame);
		const float teta = (float(rand() % p_num)/float(p_num))*PI;
		const float phi = (float(i)/float(p_num))*2*PI;

		particles.push_back(pos);
		p_moves.push_back(Point3D(l*Sin(teta)*Cos(phi), l*Sin(teta)*Sin(phi), l*Cos(teta)));
		p_rads.push_back(max_rad*float(rand() % 5)*0.2f);
	}
}

}
