#include "Emulator.h"
#include "OpenGLVisualizer.h"

#include <math.h>
#include <time.h>
#include <iostream>
#include <fstream>

using namespace emulator;

//////////////////////////// Global vars for compatibility ///////////////////////////////////
int BASES_COUNT;
int FIELD_WIDTH; // cells
int FIELD_HEIGHT; // cells
int MSG_SIZE; //bytes
int MEMORY_SIZE; // bytes
int SHOOT_RANGE; // cells
int ROBOT_HP; // shoots
int BASE_HP; // shoots
int RELOAD_TIME; // ticks
int VIEW_RANGE; // cells
int TRANSMIT_RANGE; // cells
int ROBOT_CONSTRUCTION_TIME; // ticks
int LAST_ITER;
int ITER;
int TIMEOUT;

Emulator emu; // emulator

bool shift(int x, int y, int dir, int& dx, int& dy)
{
	return emu.Env().Shift(x, y, dir, dx, dy);
}

int get_dir(int x1, int y1, int x2, int y2)
{
	return emu.Env().GetDirection(x1, y1, x2, y2);
}

int dist(int x1, int y1, int x2, int y2)
{
	return emu.Env().Distance(x1,y1,x2,y2);
}

void InitGlobalVars(const Environment& env) // init global vars with environemnt
{
	BASES_COUNT				= env.bases_count;
	FIELD_WIDTH				= env.field_width; // cells
	FIELD_HEIGHT			= env.field_height; // cells
	MSG_SIZE				= env.msg_size; //bytes
	MEMORY_SIZE				= env.memory_size; // bytes
	SHOOT_RANGE				= env.shoot_range; // cells
	ROBOT_HP				= env.robot_hp; // shoots
	BASE_HP					= env.base_hp; // shoots
	RELOAD_TIME				= env.reload_time; // ticks
	VIEW_RANGE				= env.view_range; // cells
	TRANSMIT_RANGE			= env.transmit_range; // cells
	ROBOT_CONSTRUCTION_TIME = env.robot_construction_time; // ticks
	LAST_ITER				= env.last_iter;
	ITER					= env.iter;
	TIMEOUT					= env.timeout;
}

namespace emulator
{

///////////////////////// Emulator /////////////////////////////
Emulator::Emulator() : field(0), move_request(0), visualizer(0), last_robot_id(0), last_base_id(0)
{
	script_engine.Init();

	RegisterBots(); // register built-in c++ bots
}

Emulator::~Emulator()
{
	Clear();
	script_engine.Free();
}

void Emulator::Clear() // clear emu data
{
	for(set<XUnit*>::iterator it = robots.begin();it != robots.end();it++) delete (*it); // delete robots
	for(set<XUnit*>::iterator it = bases.begin();it != bases.end();it++) delete (*it); // delete bases

	if(field) delete[] field; field = 0; // delete field
	if(move_request) delete[] move_request; move_request = 0;

	robots.clear();
	bases.clear();

	robot_data.clear();
	base_data.clear();
}

void Emulator::UpdateRobotData(const set<XUnit*>& dt, int stage) // update data for robots
{
	for(set<XUnit*>::const_iterator it = dt.begin();it != dt.end();it++)
	{
		if (!(*it)->base && (*it)->state)
		{
			XUnit& robot = **it;
			RobotData& data = robot_data[robot.id];			
			if(stage == 1)
			{
				data.cooldown = robot.cooldown;
				data.hitpoints = robot.hitpoints;
				data.team = robot.team;
				data.x = robot.x;
				data.y = robot.y;
			}
			else if(stage == 2)
			{
				data.move_direction = robot.state->move_direction;
				data.shoot = robot.state->shoot;
				data.shoot_x = robot.state->shoot_x;
				data.shoot_y = robot.state->shoot_y;
			}			
		}
	}
}

void Emulator::UpdateBaseData(const set<XUnit*>& dt, int stage)
{
	for(set<XUnit*>::const_iterator it = dt.begin();it != dt.end();it++)
	{
		if ((*it)->base)
		{
			XUnit& base = **it;
			BaseData& data = base_data[base.id];

			data.cooldown = base.cooldown;
			data.hitpoints = base.hitpoints;
			data.team = base.team;
			data.x = base.x;
			data.y = base.y;			
		}
	}	
}

void Emulator::GetRobotData(vector<RobotData>& res)
{	
	res.resize(robot_data.size());
	int i = 0;
	for(map<unsigned int, RobotData>::const_iterator it = robot_data.begin();it != robot_data.end();it++, i++) 
		res[i] = it->second;	
}

void Emulator::GetBaseData(vector<BaseData>& res)
{	
	res.resize(base_data.size());
	int i = 0;
	for(map<unsigned int, BaseData>::const_iterator it = base_data.begin();it != base_data.end();it++, i++) 
		res[i] = it->second;
}

bool Emulator::Finished()
{
   if(bases.empty()) return true;

   for(set<XUnit*>::iterator it = bases.begin();it != bases.end();it++)
      death_time[(*it)->team] = Env().iter;

   // exit if all bases belong to the same team
   int pc = -1;
   for(set<XUnit*>::iterator it = bases.begin();it != bases.end();it++)
      if((*it)->team != -1)
         pc = (**it).team;

   for(set<XUnit*>::iterator it = bases.begin();it != bases.end();it++)
      if(((*it)->team != -1) && (*it)->team != pc) return false;

   // exit if all bases are neutral or belong to the same team and no enemy robots are present
   int wt=-1;
   bool only=true;
   for(set<XUnit*>::iterator it = bases.begin();it != bases.end();it++)
   {
      if((**it).team != -1)
      {
         if(wt == -1)
            wt = (**it).team;
         else if((**it).team != wt)
            only = false;
      }
   }
   if(wt!=-1 && only)
   {
      for(set<XUnit*>::iterator it = robots.begin(); it != robots.end(); it++)
         if((**it).team!=wt)
            return false;
   }
   return true;
}

bool Emulator::SpawnRobot(int x, int y, int team) // spawn robot near base
{	
	for(int i = 1; i <= 6; i++) // try to spawn in any direction
	{
		int dx, dy;
		if(environment.Shift(x, y, i, dx, dy) && !field[dy*environment.field_width + dx])
		{
			XUnit *r = new XUnit();
				r->id = last_robot_id++;
				r->base = false;
				r->x = dx;
				r->y = dy;
				r->team = team;
				r->hitpoints = environment.robot_hp;
				r->state = new RobotState(environment.memory_size, environment.msg_size);
			robots.insert(r);

			field[dy*environment.field_width + dx] = r;
			return true;
		}
	}
return false;
}

void Emulator::IterateRobotSpawn() // do actions for bases
{
	for(set<XUnit*>::iterator it = bases.begin();it != bases.end();it++)
	{
		XUnit &b = **it;		
		if(b.team != -1)
		{
			if(b.cooldown > 0) // decrease cooldown
			{
				b.cooldown--;
			}
			else // cooldown is zero
			{
				SpawnRobot(b.x, b.y, b.team); // spawn new robot if possible
				b.cooldown = environment.robot_construction_time - 1; // set new cooldown in any case
			}
		}
	}
}

void Emulator::IterateRobotView() // init robot view
{
	const int width = environment.field_width;	
	const int v_range = environment.view_range;	

	for(set<XUnit*>::iterator it = robots.begin();it != robots.end();it++) // clear prev data
	{		
		RobotState& s = *(*it)->state;
		s.bases.clear();
		s.robots.clear();
	}

	int dist;
	
	for(set<XUnit*>::iterator it = robots.begin();it != robots.end();it++) // for each robot
	{
		XUnit &r1 = **it;
		for(int y = r1.y - v_range;y <= r1.y + v_range;y++) // search anything in view range
		{
			int i = abs(r1.y - y);
			for(int x = r1.x - v_range + i/2 + i%2;x <= r1.x + v_range - i/2;x++)
			{
				if(environment.IsOnField(x, y) && !((x == r1.x) && (y == r1.y))
					&& field[y*width + x]) // find robot or base
				{
					XUnit &r2 = *field[y*width + x];

					dist = environment.Distance(r1.x, r1.y, r2.x, r2.y); // dist between 

					if(dist <= v_range) // in view range
					{
						if(r2.base) // it's base
						{						
							r1.state->bases.push_back(Unit(r2.x, r2.y, r2.team, dist)); // add base to view
						}					
						else
						{							
							r1.state->robots.push_back(Unit(r2.x, r2.y, r2.team, dist)); // add robot to view						
						}
					}					
				}
			}
		}
	}
	/*for(set<XUnit*>::iterator it = robots.begin();it != robots.end();it++) // for each robot
	{
		XUnit& r = **it;
		RobotState& s = *r.state;

		for(set<XUnit*>::const_iterator it2 = bases.begin();it2 != bases.end();it2++) // for each base
		{
			XUnit& b = **it2;
			if(environment.Distance(r.x, r.y, b.x, b.y) <= v_range) // add visible bases
				s.bases.push_back(Unit(b.x, b.y, b.team));
		}		
		for(set<XUnit*>::const_iterator it2 = it;++it2 != robots.end();) // for other robots
		{
			XUnit &r2 = **it2;

			//if(!environment.IsInRange(r.x, r.y, r2.x, r2.y, max_range)) continue; // simple check for range

			dist = environment.Distance(r.x, r.y, r2.x, r2.y); // dist between robots

			if(dist <= v_range) // robots are in view range
			{
				r.state->robots.push_back(Unit(r2.x, r2.y, r2.team)); // add to view			
				r2.state->robots.push_back(Unit(r.x, r.y, r.team));
			}

			if((dist <= t_range) && (r.team == r2.team)) // robots are in transmit range and in common team
			{
				if(r.state->transmit) // is transmitting
				{
					r2.state->AddMessage(r.state->message, m_size);
				}
				if(r2.state->transmit)
				{
					r.state->AddMessage(r2.state->message, m_size);
				}
			}
		}
	}*/
}

void Emulator::IterateRobotActions() // call actions for all robots
{
	const int width = environment.field_width;	
	const int s_range = environment.shoot_range;
	const int r_time = environment.reload_time;

	for(set<XUnit*>::iterator it = bases.begin();it != bases.end();it++) // clear base info
	{
		(**it).damage = 0;
		(**it).attacker = -1;
		(**it).attacker_dirty = false;
	}

	for(set<XUnit*>::iterator it = robots.begin();it != robots.end();it++) // prepare robot states
	{
		XUnit &r = **it;
		RobotState &s = *r.state;

		r.damage = 0;	
		s.x = r.x; s.y = r.y; s.team = r.team; s.hitpoints = r.hitpoints; s.cooldown = r.cooldown; 
		s.shoot = false; 
		s.transmit = false; 
		s.move_direction = 0;		
	}

	for(set<XUnit*>::iterator it = robots.begin();it != robots.end();it++)
	{
		XUnit &r = **it;
		RobotState &s = *r.state;

		if(actions[r.team]) // there is action for bot
		{
			actions[r.team](*r.state); // call func
		}
		else
		{
			script_engine.ExecScript(scripts[r.team], r.state, &environment); // exec script for robot
		}			

		if(r.cooldown > 0) s.shoot = false; // cooldown - can't shoot
		if(s.shoot) // robot is shooting
		{
			r.cooldown = r_time - 1; // set cooldown
			if(environment.IsOnField(s.shoot_x, s.shoot_y) && 
				!((s.shoot_x == r.x) && (s.shoot_y == r.y)) &&
				field[s.shoot_y*width + s.shoot_x] && 
				(environment.Distance(r.x, r.y, s.shoot_x, s.shoot_y) <= s_range)) // there is something to hit
			{ 
				XUnit &target = *field[s.shoot_y*width + s.shoot_x];
				if(target.team != r.team) // something from another team
				{
					target.damage += 1; // set damage
					if(target.base) // is base
					{
						if((target.attacker == -1) || (target.attacker == r.team)) // base attacked by one team
						{
							target.attacker = r.team;
						}
						else // different teams
						{
							target.attacker_dirty = true;
						}
					}
				}
			}
			else
			{
				s.shoot = false; // invalid target - can't shoot
			}
		}
		else if(r.cooldown > 0) r.cooldown--; // decrease cooldown
	}
}

void Emulator::IterateRobotDamage() // count damage to tobots
{
	const int width = environment.field_width;
	
	for(set<XUnit*>::iterator it = robots.begin();it != robots.end();it++) // for each robot
	{
		XUnit& r = **it;
		RobotState& s = *r.state;
		if(r.hitpoints <= r.damage) // robot is killed
		{ 				
			r.hitpoints = 0; // make corpse
			s.hitpoints = 0; 
			s.shoot = false;
			s.transmit = false;
			s.move_direction = 0;

			field[r.y*width + r.x] = 0; // get robot from field
		}
		else
		{
			r.hitpoints -= r.damage; // decrease hitpoints
		}
	}
}

void Emulator::IterateBaseDamage() // count damage to bases
{
	for(set<XUnit*>::iterator it = bases.begin();it != bases.end();it++)
	{
		XUnit &b = **it;
		if(b.hitpoints <= b.damage) // base is "killed"
		{ 
			if(!b.attacker_dirty) // base is captured
			{
				b.hitpoints = environment.base_hp;
				b.team = b.attacker;
				b.cooldown = environment.robot_construction_time - 1;
			}
		}
		else
		{
			b.hitpoints -= b.damage; // decrease hitpoints
		}
	}
}

void Emulator::IterateRobotMessages() // message exchange
{
	const int width = environment.field_width;	
	const int t_range = environment.transmit_range;	
	const int m_size = environment.msg_size;

	for(set<XUnit*>::iterator it = robots.begin();it != robots.end();it++) // clear all previous messages
	{		
		RobotState& s = *((*it)->state);
		for(vector<void*>::iterator ii = s.messages.begin();ii != s.messages.end();ii++) // free messages
			delete[] (*ii);
		s.messages.clear();		
	}

	int dist;
	
	for(set<XUnit*>::iterator it = robots.begin();it != robots.end();it++) // for each robot
	{
		XUnit &r1 = **it;
		if(!r1.state->transmit) continue; // skip if is dead or not transmitting
		for(int y = r1.y - t_range;y <= r1.y + t_range;y++) // search robots in transmit range
		{
			int i = abs(r1.y - y);
			for(int x = r1.x - t_range + i/2 + i%2;x <= r1.x + t_range - i/2;x++)
			{
				if(environment.IsOnField(x, y) && !((x == r1.x) && (y == r1.y))
					&& field[y*width + x] && !field[y*width + x]->base && (field[y*width + x]->team == r1.team)) // find robot from same team
				{
					XUnit &r2 = *field[y*width + x];					

					dist = environment.Distance(r1.x, r1.y, r2.x, r2.y); // dist between 

					if(dist <= t_range) // robots are in transmit range
					{
						r2.state->AddMessage(r1.state->message);
					}
				}
			}
		}
	}
}

void Emulator::IterateRobotMoves() // move robots
{
	const int width = environment.field_width;
	const int height = environment.field_height;

	int dx, dy;

	memset(move_request, 0, sizeof(size_t)*width*height);
	for(set<XUnit*>::iterator it = robots.begin();it != robots.end();it++) // for each robot
	{
		XUnit &r = **it;
		if((r.hitpoints > 0) && (r.state->move_direction != 0)) // robot is alive and moving
		{						
			if(environment.Shift(r.x, r.y, r.state->move_direction, dx, dy) && !field[dy*width + dx]) // can move
			{
				move_request[dy*width + dx]++; // set request
			}
			move_request[r.y*width + r.x]++; // this cell is also busy
		}
	}
		
	for(set<XUnit*>::iterator it = robots.begin(); it != robots.end(); it++) // move robots
	{
		XUnit &r = **it;
		if((r.hitpoints > 0) && (r.state->move_direction != 0)) // robot is alive and moving
		{						
			if(environment.Shift(r.x, r.y, r.state->move_direction, dx, dy)
					&& !field[dy*width + dx] && (move_request[dy*width + dx] == 1)) // can move
			{
				field[dy*width + dx] = (*it); // do move
				field[r.y*width + r.x] = 0;
				r.x = dx; r.y = dy;
			}
			else
			{
				r.state->move_direction = 0; // can't move - staying
			}
		}
	}
}

void Emulator::GetDeadRobots(set<XUnit*>& dead) // find dead robots
{
	dead.clear();
	for(set<XUnit*>::iterator it = robots.begin();it != robots.end();it++) // for each robot
	{
		if((*it)->hitpoints == 0) dead.insert(*it); // zero hp - add
	}
}

void Emulator::Iterate() // do cations for robots and bases
{	
	set<XUnit*> dead_robots; // robots to delete
	GetDeadRobots(dead_robots);

	if(visualizer) UpdateRobotData(dead_robots, 1); // visuzalize dead robots

	for(set<XUnit*>::iterator it = dead_robots.begin();it != dead_robots.end();it++) // erase dead robots
	{	
		//field[(*it)->state->y*environment.field_width + (*it)->state->x] = 0; // free field
		delete (*it);
		robots.erase(*it); // delete from robots	
	}
	
	IterateRobotSpawn(); // spawn new robots

	if(visualizer) UpdateBaseData(bases, 0); // vis bases

	IterateRobotView(); // init robot views
	IterateRobotMessages(); // message exchange		
	IterateRobotActions(); // call actions
		
	if(visualizer) UpdateRobotData(robots, 1); // write robot health, pos, etc

	IterateRobotDamage(); // count damage for robots
	IterateBaseDamage(); // count damage for bases
//	IterateRobotMessages(); // message exchange
	IterateRobotMoves(); // move robots

	if(visualizer) UpdateRobotData(robots, 2); // write robot move and shoot intents
}

int Emulator::Emulate()
{
	time_t st = time(NULL);

	vector<RobotData> rs;
	vector<BaseData> bs;

	//while(!Finished()) // && (env.ITER < env.LAST_ITER) && (time(NULL) - st <= env.TIMEOUT) // make iterations
	cout << "Starting emulation" << endl;

	do
	{ 
		//printf("iter: %d, time left: %d\n", env.ITER, TIMEOUT-time(NULL)+st);
		environment.iter++;
		ITER = environment.iter;
		//printf("iter: %d\n", environment.iter);

		robot_data.clear();
		base_data.clear();
				
		Iterate();

		if(visualizer)
		{			
			GetRobotData(rs);
			GetBaseData(bs);
			visualizer->visualize(rs, bs);
		}	

		//printf("end iter %d\n", environment.iter);		
	}
	while(!Finished());

	cout << "Emulation is finished" << endl;

   /*int wt=-1;
   bool only=true;
   for(set<XUnit*>::iterator it = bases.begin(); it != bases.end(); it++)
   {
      if((**it).team!=-1)
      {
         if(wt==-1)
            wt=(**it).team;
         else if((**it).team!=wt)
            only = false;
      }
   }
   if(wt!=-1 && only)
for(set<XUnit*>::iterator it = bases.begin(); it != bases.end(); it++)
(**it).team = wt;

	int winner=-1;
	// if only one team left, it's the winner
	// if more than one teams left, the places is distributed according to bases count
	//    for each team
	// for the rest players, the places are set according to the death time
	int wbc=0;
	map<int, int> bases_count;
	for(set<XUnit*>::iterator it = bases.begin(); it!=bases.end(); it++)
	{
		bases_count[(**it).team]++;
		if(winner==-1 || bases_count[(**it).team]>wbc)
		{
			winner = (**it).team;
			wbc = bases_count[(**it).team];
		}
	}

	cout << "Stopped at iter=" << environment.iter << endl;
	for(int i=0; i<bases.size(); i++)
	{
		if(bases_count[i]>0)
			cout << "Team " << i << ": " << bases_count[i] << " bases" << endl;
	}
	for(map<int, int>::iterator it=death_time.begin(); it!=death_time.end(); it++)
		if(it->second != ITER && it->first!=-1)
			cout << "Team " << it->first << ": death time=" << it->second << endl;

	if(winner!=-1)
	{ // check that the winner is really best, no other player with same bases count exists
		for(int i=0; i<robots.size(); i++)
			if(winner != i && bases_count[i] == wbc)
			{
				winner = -1;
				break;
			}
	}

	if(out!=NULL)
		save(out);*/

	return 0;//winner;
}

int Emulator::AddRobot(const string& name, const string& script)
{	
	if(!script_engine.Exists(script))
	{
		if(script_engine.LoadScript(script, "Scripts/") == -1) return -1;
		if(script_engine.CompileScript(script) == -1) return -1;
	}	
	teams.push_back(name);
	scripts.push_back(script);
	actions.push_back(0); // add 0 action - use script instead
	return 0;
}

int Emulator::AddRobot(const string& name, RobotAction ac)
{
	teams.push_back(name);
	scripts.push_back(""); // insert empty script - use action instead
	actions.push_back(ac); 
return 0;
}

bool Emulator::ValidBaseLocation(int x, int y)
{
	if((x < 2) || ((x + 2) >= Env().field_width) || (y < 2) || ((y + 2) >= Env().field_height)) return false;
	for(set<XUnit*>::const_iterator it = bases.begin();it != bases.end();it++)
	{
		if(Env().Distance(x, y, (*it)->x, (*it)->y) < 5) return false;
	}
return true;
}

void Emulator::InitRandomMap(int width, int height, int b_num, int t_num)
{
	Clear(); // clear all prev emu data

	environment.field_width = width; 
	environment.field_height = height;
	environment.bases_count = b_num;

	field = new XUnit*[width*height];
	memset(field, 0, sizeof(XUnit*)*width*height);

	move_request = new size_t[width*height];
	memset(move_request, 0, sizeof(size_t)*width*height);

	for(int i = 0;i < b_num; i++) // add bases
	{
		int x, y;
		do
		{
			x = rand()%width;
			y = rand()%height;			
		}
		while(!ValidBaseLocation(x, y));

		XUnit* b = new XUnit();
			b->id = last_base_id++;
			b->base = true;
			b->x = x;
			b->y = y;
			b->team = (i >= t_num) ? -1 : i;		
			b->hitpoints = Env().base_hp;
			b->cooldown = 0;
			b->state = 0;
		bases.insert(b);

		field[b->y*width + b->x] = b;
	}
}

int Emulator::LoadMap(const string& file)
{
	fstream f_in(file.c_str());

	if(!f_in.is_open())
	{
		cerr << "Error: failed to open map " << file << endl;
		return -1;
	}

	int width, height, b_num, r_num;

	Clear(); // clear prev field data

	f_in >> width >> height; // read width and height

	environment.field_width = width; 
	environment.field_height = height;	

	field = new XUnit*[width*height];
	memset(field, 0, sizeof(XUnit*)*width*height);

	move_request = new size_t[width*height];
	memset(move_request, 0, sizeof(size_t)*width*height);

	f_in >> b_num; // num of bases

	environment.bases_count = b_num;
	
	for(int i = 0;i < b_num;i++) // read bases
	{		
		XUnit* b = new XUnit();

		f_in >> b->x >> b->y >> b->team >> b->hitpoints >> b->cooldown; // read base info

		if((b->team >= b_num) || (b->team < 0)) b->team = -1; // make neutral base
		b->id = last_base_id++;
		b->base = true;		
		b->state = 0;
		bases.insert(b);

		field[b->y*width + b->x] = b;
	}

	f_in >> r_num; // num o robots
	
	for(int i = 0;i < r_num;i++) // read robots
	{
		XUnit* r = new XUnit();

		f_in >> r->x >> r->y >> r->team >> r->hitpoints >> r->cooldown; // read robot info

		if((r->team >= b_num) || (r->team < 0)) // invalid team - can't have robot without team
		{
			cerr << "Error : ivalid team number " << r->team << " for robot on [" << r->y << ", " << r->x << "]" << endl;
			delete r; // delete robot
			continue;
		}
		r->id = last_robot_id++;
		r->base = false;
		r->state = new RobotState(environment.memory_size, environment.msg_size);
		robots.insert(r);

		field[r->y*width + r->x] = r;
	}		

	f_in.close();
return 0;
}

int Emulator::AddMap(const string& file)
{
	if(file.find("rnd") == 0) // file begins with "rnd" - use random map
	{
		int nx, ny, nb;
		int pos;
		string s = string(file, 3);
		if((pos = s.find("x")) == string::npos) // it's form rnd<SZ>
		{
			nx = ny = atoi(s.c_str());
			nb = nx/4 + 1;
		}
		else // it's from rnd<SZX>x<S>
		{
			string s1 = string(s, 0, pos);
			string s2 = string(s, pos + 1);
			nx = atoi(s1.c_str());

			if((pos = s2.find("x")) == string::npos) // it's form rnd<SZX>x<SZY>
			{
				ny = atoi(s2.c_str());
				nb = (nx + ny)/8 + 1;
			}
			else
			{
				string s21 = string(s2, 0, pos);
				string s22 = string(s2, pos + 1);

				ny = atoi(s21.c_str());
				nb = atoi(s22.c_str());
			}
		}		
		InitRandomMap(nx, ny, nb, teams.size());
		return 0;
	}
	else
	{
		LoadMap(file);
		return 0;
	}
return -1;
}

int Emulator::LoadConfig(const string& file, Environment& env)
{
	fstream f_in(file.c_str());

	if(!f_in.is_open())
	{
		cerr << "Error: failed to open config " << file << endl;
		return -1;
	}

	while(!f_in.eof())
	{
		string tag, val;
		f_in >> tag >> val;

		if(tag == "") continue;
		if(tag[0] == '%') {char tmp[250]; f_in.getline(tmp, 250); continue;} // skip comment

		int value = atoi(val.c_str());

		if(tag == "msg_size") env.msg_size = value;
		else if(tag == "memory_size") env.memory_size = value;
		else if(tag == "shoot_range") env.shoot_range = value;
		else if(tag == "robot_hp") env.robot_hp = value;
		else if(tag == "base_hp") env.base_hp = value;
		else if(tag == "reload_time") env.reload_time = value;
		else if(tag == "view_range") env.view_range = value;
		else if(tag == "transmit_range") env.transmit_range = value;
		else if(tag == "robot_construction_time") env.robot_construction_time = value;
		else if(tag == "timeout") env.timeout = value;
		else if(tag == "last_iter") env.last_iter = value;
		else
		{
			cerr << "Error: invalid or unknown tag: " << tag << endl;
		}					
	}

	f_in.close();
return 0;
}

int Emulator::LoadGame(const string& file)
{
	fstream f_in(file.c_str());

	if(!f_in.is_open())
	{
		cerr << "Error: failed to open game " << file << endl;
		return -1;
	}

	while(!f_in.eof())
	{
		string tag;

		f_in >> tag;

		if(tag == "") continue;
		if(tag[0] == '%') {char tmp[250]; f_in.getline(tmp, 250); continue;} // skip comment

		if(tag == "bot") // add c++ bot
		{
			string name, team;
			f_in >> name >> team;
			if(registered_bots.find(team) == registered_bots.end())
			{
				cerr << "Error: unknown bot: " << team << endl;
			}
			else
			{
				if(AddRobot(name, registered_bots[team]) != -1)
				{
					cout << "Bot " << name << " (" << team << ") is loaded" << endl;
				}
			}
		}
		else if(tag == "sbot") // script bot
		{
			string name, script;
			f_in >> name >> script;
			if(AddRobot(name, script) == -1)
			{
				cerr << "Error: unknown script bot " << script << endl;
			}
			else
			{
				cout << "Script bot " << name << " (" << script << ") is loaded" << endl; 
			}
		}
		else if(tag == "map")
		{
			string map;
			f_in >> map;
			if(AddMap(map) == -1)
			{
				cerr << "Error: failed to add map " << map << endl;
			}
		}
		else
		{
			cerr << "Error: invalid or unknown tag: " << tag << endl;
		}
	}

	f_in.close();
return 0;
}

/*void init()
{
	TIMEOUT=1200;
	//ADD(BadBot)
	//ADD(DataBot)
	//ADD(DMBot)
	//ADD(DoomBot)
	//ADD(RaS)
	//ADD(SVPRO)
	//ADD(TBot)

//maps.push_back("DOM40.txt");
//maps.push_back("LT40.txt");

	AddRobot("myrobot");
	AddRobot("myrobot");
//maps.push_back("rnd80");

	
	for(int i=0; i<RC; i++)
		for(int j=0; j<RC; j++)
			SCORE[i][j] = 0;
}*/

/*vector<const char*> bot_names;
	
const char* cat(const char*name)
{
	if(strlen(name)<6)
		return name;

	char* res = new char[strlen(name)];
	strcpy(res, name);
	res[6] = '\0';
	return res;
}


int match(int map, int r1, int r2)
{
	string s;
	vector<RobotAction> p;
	p.push_back(robots[r1]);
	p.push_back(robots[r2]);
	bot_names.clear();
	bot_names.push_back(names[r1]);
	bot_names.push_back(names[r2]);
	cout << endl << endl << "MATCH: [" << r1 << "]" << names[r1] << " -vs- [" << r2 << "]"
		<< names[r2] << " on map " << maps[map] << endl;
#ifndef FAST
	getline(cin, s);
#endif
	int res = emulate(p, maps[map], NULL);
//	int res = -1 + rand()%3;
	switch(res)
	{
		case -1:
			cout << endl << "MATCH RESULT: " << names[r1] << " VS " << names[r2] << ": DRAW on map " 
				<< maps[map] << endl;
			SCORE[r1][r2]++;
			SCORE[r2][r1]++;
			break;
		case 0:
			cout << endl << "MATCH RESULT: " << names[r1] << " WINS " << names[r2]
				<< " on map " << maps[map] << endl;
			SCORE[r1][r2]+=2;
			break;
		case 1:
			cout << endl << "MATCH RESULT: " << names[r2] << " WINS " << names[r1]
				<< " on map " << maps[map] << endl;
			SCORE[r2][r1]+=2;
			break;
	}
	return -1;
}

int score(int r)
{
	int sum=0;
	for(int i=0; i<RC; i++)
		sum+=SCORE[r][i];
	return sum;
}*/

}

vector<string> bot_names;

void *emuThreadFunc(void *arg)
{
	//Emulator emu;

	srand(time(NULL));

	Environment env;

	if(emu.LoadConfig("config.txt", env) == -1) return 0;

	emu.SetEnvironment(env); // set env for emulator

	if(emu.LoadGame("game.txt") == -1) return 0;

	InitGlobalVars(emu.Env());

	emu.GetTeams(bot_names);

	VisualizerController* vis = new VisualizerOpenGLController(emu.Env().field_width, emu.Env().field_height, bot_names.size());

	emu.SetVisualizer(vis);

	emu.Emulate();

	delete vis;

	return NULL;
}

