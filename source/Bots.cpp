#include "Emulator.h"

#define BOT(name) namespace name { void name(RobotState&); } using namespace name;

// built-in c++ bots
BOT(Bot)
BOT(BadBot)
BOT(DataBot)
BOT(DMBot)
BOT(DoomBot)
BOT(RaS)
BOT(SmartBot)
BOT(SVPRO)
BOT(TBot)
BOT(TBot_v1)
BOT(YuBot)

namespace emulator
{

#define REG_BOT(name, bot) registered_bots[name] = bot::bot;

void Emulator::RegisterBots() 	// register built-in c++ bots
{
	REG_BOT("bot", Bot) 
	REG_BOT("badbot", BadBot)
	REG_BOT("databot", DataBot)
	REG_BOT("dmbot", DMBot)
	REG_BOT("doombot", DoomBot)
	REG_BOT("ras", RaS)
	REG_BOT("smart", SmartBot)
	REG_BOT("svpro", SVPRO)
	REG_BOT("tbot", TBot)
	REG_BOT("tbotv1", TBot_v1)
	REG_BOT("yubot", YuBot)
}

}
