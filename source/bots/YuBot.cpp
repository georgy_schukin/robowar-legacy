#include "Robot.h"
#include <iostream>
#include <stdlib.h>
#include <string.h>

using namespace std;

namespace YuBot
	{
	/*	// parameters
	int re_max;               // maximum old message retransmit 
	int good_range;           // preferred distance to enemy 
	int	f_range;              // radius to coount friends
	int f_ok;                 // number of friends in radius to do something special
	int sea_num;              // number of initial searchers

	int searcher_probability; //

	int defend;               // use defenders or not
	int def_range;            // defender radius
	int def_need;             // approximate number of defenders

	void set_parameters()
	{ re_max = 10;                // maximum old message retransmit
	 good_range = SHOOT_RANGE-1;   // preferred distance to enemy
	 f_range = TRANSMIT_RANGE/2;         // radius to coount friends
	 f_ok = (3*f_range*(f_range-1)+1)/6; // number of friends in radius
	 sea_num = 1;                        // number of initial searchers

	 searcher_probability = 1000;        //

	defend = 0;                 // use defenders or not
	 def_range = SHOOT_RANGE;    // defender radius
	 def_need = (3*def_range*(def_range-1)+1)/2; // approximate number of defenders
	}
		struct EnemyInfo
	{ int x,y;    // position
	 int dist;   // distance
	 int attack; // attacked
	  int base;   // is base
	  int re;     // retranslation count
	};

	
		int Distance(int x1,int y1,int x2,int y2)
	{ int d,dx,dy;
	dx = abs(x2-x1);
	  dy = abs(y2-y1);
	 if (dx==0 || dy==0) return dx+dy;
	 if ((y1%2==0 && x1<x2) || (y1%2!=0 && x2<x1)) d = (dy+1)/2; // even right || odd left
	 else                                          d = dy/2;     // even left || odd right
	 if (d>dx) d = dx;
	 if (d>dy) d = dy;
	 return dx+dy-d;
	}
	*/
	int get_friends(RobotState& S)
	{
		int res=0;
		for(vector<Unit>::iterator it = S.robots.begin();
				it != S.robots.end(); it++)
			if(S.team == it->team)
				res++;
		return res;
	}

    int get_enemies(RobotState& S)
	{
		int res=0;
		for(vector<Unit>::iterator it = S.robots.begin();
				it != S.robots.end(); it++)
			if(S.team != it->team)
				res++;
		return res;
	} 

	

	void get_sect(int x,int y, RobotState& S)
    {
	int sect_width=FIELD_WIDTH/4,
        sect_height=FIELD_HEIGHT/4;
    if (((x<=sect_width) && (y<=sect_height)) || ((x>=2*sect_width) &&(x<=3*sect_width) && (y<=sect_height)) || ((x<=sect_width) && (y<=3*sect_height)&& (y>=2*sect_height)) ||  ((x>=2*sect_width) &&(x<=3*sect_width) &&  (y<=3*sect_height)&& (y>=2*sect_height))) 
		S.move_direction=1+random()%3;
	else if  (((x>=2*sect_width) &&(x<=3*sect_width) && (y<=sect_height)) || ((x>=3*sect_width) && (y<=sect_height)) || ((x>=2*sect_width) &&(x<=3*sect_width) && (y<=3*sect_height)&& (y>=2*sect_height)) ||  ((x>=3*sect_width) && (y<=3*sect_height)&& (y>=2*sect_height))) 
		S.move_direction=2+random()%3;
	else if (((x<=sect_width) && (y<=2*sect_height)&& (y>=sect_height)) || ((x>=2*sect_width) &&(x<=3*sect_width) && (y>=sect_height) && (y<=2*sect_height)) || ((x<=sect_width) && (y>=3*sect_height)) ||  ((x>=2*sect_width) &&(x<=3*sect_width) &&  (y<=3*sect_height)&& (y>=2*sect_height))) 
	  	 S.move_direction=(4+random()%3)%6+1;
	else S.move_direction=3+random()%3;
	    
	} 

	void get_sect1(int x,int y, RobotState& S)
    {
	int sect_width=FIELD_WIDTH/4,
        sect_height=FIELD_HEIGHT/4;
	//1, 5, 9, 13 ������
    if (((x<=sect_width) && (y<=sect_height)) || ((x>=2*sect_width) &&(x<=3*sect_width) && (y<=sect_height)) || ((x<=sect_width) && (y<=3*sect_height)&& (y>=2*sect_height)) ||  ((x>=2*sect_width) &&(x<=3*sect_width) &&  (y<=3*sect_height)&& (y>=2*sect_height))) 
		 S.move_direction=1+random()%3;
	//2, 6, 10, 14 ������
	else if  (((x>=sect_width) &&(x<=2*sect_width) && (y<=sect_height)) || ((x>=3*sect_width) && (y<=sect_height)) || ((x>=sect_width) &&(x<=2*sect_width) && (y<=3*sect_height)&& (y>=2*sect_height)) ||  ((x>=3*sect_width) && (y<=3*sect_height)&& (y>=2*sect_height))) 
		 S.move_direction=2+random()%3;
	//3, 7, 11, 15 ������
	else if (((x<=sect_width) && (y<=2*sect_height)&& (y>=sect_height)) || ((x>=2*sect_width) &&(x<=3*sect_width) && (y>=sect_height) && (y<=2*sect_height)) || ((x<=sect_width) && (y>=3*sect_height)) ||  ((x>=2*sect_width) &&(x<=3*sect_width) &&  (y>=3*sect_height))) 
	  	S.move_direction=(4+random()%3)%6+1;
	//4, 8, 12, 16 ������
	else S.move_direction=3+random()%3;
	    
	} 

	void get_sect2(int x,int y,  RobotState& S)
{
    int sect_width=FIELD_WIDTH/2,
        sect_height=FIELD_HEIGHT/2;
    if (x<=sect_width)
	if (y<=sect_height)
	    S.move_direction=1+random()%3;
	else
	     S.move_direction=2+random()%3;
    else
	if (y<=sect_height)
	    S.move_direction=(4+random()%3)%6+1;
	else
	    S.move_direction=3+random()%3;

   //no way

}
	void peren(int x,int y, RobotState& S, int &eix, int &eiy) //�������������� �����, � ������� ���������
	{
	if (abs(S.x-x)>=abs(S.y-y)) 
	{eix=x-SHOOT_RANGE/2+random()%SHOOT_RANGE;
	eiy=y;}
	else
	{eiy=y-SHOOT_RANGE/2+random()%SHOOT_RANGE;
	 eix=x;}
	}


	void YuBot(RobotState& S)
	{
	/* while (ITER<=50) 
	{ for (ITER=1; ITER<=5; i++) 
		S.move_direction=3;
	  S.move_direction=4;
	  for (i=1; i<=6; i++) 
		S.move_direction=5;
	  for (i=1; i<=7; i++) 
		S.move_direction=6;
	  for (i=1; i<=8; i++) 
		S.move_direction=1;
	  for (i=1; i<=9; i++) 
		S.move_direction=2; 
	} 
	int dx, dy;
	if (shift(S.x, S.y, S.move_direction, dx, dy)==false)
	 if (ITER<=5) 
		S.move_direction=2;
	 if (ITER==5)
	  S.move_direction=3;
	 if ((ITER>=6) && (ITER<=11)) 
		S.move_direction=4;
	 if ((ITER>11) && (ITER=18)) 
		S.move_direction=5;
	  if ((ITER>18) && (ITER=26)) 
		S.move_direction=6;
	  if ((ITER>26) && (ITER=35)) 
		S.move_direction=1; */
		int x, y;
		int id = ((int*)S.memory)[0];
		int i;
			x=S.x;
			y=S.y;
		if (ITER<=ROBOT_CONSTRUCTION_TIME*5)   
		 {if (id==0) (id=(ITER/ROBOT_CONSTRUCTION_TIME+1)); //����� +1 ����� ������ ���������� ����� �� ����� �� �����
		 {	
			
			if ((id!=0)&&(x<=FIELD_WIDTH-1) && (y<=FIELD_HEIGHT-1)) //���� ������ 6 ������� �� ������� �� ������� �����-4����
			{ switch(id)
			 { case 1: {S.move_direction=3;
						S.move_direction=4;
						S.move_direction=5;
						for (i=0;i<10;i++)
						S.move_direction=4;
						}
			  break;

			  case 2: {S.move_direction=3;
					   S.move_direction=4;
					   for (i=0;i<20;i++)
					   S.move_direction=3;
					  }
			  break;

			  case 3: {S.move_direction=5;
					   S.move_direction=4;
					   for (i=0;i<20;i++)
				       S.move_direction=5;
					}
			  break;

			  case 4: {S.move_direction=5;
					  for (i=0;i<20;i++)
				       S.move_direction=6;
					  }
			  break;
		
			  case 5: {S.move_direction=3;
					  for (i=0;i<20;i++)
				       S.move_direction=2;
					 }
			  break;

			  case 6: {for (i=0;i<20;i++)
					   S.move_direction=1;
					 }
			  break;
			 } 
			}
		
		 }}

		 //������� ������
		 if ((ITER>ROBOT_CONSTRUCTION_TIME*5) && (ITER<=200) && (id==0)) get_sect(x, y, S); //��� ���-�� ��������
		 if ((ITER>200)&&(ITER<=270)&&(id==0)) get_sect1(x, y, S);
		 if ((ITER>270)&&(ITER<=550)&&(id==0)) get_sect2(x, y, S);
				else if (id==0)
					 S.move_direction=random()%6+1;

					 S.transmit = false; 
					 int f = get_friends(S); 
					 int e =	get_enemies(S); 
					 int key=1; 
					 Unit b,c; 
					 int* mem = (int*)S.message; 
					 // ����������� ��������� �� ��������� �����, � ������� ����:
					 int koor = ((short*)S.memory)[0]; 
					 int idu=0;
					 int zx, zy;
		
			
			if(e > f)
			{ // runaway
				for(vector<Unit>::iterator it = S.robots.begin(); it != S.robots.end() && key==1; it++)
				{
				    b=*it;
				    if (S.team!=b.team )
					{
					    S.move_direction = (get_dir(S.x, S.y, b.x, b.y) + 2)%6;
					    key=0;
						}
				}
			}
			else if(e==0)
			{
			  int dx, dy;
			    
			    if(shift(S.x, S.y, S.move_direction, dx, dy)==false)
				   S.move_direction=(S.move_direction+1)%6+1;
				
			
				
			}//f>e
		    else{
			for(vector<Unit>::iterator it = S.robots.begin(); it != S.robots.end() && key==1; it++)
		    {
		        b=*it;
		        if (S.team!=b.team )
				{
				    S.move_direction = get_dir(S.x, S.y, (*it).x, (*it).y);
				    key=0;
				}
		    }
		}
	//	if ((e==0)&&(id==0)&&(S.bases.empty()==true)&&(S.robots.empty()==true))  get_sect(S.x, S.y, S); //����� ��������

		if (e==0)
		if(S.bases.empty()==false)
		{
			for(vector<Unit>::iterator it = S.bases.begin(); it != S.bases.end(); it++)
			{
				Unit& b = *it;
				if(b.team != S.team)
				{
					S.shoot = true;
					S.shoot_x = b.x;
					S.shoot_y = b.y;
					S.move_direction = 0;
					if(get_friends(S)>1 && f>e+1)
					S.move_direction = get_dir(S.x, S.y, b.x, b.y);
					if 	(ITER>60) S.transmit = true;
						int* mem = (int*)S.message;
						mem[0] = b.x;
						mem[1] = b.y;
						mem[2] = ITER;
				}
			}
		}
		if(S.robots.empty()==false)
		{
			for(vector<Unit>::iterator it = S.robots.begin(); it != S.robots.end(); it++)
			{
				Unit& b = *it;
				if (b.team != S.team)
				{	S.shoot = true;
					S.shoot_x = b.x;
					S.shoot_y = b.y;
					S.move_direction = 0;
					if(get_friends(S)>1 && f>e+1)
					 {if (id==0) 
						{if (idu==0) 
							{peren(b.x, b.y, S, b.x, b.y);
							((short*)S.memory)[0] = b.x;
							((short*)S.memory)[1] = b.y;
							zx = ((short*)S.memory)[0];
							zy =  ((short*)S.memory)[1];
							S.move_direction = get_dir(S.x, S.y, zx, zy);
							idu=1;}
						else S.move_direction = get_dir(S.x, S.y, b.x, b.y);}
					else S.move_direction = get_dir(b.x, b.y, S.x, S.y);} //���� ��������� ����� ����� ����� - �������

					if(e>f)
						S.move_direction = get_dir(b.x, b.y, S.x, S.y);

						S.transmit = true; //�������� ���������
						int* mem = (int*)S.message;
						mem[0] = b.x;
						mem[1] = b.y;
						mem[2] = ITER;
					    }
												
									
				}
			}

		
		if(id==0 && S.shoot==false && S.messages.empty()==false) //������� ����� �������� ���������
		{
			int* msg = (int*)S.messages[0];
			int tx = msg[0];
			int ty = msg[1];
							peren(tx, ty, S, tx, ty);
							((short*)S.memory)[0] = tx;
							((short*)S.memory)[1] = ty;
							zx = ((short*)S.memory)[0];
							zy =  ((short*)S.memory)[1];
							S.move_direction = get_dir(S.x, S.y, zx, zy);
							
		
			if(!S.transmit && ITER-msg[2]<5)
			{
				memcpy(S.message, S.messages[0], MSG_SIZE);
				S.transmit = true;
			}
		 }
	}	
	/*	// sort enemies and divide them into near and far
  while (!eall.empty())
  { list<EnemyInfo>::iterator emin=eall.begin();
    int dmin=FIELD_HEIGHT+FIELD_WIDTH;
    for (list<EnemyInfo>::iterator eit=eall.begin();eit!=eall.end();eit++)
      if (dmin>(*eit).dist) { dmin=(*eit).dist; emin=eit; }
    if (dmin<=SHOOT_RANGE) enea.push_back(*emin);
                      else efar.push_back(*emin);
    eall.erase(emin);
  }
  */
	
	
	
}
