#include "Robot.h"
#include <iostream>
#include <stdlib.h>
#include <string.h>

#ifdef _WIN32
#include <Windows.h>
#define usleep(x) Sleep(x/1000)
#define random() rand()
#endif

using namespace std;

namespace SmartBot
{
	int get_friends(RobotState& S)
	{
		int res=0;
		for(vector<Unit>::iterator it = S.robots.begin();
				it != S.robots.end(); it++)
			if(S.team == it->team)
				res++;
		return res;
	}

	int get_enemies(RobotState& S)
	{
		int res=0;
		for(vector<Unit>::iterator it = S.robots.begin();
				it != S.robots.end(); it++)
			if(S.team != it->team)
				res++;
		return res;
	}

	void SmartBot(RobotState& S)
	{
//		if (S.memory[MEMORY_SIZE-1]!=1) "�����";
//		S.memory[MEMORY_SIZE-1]=1;

		S.transmit = false;
		int f = get_friends(S);
		int e = get_enemies(S);

		/*if(e>=f)
		{ // runaway
			for(vector<Unit>::iterator it = S.robots.begin();
					it != S.robots.end(); it++)
			{
				S.move_direction = get_dir(S.x, S.y, (*it).x, (*it).y) + 3;
				if(S.move_direction>6)
					S.move_direction-=6;
			}
		}
		else if(e==0)
		{ // attack
			for(vector<Unit>::iterator it = S.robots.begin();
					it != S.robots.end(); it++)
			{
				S.move_direction = get_dir(S.x, S.y, (*it).x, (*it).y) + 3;
				if(S.move_direction>6)
					S.move_direction-=6;
				int dx, dy;
				if(shift(S.x, S.y, S.move_direction, dx, dy)==false)
					S.move_direction = random()%7;
			}

		}*/
		
		S.move_direction = random()%7;
	//	if(f<3)
	//		S.move_direction=0;
		if(S.bases.empty()==false)
		{
			for(vector<Unit>::iterator it = S.bases.begin(); it != S.bases.end(); it++)
			{
				Unit& b = *it;
				if(b.team != S.team)
				{
					S.shoot = true;
					S.shoot_x = b.x;
					S.shoot_y = b.y;
					S.move_direction = 0;
					if(get_friends(S)>1)
						S.move_direction = get_dir(S.x, S.y, b.x, b.y);
					S.transmit = true;
					int* mem = (int*)S.message;
					mem[0] = b.x;
					mem[1] = b.y;
					mem[2] = ITER;
				}
			}
		}
		if(S.robots.empty()==false)
		{
			for(vector<Unit>::iterator it = S.robots.begin(); it != S.robots.end(); it++)
			{
				Unit& b = *it;
				if(b.team != S.team)
				{
					S.shoot = true;
					S.shoot_x = b.x;
					S.shoot_y = b.y;
					S.move_direction = 0;
					if(get_friends(S)>1 && f>e+1)
						S.move_direction = get_dir(S.x, S.y, b.x, b.y);
					if(e>f)
						S.move_direction = get_dir(b.x, b.y, S.x, S.y);
					S.transmit = true;
					int* mem = (int*)S.message;
					mem[0] = b.x;
					mem[1] = b.y;
					mem[2] = ITER;
				}
			}
		}
		if(S.shoot==false && S.messages.empty()==false)
		{
			int* msg = (int*)S.messages[0];
			int tx = msg[0];
			int ty = msg[1];
			S.move_direction=get_dir(S.x, S.y, tx, ty);
			if(!S.transmit && ITER-msg[2]<5)
			{
				memcpy(S.message, S.messages[0], MSG_SIZE);
				S.transmit = true;
			}
		}
	}
}
