#include "Robot.h"
#include <list>
#include <map>
#include <stdlib.h>
#include <string.h>

#include <stdio.h>

using namespace std;

namespace TBot
{

//-----------------------------------------------------------------------------------------
// parameters
int re_max;               // maximum old message retransmit
int good_range;           // preferred distance to enemy
int f_range;              // radius to coount friends
int f_ok;                 // number of friends in radius to do something special
int sea_num;              // number of initial searchers

int searcher_probability; //

int defend;               // use defenders or not
int def_range;            // defender radius
int def_need;             // approximate number of defenders

int Distance(int x1,int y1,int x2,int y2);

void set_parameters(RobotState &S)
{ int diameter=Distance(0,0,FIELD_WIDTH,FIELD_HEIGHT);
  re_max = diameter/TRANSMIT_RANGE;              // maximum old message retransmit
  if (S.hitpoints>2) good_range = SHOOT_RANGE-1; // preferred distance to enemy
                else good_range = 1;
  f_range = TRANSMIT_RANGE/2;         // radius to coount friends
  f_ok = (3*f_range*(f_range-1)+1)/6; // number of friends in radius
  sea_num = 3;                        // number of initial searchers

  searcher_probability = 1000;        //

  defend = 0;                 // use defenders or not
  def_range = SHOOT_RANGE;    // defender radius
  def_need = (3*def_range*(def_range-1)+1)/2; // approximate number of defenders
}
//-----------------------------------------------------------------------------------------
enum MessageType {mt_none=0,mt_born,mt_born_answer,mt_shoot,mt_move,mt_enemy};
enum StatusType {st_none=0,st_born,st_searcher,st_defender,st_trooper,st_cheater};

int iter0=0;
int max_msg_num=0,max_msg_num_want=0,max_msg_size;
int msg_num=0,msg_num_want=0,msg_size=0;

struct MessageInfo
{ int id;          // id
  int x,y;         // position
  MessageType act; // action
  int tx,ty;       // target position
  int d1,d2;       // some other data
};

struct EnemyInfo
{ int x,y;    // position
  int dist;   // distance
  int attack; // attacked
  int base;   // is base
  int re;     // retranslation count
  int ok;     // information is considered to be correct
};

struct FriendInfo
{ int id;            // id
  StatusType status; // status
  int x,y;           // position
  int dist;          // distance
  int base;          // is base
};

//-----------------------------------------------------------------------------------------
int Distance(int x1,int y1,int x2,int y2)
{ int d,dx,dy;
  dx = abs(x2-x1);
  dy = abs(y2-y1);
  if (dx==0 || dy==0) return dx+dy;
  if ((y1%2==0 && x1<x2) || (y1%2!=0 && x2<x1)) d = (dy+1)/2; // even right || odd left
  else                                          d = dy/2;     // even left || odd right
  if (d>dx) d = dx;
  if (d>dy) d = dy;
  return dx+dy-d;
}

void MessageInit(void *msg,int id,int x,int y,StatusType st)
{
  short *m=(short*)msg;
//  m[0]=(short)id; // robot id
  m[0]=4;
  m[1]=(short)x;  // robot position: x
  m[2]=(short)y;  // robot position: y
  m[3]=(short)st; // robot status
  m[4]=0;         // end of submessage list
}

void MessageFinalize(void *msg,int id,StatusType st)
{
  short *m=(short*)msg;
  m[0]=id;        // robot id
  m[3]=(short)st; // robot status
}

void MessageAppend(void *msg,MessageInfo &mi)
{ const int max_msg_size = 6;
  int total=*((short*)msg);

  if(total + max_msg_size >= MSG_SIZE/sizeof(short)) return;

  short *m=(short*)msg + total;
  int size;// = *m;
  msg_num_want++;
  //if (total+max_msg_size > MSG_SIZE/sizeof(short)) return;
  m[1]=(short)mi.act;
  if (mi.act==mt_enemy) { size=6; m[2]=(short)mi.tx; m[3]=(short)mi.ty; m[4]=(short)mi.d1; m[5]=(short)mi.d2; }
  else
    switch(mi.act)
    { //case mt_enemy:       size=6; m[2]=(short)mi.tx; m[3]=(short)mi.ty; m[4]=(short)mi.d1; m[5]=(short)mi.d2; break;
      case mt_born:        size=5; m[2]=(short)mi.tx; m[3]=(short)mi.ty; m[4]=(short)mi.d1; break;
      case mt_born_answer: size=5; m[2]=(short)mi.tx; m[3]=(short)mi.ty; m[4]=(short)mi.d1; break;
      case mt_shoot:       size=5; m[2]=(short)mi.tx; m[3]=(short)mi.ty; m[4]=(short)mi.d1; break;
      case mt_move:        size=4; m[2]=(short)mi.tx; m[3]=(short)mi.ty; break;
      default:             size=2; break;
    }
  m[0]=(short)size;
  m[size]=0;
  *(short*)msg = total+size;
  msg_num++;
  msg_size=(total+size)*sizeof(short);
}

void MessageRead(void *msg,list<MessageInfo> &minfo,FriendInfo &fi)
{ MessageInfo mi;
  short *m=(short *)msg;
  fi.id = mi.id = m[0];
  fi.x  = mi.x  = m[1];
  fi.y  = mi.y  = m[2];
  fi.status = (StatusType)m[3];
  fi.base = 0;
  m += 4;
  int size = m[0];
  int total = 4+size;
  while (size>0 && total<MSG_SIZE/sizeof(short))
  { mi.act = (MessageType)m[1];
    if (mi.act==mt_enemy) { mi.tx = m[2]; mi.ty = m[3]; mi.d1 = m[4]; mi.d2 = m[5]; minfo.push_back(mi); }
	else
      switch (mi.act)
      { case mt_born:        mi.tx = m[2]; mi.ty = m[3]; mi.d1 = m[4];               minfo.push_back(mi); break;
        case mt_born_answer: mi.tx = m[2]; mi.ty = m[3]; mi.d1 = m[4];               minfo.push_back(mi); break;
        case mt_shoot:       mi.tx = m[2]; mi.ty = m[3]; mi.d1 = m[4]; mi.d2 = 0;    minfo.push_back(mi); break;
        case mt_move:        mi.tx = m[2]; mi.ty = m[3];                             minfo.push_back(mi); break;
        //case mt_enemy:       mi.tx = m[2]; mi.ty = m[3]; mi.d1 = m[4]; mi.d2 = m[5]; minfo.push_back(mi); break;
      }
    m += size;
    size = m[0];
    total += size;
  }
}

void PositionMove(int &x,int &y,int direction)
{ switch(direction)
  { case 1: x++; break;
    case 2: y++; if (y%2==0) x++; break;
    case 3: y++; if (y%2!=0) x--; break;
    case 4: x--; break;
    case 5: y--; if (y%2!=0) x--; break;
    case 6: y--; if (y%2==0) x++; break;
  }
}

int choose_random(int a,int b) { return rand()%2==0 ? a : b; }

int DirectionTo(int x1,int y1,int x2,int y2)
{ if (y2>y1)      // going down
  { if (y1%2==0) if (x2>x1) return 2;
                 else       return 3;
    else         if (x2<x1) return 3;
                 else       return 2;
  }
  else if (y2<y1) // going up
  { if (y1%2==0) if (x2>x1) return 6;
                 else       return 5;
    else         if (x2<x1) return 5;
                 else       return 6;
  }
  else            // same y
  { if (x2>x1)      return 1;
    else if (x2<x1) return 4;
  }
  return 0;
}

int DirectionRandomTo(int x1,int y1,int x2,int y2)
{
  int dx=abs(x1-x2);
  int dy=abs(y1-y2);
  if (y2>y1)      // going down
  { if (y1%2==0) if (x2>x1)  // dir = 2;
                 { if (dy>=3*dx) return choose_random(2,3);
                   if (dy<2*dx-1) return choose_random(2,1);
                   return 2;
                 }
                 else       // dir = 3;
                 { if (dy>=4*dx) return choose_random(3,2);
                   if (dy<2*dx) return choose_random(3,4);
                   return 3;
                 }
    else         if (x2<x1) // dir = 3;
                 { if (dy>=3*dx) return choose_random(3,2);
                   if (dy<2*dx-1) return choose_random(3,4);
                   return 3;
                 }
                 else       // dir = 2;
                 { if (dy>=4*dx) return choose_random(2,3);
                   if (dy<2*dx) return choose_random(2,1);
                   return 2;
                 }
  }
  else if (y2<y1) // going up
  { if (y1%2==0) if (x2>x1) // dir = 6;
                 { if (dy>=3*dx) return choose_random(6,5);
                   if (dy<2*dx-1) return choose_random(6,1);
                   return 6;
                 }
                 else       // dir = 5;
                 { if (dy>=4*dx) return choose_random(5,6);
                   if (dy<2*dx) return choose_random(5,4);
                   return 5;
                 }
    else         if (x2<x1) // dir = 5;
                 { if (dy>=3*dx) return choose_random(5,6);
                   if (dy<2*dx-1) return choose_random(5,4);
                   return 5;
                 }
                 else       // dir = 6;
                 { if (dy>=4*dx) return choose_random(6,5);
                   if (dy<2*dx) return choose_random(6,1);
                   return 6;
                 }
  }
  else            // same y
  { if (x2>x1)      return 1;
    else if (x2<x1) return 4;
  }
  return 0;
}

int DirectionRandomFrom(int x1,int y1,int x2,int y2)
{
  int dx=abs(x1-x2);
  int dy=abs(y1-y2);
  if (y2>y1)      // going down
  { if (y1%2==0) if (x2>x1)  // dir = 5;
                 { if (dy>=3*dx) return choose_random(5,6);
                   if (dy<2*dx-1) return choose_random(5,4);
                   return 5;
                 }
                 else       // dir = 6;
                 { if (dy>=4*dx) return choose_random(6,5);
                   if (dy<2*dx) return choose_random(6,1);
                   return 6;
                 }
    else         if (x2<x1) // dir = 6;
                 { if (dy>=3*dx) return choose_random(6,5);
                   if (dy<2*dx-1) return choose_random(6,1);
                   return 6;
                 }
                 else       // dir = 5;
                 { if (dy>=4*dx) return choose_random(5,6);
                   if (dy<2*dx) return choose_random(5,4);
                   return 5;
                 }
  }
  else if (y2<y1) // going up
  { if (y1%2==0) if (x2>x1) // dir = 3;
                 { if (dy>=3*dx) return choose_random(3,2);
                   if (dy<2*dx-1) return choose_random(3,4);
                   return 3;
                 }
                 else       // dir = 2;
                 { if (dy>=4*dx) return choose_random(2,3);
                   if (dy<2*dx) return choose_random(2,1);
                   return 2;
                 }
    else         if (x2<x1) // dir = 2;
                 { if (dy>=3*dx) return choose_random(2,3);
                   if (dy<2*dx-1) return choose_random(2,1);
                   return 2;
                 }
                 else       // dir = 3;
                 { if (dy>=4*dx) return choose_random(3,2);
                   if (dy<2*dx) return choose_random(3,4);
                   return 3;
                 }
  }
  else            // same y
  { if (x2>x1)      return 4;
    else if (x2<x1) return 1;
  }
  return 0;
}

int DirectionFrom(int x1,int y1,int x2,int y2)
{ if (y2>y1)      // going up
  { if (y1%2==0) if (x2>x1) return 5;
                 else       return 6;
    else         if (x2<x1) return 6;
                 else       return 5;
  }
  else if (y2<y1) // going down
  { if (y1%2==0) if (x2>x1) return 3;
                 else       return 2;
    else         if (x2<x1) return 2;
                 else       return 3;
  }
  else            // same y
  { if (x2>x1)      return 4;
    else if (x2<x1) return 1;
  }
  return 0;
}

int DirectionRandom() { return 1+rand()%6; }

int DirectionRandomRadius(RobotState &S,int cx,int cy,int dist)
{ int tx,ty;
  if (Distance(S.x,S.y,cx,cy)>dist) return DirectionRandomTo(S.x,S.y,cx,cy);
  for (int i=0;i<100;i++)
  { int dir=1+rand()%6;
    tx=S.x; ty=S.y;
    PositionMove(tx,ty,dir);
    if (Distance(tx,ty,cx,cy)<=dist) return dir;
  }
  return 0;
}

void ShootDecision(RobotState &S,list<EnemyInfo> &enea)
{ if (enea.empty()) { S.shoot=false; return; }
  list<EnemyInfo>::iterator emin=enea.begin();
  for (list<EnemyInfo>::iterator eit=enea.begin();eit!=enea.end();eit++)
  { if (!(*eit).base && (*emin).base) emin=eit;                                   // robots in priority
    else if ((*eit).dist<(*emin).dist) emin=eit;                                  // nearby enemies in priority
    else if ((*eit).dist==(*emin).dist && (*eit).attack>(*emin).attack) emin=eit; // previously attacked enemies in priority
  }
  S.shoot_x=(*emin).x;
  S.shoot_y=(*emin).y;
  S.shoot=true;
}

void MoveDecision(RobotState &S,list<EnemyInfo> &enea,list<EnemyInfo> &efar)
{ if (enea.empty())
  { if (efar.empty()) { S.move_direction = DirectionRandom(); return; } // General case of movement ------------------------------------------ // MAYBE TODO: not to go near friends...
    list<EnemyInfo>::iterator emin=efar.begin();
//    for (list<EnemyInfo>::iterator eit=efar.begin();eit!=efar.end();eit++) if ((*eit).dist<(*emin).dist) emin=eit; // get the nearest enemy
    S.move_direction = DirectionRandomTo(S.x,S.y,(*emin).x,(*emin).y);                                             // go to it!
  }
  else
  { list<EnemyInfo>::iterator emin=enea.begin();
//    for (list<EnemyInfo>::iterator eit=enea.begin();eit!=enea.end();eit++) if ((*eit).dist<(*emin).dist) emin=eit; // get the nearest enemy
    if ((*emin).dist>good_range) S.move_direction = DirectionRandomTo(S.x,S.y,(*emin).x,(*emin).y);              // reach the good_range
    else if ((*emin).dist<good_range) S.move_direction = DirectionRandomFrom(S.x,S.y,(*emin).x,(*emin).y);
    else S.move_direction = 0;
  }
}

void MoveDecisionRadius(RobotState &S,list<EnemyInfo> &enea,list<EnemyInfo> &efar,int x,int y,int dist)
{ if (enea.empty())
  { if (efar.empty()) { S.move_direction = DirectionRandomRadius(S,x,y,dist); return; } // General case of movement ------------------------------------------ // MAYBE TODO: not to go near friends...
    list<EnemyInfo>::iterator emin=efar.begin();
//    for (list<EnemyInfo>::iterator eit=efar.begin();eit!=efar.end();eit++) if ((*eit).dist<(*emin).dist) emin=eit; // get the nearest enemy
    { int dir = DirectionRandomTo(S.x,S.y,(*emin).x,(*emin).y);                                                    // try to go to it!
      int x1=S.x,y1=S.y;
      PositionMove(x1,y1,dir);
      if (Distance(x,y,x1,y1)<=dist) S.move_direction = dir;
                                else S.move_direction = DirectionRandomRadius(S,x,y,dist);
    }
  }
  else
  { list<EnemyInfo>::iterator emin=enea.begin();
    int dir;
    int x1=S.x,y1=S.y;
//    for (list<EnemyInfo>::iterator eit=enea.begin();eit!=enea.end();eit++) if ((*eit).dist<(*emin).dist) emin=eit; // get the nearest enemy
    if ((*emin).dist>good_range) dir = DirectionRandomTo(S.x,S.y,(*emin).x,(*emin).y);                           // reach the good_range
    else if ((*emin).dist<good_range) dir = DirectionRandomFrom(S.x,S.y,(*emin).x,(*emin).y);
    else dir = 0;
    if (dir)
    { PositionMove(x1,y1,dir);
      if (Distance(x,y,x1,y1)>dist) dir = 0;
    }
    S.move_direction = dir;
  }
}

/*
void MoveDecisionRadius(RobotState &S,list<EnemyInfo> &enea,list<EnemyInfo> &efar,int x,int y,int dist)
{ EnemyInfo ei;
  if (enea.empty())
  { if (efar.empty()) { S.move_direction = DirectionRandomRadius(S,x,y,dist); return; } // General case of movement ----------------------------
    ei.dist=FIELD_HEIGHT+FIELD_WIDTH;
    int was=0;
    for (list<EnemyInfo>::iterator eit=efar.begin();eit!=efar.end();eit++)
    { int d = Distance(x,y,(*eit).x,(*eit).y);
      if (d<=dist+SHOOT_RANGE && d<ei.dist) { was=1; ei=*eit; }                       // get the enemy nearest to the base
    }
    if (was)
    { int dir = DirectionRandomTo(S.x,S.y,ei.x,ei.y);                                 // try to go to it!
      int x1=S.x,y1=S.y;
      PositionMove(x1,y1,dir);
      if (Distance(x,y,x1,y1)<=dist) S.move_direction = dir;
                                else S.move_direction = DirectionRandomRadius(S,x,y,dist);
    }
    else S.move_direction = DirectionRandomRadius(S,x,y,dist);
  }
  else
  { ei.dist=SHOOT_RANGE+1;
    for (list<EnemyInfo>::iterator eit=enea.begin();eit!=enea.end();eit++) if ((*eit).dist<ei.dist) ei=*eit; // get the nearest enemy
    int dir;
    if (ei.dist<good_range) dir = DirectionRandomFrom(S.x,S.y,ei.x,ei.y);                  // try to reach the good_range
    else if (ei.dist>good_range) dir = DirectionRandomTo(S.x,S.y,ei.x,ei.y);
    else dir = 0;
    int x1=S.x,y1=S.y;
    PositionMove(x1,y1,dir);
    if (Distance(x,y,x1,y1)<=dist) S.move_direction = dir;
                              else S.move_direction = DirectionRandomRadius(S,x,y,dist);
  }
}
*/

int key(int x,int y) { return y*FIELD_WIDTH+x; }

void TBot(RobotState& S)
{
  set_parameters(S);

/*
  if (ITER>iter0)
  { 
    fprintf(stderr,"%4d: %d (%d) : %d B\n",iter0,max_msg_num,max_msg_num_want,max_msg_size);
    max_msg_num=0; max_msg_num_want=0; max_msg_size=0;
    iter0=ITER;
  }
  msg_num=0; msg_num_want=0; msg_size=0;
*/
  vector<int> sx,sy;
  { int x=FIELD_WIDTH,y=FIELD_HEIGHT;
    int vr=VIEW_RANGE;
    sx.push_back(vr); sx.push_back(vr); sx.push_back(x-vr); sx.push_back(x-vr);
    sy.push_back(vr); sy.push_back(y-vr); sy.push_back(vr); sy.push_back(y-vr);

    sx.push_back(3*vr); sx.push_back(3*vr); sx.push_back(x-3*vr); sx.push_back(x-3*vr);
    sy.push_back(3*vr); sy.push_back(y-3*vr); sy.push_back(3*vr); sy.push_back(y-3*vr);
  }

  // self-identification:
  int id = ((short*)S.memory)[0]; while (id==0) id = (short)rand();
  int num = ((short*)S.memory)[1];
  StatusType status = (StatusType)((short*)S.memory)[2];
  int extrastatus = ((short*)S.memory)[3];
  int *Data = (int*)S.memory + 3;
  int ibx = 0;      // my base: X
  int iby = 1;      // my base: Y
  int ipx = 2;      // my previous position: X
  int ipy = 3;      // my previous position: Y
  int itarget = 4;  // searcher: my target index
  int iptarget = 5; // searcher: my previous target index
  int itx = 6;      // my targey position: X
  int ity = 7;      // my targey position: Y
  list<MessageInfo> minfo;
  MessageInit(S.message,id,S.x,S.y,status);
  
  map<int,EnemyInfo> eall; // enemies
  list<EnemyInfo> enea; // enemies, dist <= SHOOT_RANGE
  list<EnemyInfo> efar; // enemies, dist > SHOOT_RANGE
  map<int,FriendInfo> frie; // friends

  int def_num=0;        // number of defenders
  int max_num=0;

  // radio info:
  //   read messages, gather friends, count defenders
  for (vector<void*>::iterator it=S.messages.begin();it!=S.messages.end();it++)
  { FriendInfo fi;
    MessageRead(*it,minfo,fi);
    if (fi.status==st_defender) def_num++;
    fi.dist=Distance(S.x,S.y,fi.x,fi.y);
    frie[key(fi.x,fi.y)]=fi;
  }
  //   gather all enemies from messages, count shoots, exclude duplicates
  for (list<MessageInfo>::iterator it=minfo.begin();it!=minfo.end();)
  { MessageInfo &mi=*it;
    if (mi.act==mt_shoot || mi.act==mt_enemy)
    { int shoot = mi.act==mt_shoot;
      int k = key(mi.x,mi.y);
      map<int,EnemyInfo>::iterator eit=eall.find(k);
      if (eit==eall.end()) 
      { EnemyInfo ei;
        int dis = Distance(S.x,S.y,mi.tx,mi.ty);
        ei.x=mi.tx; ei.y=mi.ty; ei.dist=dis; ei.attack=shoot; ei.base=0; ei.re=mi.d2; ei.ok=dis>VIEW_RANGE;
        eall[k]=ei; 
      }
      else eit->second.attack+=shoot;
      it=minfo.erase(it);
    }
    else it++;
  }

  // view info:
  //   append info about viewable robots: friends and enemies
  for (vector<Unit>::iterator it=S.robots.begin();it!=S.robots.end();it++)
  { Unit &u = *it;
    int dis = Distance(S.x,S.y,u.x,u.y);
    int k = key(u.x,u.y);
    if (u.team==S.team)
    { map<int,FriendInfo>::iterator fit=frie.find(k);
      if (fit==frie.end()) { FriendInfo fi; fi.id=0; fi.status=st_none; fi.x=u.x; fi.y=u.y; fi.dist=dis; fi.base=0; frie[k]=fi; }
    }
    else
    { map<int,EnemyInfo>::iterator eit=eall.find(k);
      if (eit==eall.end()) { EnemyInfo ei; ei.x=u.x; ei.y=u.y; ei.dist=dis; ei.attack=0; ei.base=0; ei.re=0; ei.ok=1; eall[k]=ei; }
                      else { eit->second.ok=1; }
    }
  }

  //   append info about viewable bases: friends and enemies
  for (vector<Unit>::iterator it=S.bases.begin();it!=S.bases.end();it++)
  { Unit &u = *it;
    int dis = Distance(S.x,S.y,u.x,u.y);
    int k = key(u.x,u.y);
    if (u.team==S.team)
    { map<int,FriendInfo>::iterator fit=frie.find(k);
      if (fit==frie.end()) { FriendInfo fi; fi.id=0; fi.status=st_none; fi.x=u.x; fi.y=u.y; fi.dist=dis; fi.base=1; frie[k]=fi; }
                      else fit->second.base=1;
    }
    else
    { map<int,EnemyInfo>::iterator eit=eall.find(k);
      if (eit==eall.end()) { EnemyInfo ei; ei.x=u.x; ei.y=u.y; ei.dist=dis; ei.attack=0; ei.base=1; ei.re=0; ei.ok=1; eall[k]=ei; }
                      else { eit->second.base=1; eit->second.ok=1; }
    }
  }

  // sort enemies and divide them into near and far excluding errorneous info
  while (!eall.empty())
  { 
    map<int,EnemyInfo>::iterator emin=eall.begin();
    int dmin=emin->second.dist;
    for (map<int,EnemyInfo>::iterator eit=eall.begin();eit!=eall.end();eit++)
      if (dmin>eit->second.dist) { dmin=eit->second.dist; emin=eit; }
    if (emin->second.ok)
      if (dmin<=SHOOT_RANGE) enea.push_back(emin->second);
                        else efar.push_back(emin->second);
    eall.erase(emin);
  }

/*
  // check whether I am mistaking about friend robots
  for (vector<Unit>::iterator it=S.robots.begin();it!=S.robots.end();it++)
  { Unit &u=*it;
    if (u.team==S.team) 
    {
      for (list<EnemyInfo>::iterator eit=enea.begin();eit!=enea.end();eit++)
      { EnemyInfo &ei=*eit;
        if (u.x==ei.x && u.y==ei.y) { printf("I think that my friend robot is near-enemy!\n"); exit(1); }
      }
      for (list<EnemyInfo>::iterator eit=efar.begin();eit!=efar.end();eit++)
      { EnemyInfo &ei=*eit;
        if (u.x==ei.x && u.y==ei.y) { printf("I think that my friend robot is far-enemy!\n"); exit(1); }
      }
    }
  }
  // check whether I am mistaking about friend bases
  for (vector<Unit>::iterator it=S.bases.begin();it!=S.bases.end();it++)
  { Unit &u=*it;
    if (u.team==S.team)
    {
      for (list<EnemyInfo>::iterator eit=enea.begin();eit!=enea.end();eit++)
      { EnemyInfo &ei=*eit;
        if (u.x==ei.x && u.y==ei.y) { printf("I think that my friend base is near-enemy!\n"); exit(1); }
      }
      for (list<EnemyInfo>::iterator eit=efar.begin();eit!=efar.end();eit++)
      { EnemyInfo &ei=*eit;
        if (u.x==ei.x && u.y==ei.y) { printf("I think that my friend base is far-enemy!\n"); exit(1); }
      }
    }
  }
*/

/*
  if (extrastatus==1)
  { for (list<MessageInfo>::iterator it=minfo.begin();it!=minfo.end();it++)
      if ((*it).act==mt_born)
      { num=(*it).d1+1;              // got my number
        status=st_born;
        mi.act=mt_born_answer;
        mi.d1=(*it).id;              // sign message with target id
        MessageAppend(S.message,mi);
        was_message=1;
        break;
      }
  }
*/

  // make decision
  switch (status)
  { case st_none: // just born
    { int was_message=0;
      int bx=S.x,by=S.y;
      status=st_born;
      for (list<MessageInfo>::iterator it=minfo.begin();it!=minfo.end();it++)
        if ((*it).act==mt_born)
        { bx=(*it).tx;
          by=(*it).ty;
          MessageInfo mi;
          num=(*it).d1+1;              // got my number
          mi.act=mt_born_answer;
//          mi.d1=(*it).id;              // sign message with target id
          mi.tx=bx;
          mi.ty=by;
          MessageAppend(S.message,mi);
          was_message=1;
          break;
        }
      if (!was_message) // I am the first in the world! Adam?
      {
        for (vector<Unit>::iterator it=S.bases.begin();it!=S.bases.end();it++)
        { Unit &u=*it;
          if (Distance(u.x,u.y,bx,by)==1) { bx=u.x; by=u.y; break; }
        }
        extrastatus = 1;
        num=1;
      }
      Data[ibx]=bx;
      Data[iby]=by;
      MoveDecisionRadius(S,enea,efar,bx,by,TRANSMIT_RANGE-1);
      ShootDecision(S,enea);
      break;
    }
    case st_born: // waiting for the next born
    { int was_message=0;
      int bx=Data[ibx];
      int by=Data[iby];
      for (list<MessageInfo>::iterator it=minfo.begin();it!=minfo.end();it++)
        if ((*it).act==mt_born_answer && (*it).tx==bx && (*it).ty==by) { was_message=1; break; }
//        if ((*it).act==mt_born_answer && (*it).d1==id) { was_message=1; break; }
      if (was_message)
      {
        // I'm ready to become someone!!!
        if (defend && def_num < def_need)
        { status = st_defender;
          ShootDecision(S,enea);
          MoveDecisionRadius(S,enea,efar,bx,by,def_range);
        }
        else  if (enea.empty() && num<=sea_num && max_num<=num)
        { status = st_searcher;
          Data[itarget] = rand()%sx.size();
          Data[iptarget] = -1;
          status = st_searcher;
          S.move_direction = DirectionRandomTo(S.x,S.y,sx[Data[itarget]],sy[Data[itarget]]);
            
//          printf("%d %d: Going to %d %d\n",id,num,sx[Data[itarget]],sy[Data[itarget]]);
        }
        else
        { status = st_trooper;
          ShootDecision(S,enea);
          MoveDecision(S,enea,efar);
        }
      }
      else
      {
        MessageInfo mi;
        mi.act=mt_born;
        mi.tx=bx;
        mi.ty=by;
        mi.d1=num;
        MessageAppend(S.message,mi);
        MoveDecisionRadius(S,enea,efar,bx,by,TRANSMIT_RANGE-1);
        ShootDecision(S,enea);
      }
      break;
    }
    case st_defender:
    { int bx=Data[ibx];
      int by=Data[iby];
      ShootDecision(S,enea);
      if (def_num > 3*def_need/4)
      { status = st_trooper;
        MoveDecision(S,enea,efar);
      }
      else
        MoveDecisionRadius(S,enea,efar,bx,by,def_range);
      break;
    }
    case st_searcher:
    { int tx=sx[Data[itarget]];
      int ty=sy[Data[itarget]];
      if (!enea.empty())
      { status=st_trooper;
        ShootDecision(S,enea);
        MoveDecision(S,enea,efar);
      }
      else if (Distance(S.x,S.y,tx,ty)<=1)
      { //sx.push_back(rand()%FIELD_WIDTH);
        //sy.push_back(rand()%FIELD_HEIGHT);
        int t;
        for (int i=0;i<100;i++) { t=rand()%sx.size(); if (t!=Data[itarget] && t!=Data[iptarget]) break; }
        Data[iptarget] = Data[itarget]; Data[itarget] = t;
        S.move_direction = DirectionRandomTo(S.x,S.y,sx[t],sy[t]);
      }
      else if (S.x==Data[ipx] && S.y==Data[ipy]) S.move_direction = DirectionRandom();
      else S.move_direction = DirectionRandomTo(S.x,S.y,sx[Data[itarget]],sy[Data[itarget]]);
      break;
    }
    case st_trooper:
    default:
    { 
      ShootDecision(S,enea);
      int yes=0;
      int x,y,dis=FIELD_HEIGHT+FIELD_WIDTH;
      if (enea.empty() && efar.empty() && !frie.empty()) // get position and distance to the nearest friend
      { int fnum=0;
        for (map<int,FriendInfo>::iterator fit=frie.begin();!yes && fit!=frie.end();fit++)
        { FriendInfo &f=fit->second;
          if (f.dist<dis) { dis=f.dist; x=f.x; y=f.y;}
          if (f.dist<=f_range) fnum++;
          yes = fnum >= f_ok;
        }
      }
      if (yes)
      { if (rand()%searcher_probability==0)
        { status = st_searcher;
          Data[itarget] = rand()%sx.size();
          Data[iptarget] = -1;
          S.move_direction = DirectionRandomTo(S.x,S.y,sx[Data[itarget]],sy[Data[itarget]]);
        }
        else
        { 
          if (dis > TRANSMIT_RANGE-1) S.move_direction = DirectionRandomTo(S.x,S.y,x,y);  //----------------------------------------------------------------------------
//          if (dis < TRANSMIT_RANGE-1) S.move_direction = DirectionRandomFrom(S.x,S.y,x,y);
          else 
          MoveDecision(S,enea,efar);
        }
      }
      else
      { 
//        if (dis > TRANSMIT_RANGE+1) S.move_direction = DirectionRandomTo(S.x,S.y,x,y);  //----------------------------------------------------------------------------
//        if (dis < TRANSMIT_RANGE-1) S.move_direction = DirectionRandomFrom(S.x,S.y,x,y);
//        else 
        MoveDecision(S,enea,efar);
      }
    }
  }

  for (list<EnemyInfo>::iterator eit=enea.begin();eit!=enea.end();eit++) // transmit nearby enemies
    if ((*eit).re < re_max)
    { EnemyInfo &ei=*eit;
      MessageInfo mi;
      if (S.shoot_x==ei.x && S.shoot_y==ei.y) mi.act = mt_shoot;
                                         else mi.act = mt_enemy;
      mi.tx = ei.x;
      mi.ty = ei.y;
      mi.d1 = ei.base;
      mi.d2 = ei.re+1;
      MessageAppend(S.message,mi);
    }
  for (list<EnemyInfo>::iterator eit=efar.begin();eit!=efar.end();eit++) // transmit far enemies
    if ((*eit).re < re_max)
    { EnemyInfo &ei=*eit;
      MessageInfo mi;
      mi.act = mt_enemy;
      mi.tx = ei.x;
      mi.ty = ei.y;
      mi.d1 = ei.base;
      mi.d2 = ei.re+1;
      MessageAppend(S.message,mi);
    }
  MessageFinalize(S.message,id,status);

  S.transmit = true;

  // self-memorizing:
  ((short*)S.memory)[0] = id;
  ((short*)S.memory)[1] = num;
  ((short*)S.memory)[2] = status;
  ((short*)S.memory)[3] = extrastatus;
  Data[ipx] = S.x;
  Data[ipy] = S.y;

//  printf("%10d, %2d: ",id,num); for (int i=0;i<10;i++) printf("%10d ",((int*)S.message)[i]); printf("\n");
//  printf("%3d: %10d, %2d, %2d:",iter,id,num,status); for (vector<MessageInfo>::iterator it=minfo.begin();it!=minfo.end();it++) printf(" %d %d %d %d %d |",(*it).id,(*it).x,(*it).y,(*it).act,(*it).d1); printf("\n");

//  if (msg_num_want>max_msg_num_want) { max_msg_num=msg_num; max_msg_num_want=msg_num_want; max_msg_size=msg_size; }
  
//  if (status!=st_born && status!=st_searcher && status!=st_trooper) { printf("Strange status: %d\n",status); exit(1); }

/*
  // check decision
  if (S.shoot && S.shoot_x==S.x && S.shoot_y==S.y) { printf("I shoot myself!\n"); exit(1); }

  for (vector<Unit>::iterator it=S.robots.begin();it!=S.robots.end();it++)
  { Unit &u=*it;
    if (S.shoot && u.x==S.shoot_x && u.y==S.shoot_y && u.team==S.team) { printf("I shoot my friend!\n"); exit(1); }
  }

  for (vector<Unit>::iterator it=S.bases.begin();it!=S.bases.end();it++)
  { Unit &u=*it;
    if (S.shoot && u.x==S.shoot_x && u.y==S.shoot_y && u.team==S.team) { printf("I shoot my base!\n"); exit(1); }
  }
*/

}

}