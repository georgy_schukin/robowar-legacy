#include "Robot.h"
#include <iostream>
#include <stdlib.h>
#include <string.h>

using namespace std;

#ifdef _WIN32
#include <Windows.h>
#define usleep(x) Sleep(x/1000)
#define random() rand()
#endif

namespace Bot
{

	void Bot(RobotState& S)
	{
		S.move_direction = random()%7;
		if(S.bases.empty()==false)
		{
			for(vector<Unit>::iterator it = S.bases.begin(); it != S.bases.end(); it++)
			{
				Unit& b = *it;
				if(b.team != S.team)
				{
					S.shoot = true;
					S.shoot_x = b.x;
					S.shoot_y = b.y;
					S.move_direction = 0;
				}
			}
		}
		if(S.robots.empty()==false)
		{
			for(vector<Unit>::iterator it = S.robots.begin(); it != S.robots.end(); it++)
			{
				Unit& b = *it;
				if(b.team != S.team)
				{
					S.shoot = true;
					S.shoot_x = b.x;
					S.shoot_y = b.y;
					S.move_direction = 0;
				}
			}

		}
	}
}
