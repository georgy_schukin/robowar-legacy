#include "Robot.h"
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#ifdef _WIN32
#include <Windows.h>
#define usleep(x) Sleep(x/1000)
#define random() rand()
#endif

using namespace std;

namespace SVPRO

{

int get_friends (RobotState& S)
{
	int res=0;
	for (vector<Unit>::iterator it = S.robots.begin(); it != S.robots.end(); it++)
		if (S.team == it->team)
			res++;
	return res;
}
int get_enemies (RobotState& S)
{
	int res=0;
	for (vector<Unit>::iterator it = S.robots.begin(); it != S.robots.end(); it++)
		if (S.team != it->team)
			res++;
	return res;
}
int get_fr_b (RobotState& S)								
{	
	int res=0;
	for (vector<Unit>::iterator it = S.bases.begin(); it != S.bases.end(); it++)
		if (S.team == it->team)
			res++;
	return res;
}
int get_en_b (RobotState& S) 
{ 
	int res=0;
	for (vector<Unit>::iterator it = S.bases.begin(); it != S.bases.end(); it++)
		if (S.team != it -> team)
			res++;
	return res;
}
void correct_traectory (RobotState& S, int& TRAECTORY_STATUS)
{
	if (S.y - VIEW_RANGE < 0)
	{
		if (S.x - VIEW_RANGE < 0)
			TRAECTORY_STATUS = random()%5;
		else
		if (S.x + VIEW_RANGE > FIELD_WIDTH)
			TRAECTORY_STATUS = random()%3 + 5;
		else
			TRAECTORY_STATUS = random()%7 + 1;
	}
	else
	if (S.y + VIEW_RANGE > FIELD_HEIGHT)
	{
		if (S.x - VIEW_RANGE < 0)
		{
			TRAECTORY_STATUS = random()%3 - 1;
			if(TRAECTORY_STATUS < 0)
				TRAECTORY_STATUS += 12;
		}
		else
		if (S.x + VIEW_RANGE > FIELD_WIDTH)
			TRAECTORY_STATUS = random()%3 + 8;
		else
		{
			TRAECTORY_STATUS = random()%6 + 8;
			if(TRAECTORY_STATUS > 12)
				TRAECTORY_STATUS -= 12;
		}
	}
	else
	{
		if (S.x - VIEW_RANGE < 0)
		{
			TRAECTORY_STATUS = random()%6 - 1;
			if(TRAECTORY_STATUS < 0)
				TRAECTORY_STATUS += 12;
		}
		else
		if (S.x + VIEW_RANGE > FIELD_WIDTH)
			TRAECTORY_STATUS = random()%6 + 5;
	}
}
void move_strategy (RobotState& S)
{
	int TRAECTORY_STATUS = ITER;
	int TRAECTORY_MEMORY = 0;
	int b_x;
	int b_y;
	int * mem = (int *) S.memory;
	int tx, ty;
	TRAECTORY_STATUS = ITER/12;
	if (mem[0] == 0)
		{
			mem[0] = TRAECTORY_STATUS;
			mem[1] = TRAECTORY_MEMORY;
		}
	switch (mem[0])
	{
		case 1: 
		{	
			if (mem[1]%2 == 0)
			{
				S.move_direction = 6; mem[1]++;
			}
			else 
				if (mem[1]%2 == 1)
				{
					S.move_direction = 2; mem[1]++;
				}
			if (mem[0] == TRAECTORY_STATUS)	S.move_direction = 6;
			else	
				if (mem[0] == TRAECTORY_STATUS - 1)	S.move_direction = 2;
		break;
		}
		case 2:
		{	
			if (mem[1]%2 == 0)
			{
				S.move_direction = 2; mem[1]++;
			}
			else 
				if (mem[1]%2 == 1)
				{
					S.move_direction = 6; mem[1]++;
				}
			if (mem[0] == TRAECTORY_STATUS) S.move_direction = 2;
			else	
				if (mem[0] == TRAECTORY_STATUS - 1) S.move_direction = 6;
		break;
		}
		case 3:
		{	
			if (mem[1]%3 == 0)
				{
					S.move_direction = 3; mem[1]++;
				}
			else	
				if (mem[1]%3 == 1)
					{
						S.move_direction = 2; mem[1]++;
					}
				else
					if (mem[1]%3 == 2)
						{
							S.move_direction = 1; mem[1]++;
						}
			if (mem[0] == TRAECTORY_STATUS)	S.move_direction = 3;
			else	
				if (mem[0] == TRAECTORY_STATUS - 1) S.move_direction = 2;
				else
					if (mem[0] == TRAECTORY_STATUS - 2)	S.move_direction = 1;
		break;
		}
		case 4:
		{	
			if (mem[1]%3 == 0)
				{
					S.move_direction = 4; mem[1]++;
				}
			else	
				if (mem[1]%3 == 1)
					{
						S.move_direction = 2; mem[1]++;
					}
				else
					if (mem[1]%3 == 2)
						{
							S.move_direction = 3; mem[1]++;
						}
			if (mem[0] == TRAECTORY_STATUS && mem[1] == 0) S.move_direction = 3;
			else	
				if (mem[0] == TRAECTORY_STATUS - 1) S.move_direction = 3;
		break;
		}	
		case 5:
		{	
			if (mem[1]%2 == 0)
				{
					S.move_direction = 2; mem[1]++;
				}
			else	
				if (mem[1]%2 == 1)
					{
						S.move_direction = 4; mem[1]++;
					}
			if (mem[0] == TRAECTORY_STATUS)	S.move_direction = 3;
			else	
				if (mem[0] == TRAECTORY_STATUS - 1)	S.move_direction = 4;
		break;
		}
		case 6:
		{	
			if (mem[1]%2 == 0)
				{
					S.move_direction = 4; mem[1]++;
				}
			else	
				if (mem[1]%2 == 1)
					{
						S.move_direction = 2; mem[1]++;
					}
			if (mem[0] == TRAECTORY_STATUS)	S.move_direction = 3;
			else	
				if (mem[0] == TRAECTORY_STATUS - 1) S.move_direction = 4;
		break;
		}
		case 7:
		{	
			if (mem[1]%2 == 0)
				{
					S.move_direction = 3; mem[1]++;
				}
			else	
				if (mem[1]%2 == 1)
					{
						S.move_direction = 5; mem[1]++;
					}
			if (mem[0] == TRAECTORY_STATUS)	S.move_direction = 3;
			else	
				if (mem[0] == TRAECTORY_STATUS - 1) S.move_direction = 4;
				else
					if (mem[0] == TRAECTORY_STATUS - 2)	S.move_direction = 5;
					else
						if (mem[0] == TRAECTORY_STATUS - 3)	S.move_direction = 5;
		break;
		}
		case 8:
		{	
			if (mem[1]%2 == 0)
				{
					S.move_direction = 5; mem[1]++;
				}
			else	
				if (mem[1]%2 == 1)
					{
						S.move_direction = 3; mem[1]++;
					}
			if (mem[0] == TRAECTORY_STATUS)	S.move_direction = 3;
			else	
				if (mem[0] == TRAECTORY_STATUS - 1)	S.move_direction = 4;
		break;
		}
		case 9:
		{	
			if (mem[1]%2 == 0)
				{
					S.move_direction = 4; mem[1]++;
				}
			else	
				if (mem[1]%2 == 1)
					{
						S.move_direction = 6; mem[1]++;
					}
			if (mem[0] == TRAECTORY_STATUS) S.move_direction = 5;
			else	
				if (mem[0] == TRAECTORY_STATUS - 1) S.move_direction = 4;
		break;
		}
		case 10:
		{	
			if (mem[1]%2 == 0)
				{
					S.move_direction = 6; mem[1]++;
				}
			else	
				if (mem[1]%2 == 1)
					{
						S.move_direction = 4; mem[1]++;
					}
			if (mem[0] == TRAECTORY_STATUS)	S.move_direction = 5;
			else	
				if (mem[0] == TRAECTORY_STATUS - 1) S.move_direction = 4;
		break;
		}
		case 11:
		{	
			if (mem[1]%3 == 0)
				{
					S.move_direction = 5; mem[1]++;
				}
			else	
				if (mem[1]%3 == 1)
					{
						S.move_direction = 6; mem[1]++;
					}
				else
					if (mem[1]%3 == 2)
						{
							S.move_direction = 1; mem[1]++;
						}
		break;
		}
		case 12:
		{	
			if (mem[1]%3 == 0)
				{
					S.move_direction = 6; mem[1]++;
				}
			else	
				if (mem[1]%3 == 1)
					{
						S.move_direction = 5; mem[1]++;
					}
				else
					if (mem[1]%3 == 2)
					{
						S.move_direction = 4; mem[1]++;
					}
			if (mem[0] == TRAECTORY_STATUS)	S.move_direction = 5;
			else	
				if (mem[0] == TRAECTORY_STATUS - 1) S.move_direction = 5;
				else
					if (mem[0] == TRAECTORY_STATUS - 2) S.move_direction = 4;
			break;
		}
		default: S.move_direction = random()%7;
	}
	while (shift (S.x, S.y, S.move_direction, b_x, b_y) == false)
	{	
		S.move_direction++;
		if (S.move_direction > 6)
			S.move_direction -=6;
	}
	correct_traectory (S, mem[0]);
}
void attack_target (RobotState& S, int b_x, int b_y, bool enemy)
{
	if (!enemy)
	{	
		S.shoot = true;
		S.shoot_x = b_x;
		S.shoot_y = b_y;
	}
	else
		if (enemy)
			S.shoot = false;
}
void mes_spam_viev (RobotState& S, int b_x, int b_y, int itr, int iterator_mod)
{
	S.transmit = true;
	int* mes = (int*)S.message;
	mes[0] = b_x;
	mes[1] = b_y;
	mes[2] = itr;
	mes[3] = iterator_mod;
}
bool read_message (RobotState& S, int& m_x, int& m_y, int& itr, int& iterator_mod)
{
	int i = 0;
	int distance;
	for (vector<void*>::iterator it = S.messages.begin(); it != S.messages.end(); it++)
	{
		int *msg = (int*)*it;
		if (ITER - msg[2] < msg[3])
		{
			if (i == 0)
			{
				m_x = msg[0];
				m_y = msg[1]; 
				itr = msg[2];
				iterator_mod = msg[3];
				i++;
				distance = dist (S.x, S.y, m_x, m_y);
			}
			if (dist (S.x, S.y, msg[0], msg[1]) <= distance && i > 0)
			{
				m_x = msg[0];
				m_y = msg[1];
				itr = msg[2];
				iterator_mod = msg[3];
			}
		}
	}
	if (i > 0)
		return true;
	return false;
}
void work_and_target_r (RobotState& S, int& b_x, int& b_y) 
{
	int i = 0;
	int distance;
	for (vector<Unit>::iterator it = S.robots.begin(); it != S.robots.end(); it++)
	{
		Unit& b = *it;
		if(b.team != S.team && i == 0)
		{
			b_x = b.x;
			b_y = b.y;
			i++;
			distance = dist (S.x, S.y, b_x, b_y);
		}
		if (b.team != S.team && dist (b.x, b.y, S.x, S.y) <= distance && i > 0)
		{	
			b_x = b.x;
			b_y = b.y;
		}					
	}
}
void work_and_target_b (RobotState& S, int& b_x, int& b_y) 
{
	int i = 0;
	int distance;
	for (vector<Unit>::iterator it = S.bases.begin(); it != S.bases.end(); it++)
	{
		Unit& b = *it;
		if (b.team != S.team && i == 0)
		{
			b_x = b.x;
			b_y = b.y;
			i++;
			distance = dist (S.x, S.y, b_x, b_y);
		}
		if (b.team != S.team && dist (b.x, b.y, S.x, S.y) <= distance && i > 0)
		{	
			b_x = b.x;
			b_y = b.y;
		}				
	}
}
bool work_and_target_f (RobotState& S, int& b_x, int& b_y) 
{
	int i = 0;
	int distance;
	for (vector<Unit>::iterator it = S.robots.begin(); it != S.robots.end(); it++)
	{
		Unit& b = *it;
		if(b.team == S.team && i == 0)
		{
			b_x = b.x;
			b_y = b.y;
			i++;
			distance = dist (S.x, S.y, b_x, b_y);
		}
		if (b.team == S.team && dist (b.x, b.y, S.x, S.y) <= distance && i > 0)
		{	
			b_x = b.x;
			b_y = b.y;
		}					
	}
	if (i > 0)
		return true;
	return false;
}
void correct_move (RobotState& S, int b_x, int b_y, bool circle)
{
	if (circle)
		S.move_direction = get_dir (b_x, b_y, S.x, S.y);
	else
		S.move_direction = get_dir (S.x, S.y, b_x, b_y);
	int bs_x;	int bs_y;
	if (shift (S.x, S.y, S.move_direction, bs_x, bs_y) == false)
		S.move_direction = S.move_direction++;
	if (S.move_direction > 6)
		S.move_direction-=5;
}
void move_distance_control (RobotState& S)
{
	int f_x;	int f_y;
	if (work_and_target_f (S, f_x, f_y) == true)
	{
		int distance = dist (S.x, S.y, f_x, f_y);
		if (distance < TRANSMIT_RANGE - 2)
			move_strategy (S);
		else
			correct_move (S, f_x, f_y, false);
	}
	else
		move_strategy (S);
}


void R0_B0_M0 (RobotState& S)
{
	move_strategy (S);
}
void R0_B0_M1 (RobotState& S, int m_x, int m_y, int itr, int iterator_mod)
{
	correct_move (S, m_x, m_y, false);
	mes_spam_viev (S, m_x, m_y, itr, iterator_mod);
}
void R0_B1_M0 (RobotState& S)
{
	int e_b = get_en_b (S);
	int b_x;	int b_y;
	work_and_target_b (S, b_x, b_y);
	if (e_b != 0)
	{
		if (dist (S.x, S.y, b_x, b_y) <= SHOOT_RANGE - 1 -1)	
		{
			attack_target (S, b_x, b_y, false);
			S.move_direction = 0;
		}
		else
			correct_move (S, b_x, b_y, false);
		mes_spam_viev (S, b_x, b_y, ITER, 6);
	}
	else
		move_strategy (S);
}
void R0_B1_M1 (RobotState& S, int m_x, int m_y, int itr, int iterator_mod)
{
	int e_b = get_en_b (S);
	int f_b = get_fr_b (S);
	int b_x;	int b_y;
	work_and_target_b (S, b_x, b_y);
	if (e_b != 0)
	{
		if (dist (S.x, S.y, b_x, b_y) < dist (S.x, S.y, m_x, m_y))
		{
			if (dist (S.x, S.y, b_x, b_y) <= SHOOT_RANGE - 1)	
			{
				attack_target (S, b_x, b_y, false);
				S.move_direction = 0;
			}
			else
				correct_move (S, b_x, b_y, false);
			mes_spam_viev (S, b_x, b_y, ITER, 6);
		}
		else
		{
			correct_move (S, m_x, m_y, false);
			mes_spam_viev (S, m_x, m_y, ITER, iterator_mod);
		}
	}
	else
	{
		correct_move (S, m_x, m_y, false);
		mes_spam_viev (S, m_x, m_y, itr, iterator_mod);
	}
}
void R1_B0_M0 (RobotState& S)
{	
	int e = get_enemies (S);
	int f = get_friends (S);
	int r_x;
	int r_y;
	int m_x;
	int m_y;
	if (e != 0)
	{
		work_and_target_r (S, r_x, r_y);
		if (e > f)
		{
			if (dist (S.x, S.y, r_x, r_y) <= SHOOT_RANGE)
				attack_target (S, r_x, r_y, false);
			correct_move (S, r_x, r_y, true);
		}
		else
		{
			if (dist (S.x, S.y, r_x, r_y) <= SHOOT_RANGE -1)	
			{
				attack_target (S, r_x, r_y, false);
				S.move_direction = 0;
			}
			else
				correct_move (S, r_x, r_y, false);
		}
		mes_spam_viev (S, r_x, r_y, ITER, 3);
	}	
	else
		move_strategy (S);
}
void R1_B0_M1 (RobotState& S, int m_x, int m_y, int itr, int iterator_mod)
{
	int e = get_enemies (S);
	int f = get_friends (S);
	int r_x;	int r_y;
	if (e != 0)
	{
		work_and_target_r (S, r_x, r_y);
		if (dist (S.x, S.y, r_x, r_y) < dist (S.x, S.y, m_x, m_y))
		{
			if (e > f)
			{
				if (dist (S.x, S.y, r_x, r_y) <= SHOOT_RANGE)
					attack_target (S, r_x, r_y, false);
				correct_move (S, r_x, r_y, true);
			}
			else
			{
				if (dist (S.x, S.y, r_x, r_y) <= SHOOT_RANGE)	
				{
					attack_target (S, r_x, r_y, false);
					S.move_direction = 0;
				}
				else
					correct_move (S, r_x, r_y, false);
			}
			mes_spam_viev (S, r_x, r_y, ITER, 3);
		}
		else
		{
			correct_move (S, m_x, m_y, false);
			mes_spam_viev (S, m_x, m_y, itr, iterator_mod);
		}
	}
	else
	{
		correct_move (S, m_x, m_y, false);
		mes_spam_viev (S, m_x, m_y, itr, iterator_mod);
	}
}
void R1_B1_M0 (RobotState& S)
{
	int e = get_enemies (S);
	int f = get_friends (S);
	int e_b = get_en_b (S);
	int f_b = get_fr_b (S);
	int r_x;
	int r_y;
	int b_x;
	int b_y;
	work_and_target_r (S, r_x, r_y);
	work_and_target_b (S, b_x, b_y);
	if (e_b != 0)
	{
		if (e != 0)
		{
			if (dist (S.x, S.y, r_x, r_y) < dist (S.x, S.y, b_x, b_y))
			{
				if (e > f)
				{
					if (dist (S.x, S.y, r_x, r_y) <= SHOOT_RANGE)
						attack_target (S, r_x, r_y, false);
					correct_move (S, r_x, r_y, true);
				}
				else
				{
					if (dist (S.x, S.y, r_x, r_y) <= SHOOT_RANGE)	
					{
						attack_target (S, r_x, r_y, false);
						S.move_direction = 0;
					}
					else
						correct_move (S, r_x, r_y, false);
				}
				mes_spam_viev (S, r_x, r_y, ITER, 3);
			}
			else
			{
				if (dist (S.x, S.y, b_x, b_y) <= SHOOT_RANGE - 1)	
				{
					attack_target (S, b_x, b_y, false);
					S.move_direction = 0;
				}
				else
					correct_move (S, b_x, b_y, false);
				mes_spam_viev (S, b_x, b_y, ITER, 6);
			}
		}
	}	
	else
	{	
		if (e != 0)
		{
			if (e > f)
			{
				if (dist (S.x, S.y, r_x, r_y) <= SHOOT_RANGE)
					attack_target (S, r_x, r_y, false);
				correct_move (S, r_x, r_y, true);
			}
			else
			{
				if (dist (S.x, S.y, r_x, r_y) <= SHOOT_RANGE)	
				{
					attack_target (S, r_x, r_y, false);
					S.move_direction = 0;
				}
				else
					correct_move (S, r_x, r_y, false);
			}
			mes_spam_viev (S, r_x, r_y, ITER, 3);
		}
		else
			move_strategy (S); (S);
	}
}
void R1_B1_M1 (RobotState& S, int m_x, int m_y, int itr, int iterator_mod)
{
	int e = get_enemies (S);
	int f = get_friends (S);
	int e_b = get_en_b (S);
	int f_b = get_fr_b (S);
	int r_x;	int r_y;
	int b_x;	int b_y;
	work_and_target_r (S, r_x, r_y);
	work_and_target_b (S, b_x, b_y);
	if (e_b != 0)
	{
		if (e != 0)
		{
			if (dist (S.x, S.y, r_x, r_y) < dist (S.x, S.y, b_x, b_y))
			{
				if (e > f)
				{
					if (dist (S.x, S.y, r_x, r_y) <= SHOOT_RANGE)
						attack_target (S, r_x, r_y, false);
					correct_move (S, r_x, r_y, true);
				}
				else
				{
					if (dist (S.x, S.y, r_x, r_y) <= SHOOT_RANGE)	
					{
						attack_target (S, r_x, r_y, false);
						S.move_direction = 0;
					}
					else
						correct_move (S, r_x, r_y, false);
				}
				mes_spam_viev (S, r_x, r_y, ITER, 3);
			}
			else
			{
				if (dist (S.x, S.y, b_x, b_y) <= SHOOT_RANGE - 1)	
				{
					attack_target (S, b_x, b_y, false);
					S.move_direction = 0;
				}
				else
					correct_move (S, b_x, b_y, false);
				mes_spam_viev (S, b_x, b_y, ITER, 6);
			}
		}
		else
		{
			if (dist (S.x, S.y, b_x, b_y) <= SHOOT_RANGE - 1)	
			{
				attack_target (S, b_x, b_y, false);
				S.move_direction = 0;
			}
			else
				if (dist (S.x, S.y, b_x, b_y) > SHOOT_RANGE)
					correct_move (S, b_x, b_y, false);
			mes_spam_viev (S, b_x, b_y, ITER, 6);
		}		
	}
	else
	{	
		if (e != 0)
		{
			if (dist (S.x, S.y, r_x, r_y) < dist (S.x, S.y, m_x, m_y))
			{
				if (e > f)
				{
					if (dist (S.x, S.y, r_x, r_y) <= SHOOT_RANGE)
						attack_target (S, r_x, r_y, false);
					correct_move (S, r_x, r_y, true);
				}
				else
				{
					if (dist (S.x, S.y, r_x, r_y) <= SHOOT_RANGE)	
					{
						attack_target (S, r_x, r_y, false);
						S.move_direction = 0;
					}
					else
						correct_move (S, r_x, r_y, false);
				}
				mes_spam_viev (S, r_x, r_y, ITER, 3);
			}
			else
			{	
				correct_move (S, m_x, m_y, false);
				mes_spam_viev (S, m_x, m_y, itr, iterator_mod);
			}
		}
		else
		{
			correct_move (S, m_x, m_y, false);
			mes_spam_viev (S, m_x, m_y, itr, iterator_mod);
		}
	}
}


void SVPRO(RobotState& S)
{
	int m_x;	int m_y;
	int itr;	int iterator_mod;
	bool ident_mes = read_message (S, m_x, m_y, itr, iterator_mod);
	if (S.robots.empty() == true)
		if (S.bases.empty() == true)
			if (ident_mes == false)
				R0_B0_M0(S);
			else
				R0_B0_M1(S, m_x, m_y, itr, iterator_mod);
		else
			if (ident_mes == false)
				R0_B1_M0(S);
			else
				R0_B1_M1(S, m_x, m_y, itr, iterator_mod);
	else
		if (S.bases.empty() == true)
			if (ident_mes == false)
				R1_B0_M0(S);
			else
				R1_B0_M1(S, m_x, m_y, itr, iterator_mod);
		else
			if (ident_mes == false)
				R1_B1_M0(S);
			else
				R1_B1_M1(S, m_x, m_y, itr, iterator_mod);
}
}