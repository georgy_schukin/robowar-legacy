#include "Robot.h"
#include <time.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdio.h>

using namespace std;

namespace DataBot{ 

int get_friends(RobotState& S)
	{
		int res=0;
		for(vector<Unit>::iterator it = S.robots.begin();
				it != S.robots.end(); it++)
			if(S.team == it->team)
				res++;
		return res;
	}

int get_enemies(RobotState& S)
	{
		int res=0;
		for(vector<Unit>::iterator it = S.robots.begin();
				it != S.robots.end(); it++)
			if(S.team != it->team)
				res++;
		return res;
	}


Unit get_crd(int x,int y, int dir){
Unit a;
    switch (dir) {
    case 1: 
    a.x=x+ITER;
    a.y = y; 
    break;
    case 2:
    a.y = y+ITER;
    a.x = x;
    break;
    case 3:
    a.y = y+ITER;
    a.x = x-ITER;
    break;
    case 4:
    a.y = y;
    a.x = x-ITER;
    break;
        case 5:
    a.y = y-ITER;
    a.x = x-ITER;
    break;
        case 6:
    a.y = y-ITER;
    a.x = x;
    break;
    }
    return a;
}


int get_place(int x,int y)
{
if (x>(FIELD_WIDTH/2))
{
	if (y>(FIELD_HEIGHT/2))
		return 4;
	else 
		return 1;
}
else
{
if (y>(FIELD_HEIGHT/2))
		return 3;
	else 
		return 2;
}
}
Unit get_out (int x,int y)
{
Unit res;
switch(get_place(x,y))
	{
	case 1:
		if(ITER%2)
			return get_crd(x,y,1);
		else
			return get_crd(x,y,6);
		break;

	case 2:
		if(ITER%2)
			return get_crd(x,y,4);
		else
			return get_crd(x,y,5);
		break;
	case 3:
		if(ITER%2)
			return get_crd(x,y,3);
		else
			return get_crd(x,y,4);
		break;
	case 4:
		if(ITER%2)
			return get_crd(x,y,1);
		else
			return get_crd(x,y,2);
		break;

	}
}

bool iscenter(int x,int y)
{
return ((x>((FIELD_WIDTH/2)-10)) && (x<FIELD_WIDTH/2+10) && (y>FIELD_HEIGHT/2-10) && (y<FIELD_HEIGHT/2+10));
}
void DataBot(RobotState& S)
{
	int r;
	S.transmit = false;
	int f = get_friends(S);
	int e = get_enemies(S);
	int* mymem = (int*)S.memory;
	//int* msg = (int*)S.messages[0];
	int* mem = (int*)S.message;
	int *mainmsg=NULL;
	Unit base;
	bool isbase=0;
	int p=-1, dir = -1;
	bool nearBase = false;
	//const int E=0, P=2, BE=3, BF=1; 
	const int E=2, P=1, BE=3, BF=0; 

	//��������� ���������
	if(!S.messages.empty())
	{	
		int* msg = (int*)S.messages[0];
		int *msgnow;

		for(vector<void *>::iterator it = S.messages.begin(); it != S.messages.end(); it++)
		{
			int *msg = (int *)(*it);
			int tp = msg[0];

			if(tp>p)			{
				p=tp;
				mainmsg=msg;
				}
			else if ((tp==p)&&(msg[4]>mainmsg[4]))
					mainmsg=msg;
		}
		if (mymem[0]==6)
		    dir=get_dir(S.x, S.y, mymem[1], mymem[2]);
		else
		{
		    mymem[0] = mainmsg[0];
		    dir = get_dir(S.x, S.y, mainmsg[1], mainmsg[2]);
		}
	}
	//��������
	bool enbases=0;

	if(!S.bases.empty())
	{
		isbase=1;
		mymem[0]=0;
		for(vector<Unit>::iterator it = S.bases.begin(); it != S.bases.end(); it++)
		{
			Unit& b=*it;
			if(b.team!=S.team)
			{
				enbases=1;
				S.shoot = true;
				S.shoot_x = b.x;
				S.shoot_y = b.y;
				base=b;
				if(dir!=-1)
					dir = get_dir(S.x, S.y, b.x, b.y);
				break;

			}
			else
			{
				if(dist(S.x,S.y,b.x,b.y)<ceil((double)VIEW_RANGE/2))
					nearBase = true;
				base=b; //��� �� ���� ��������� ���� - ���� ��� ������ ����
			}
		}
	}
		

			if(!S.robots.empty())
			{
				mymem[0]=0;
				for(vector<Unit>::iterator it = S.robots.begin(); it != S.robots.end(); it++)
				{
					Unit& b = *it;
					if(b.team != S.team)
					{

						S.shoot = true;
						S.shoot_x = b.x;
						S.shoot_y = b.y;
						if(dir != -1)
							dir = get_dir(S.x, S.y, b.x, b.y);
						break;
					}
				}
			}
		
	
	//�������� ���������
	if(!S.transmit){
		if(mainmsg!=NULL){
		if((ITER-mainmsg[3])<8)
		{
			memcpy(S.message, mainmsg, MSG_SIZE);
			S.transmit = true;
		}
		}
		if (isbase)
		{
			if(base.team==S.team)
			{
				if((e > f) || (f < (VIEW_RANGE)))	
				{
					mem[0] = BF;
					mem[1] = base.x;
					mem[2] = base.y;
					
				}
				else if(mymem[0]!=6)
				{
				mymem[0]=6;
				mem[0]=P;
				if(f>3*VIEW_RANGE&&iscenter(S.x,S.y))
				{
				
				    mem[1]=mymem[1]=(get_out(S.x,S.y)).x;
				    mymem[2]=mem[2]=(get_out(S.x,S.y)).y;
				
				}
				else
				{
				    mem[1]=mymem[1]=((FIELD_WIDTH - base.x)*ITER/10)%FIELD_WIDTH;
				    mymem[2]=mem[2]=((FIELD_HEIGHT - base.y)*ITER/10)%FIELD_HEIGHT;
				}
				}
			}
			else
			{
				mem[0]=BE;
				mem[1] = base.x;
				mem[2] = base.y;
			}
		}
		else
		{
			if(e)
			{
				mem[0]=E;
				mem[1]=S.x;
				mem[2]=S.y;
			}
			else if(f >  VIEW_RANGE)
			{
				if(mymem[0]!=6)
				{
				mymem[0]=6;
				mem[0]=P;
				if(f>3*VIEW_RANGE&&iscenter(S.x,S.y))
				{
				    mem[1]=mymem[1]=(get_out(S.x,S.y)).x;
				    mymem[2]=mem[2]=(get_out(S.x,S.y)).y;
				}
				else
				{
				mymem[1]=mem[1]=((FIELD_WIDTH - S.x)*ITER/10)%FIELD_WIDTH;
				mymem[2]=mem[2]=((FIELD_HEIGHT - S.y)*ITER/10)%FIELD_HEIGHT;
				}
				}
			}
		}
		mem[3] = ITER;	
		mem[4] = e;
		mem[5] = f;
		S.transmit = true;
	}

	if(e >= f) { // runaway
		for(vector<Unit>::iterator it = S.robots.begin(); it != S.robots.end(); it++) {
			dir = get_dir(S.x, S.y, (*it).x, (*it).y) + 3;
			if(dir > 6)
				dir -= 6;
		}
	}
	else if((e == 0)&&(dir==-1)) {
		int max_dist = 0, min_dist = VIEW_RANGE, _dir, _dist = VIEW_RANGE;
		for(vector<Unit>::iterator it = S.robots.begin(); it != S.robots.end(); it++) {
			//max_dist = _dir > max_dist ? _dir : max_dist; 
			//min_dist = _dir < min_dist ? _dir : min_dist;

			if(int _dist1 = dist(S.x, S.y, (*it).x, (*it).y) < min_dist){
				min_dist = _dist1;
				_dir = get_dir(S.x, S.y, (*it).x, (*it).y);
			}

		}
		dir = _dir + 3;
		if(dir > 6)	
			dir -= 6;

		int dx, dy;
		if(!shift(S.x, S.y, dir, dx, dy))
			dir = -1;
	}
	if(nearBase){
		
		int n;
		if (base.x>FIELD_WIDTH/2)
		{
			if (base.y>FIELD_HEIGHT/2)
				n=2;
			else
				n=2;
		}
		else
		{
			if (base.y>FIELD_HEIGHT/2)
				n=2;
			else
				n=3;
		}

		dir = get_dir(S.x, S.y, base.x, base.y) + n;
		if(dir > 6)
			dir -= 6;
		int dx, dy;
		if(!shift(S.x, S.y, dir, dx, dy))
			dir = -1;
	}
	S.move_direction = (dir==-1) ? random()%6+1 : dir;
	//if((f> 3*VIEW_RANGE) && iscenter){
	    //S.move_direction = random()%6+1;
	//    }

}
}
