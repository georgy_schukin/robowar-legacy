#include "Robot.h"
#include <iostream>
#include <stdlib.h>
#include <string.h>

using namespace std;

extern int FIELD_WIDTH;
extern int FIELD_HEIGHT;
extern int SHOOT_RANGE;
extern int VIEW_RANGE;
extern int ITER;
extern int ROBOT_CONSTRUCTION_TIME;
extern int TRANSMIT_RANGE;

namespace RaS{
	int traectory(RobotState& S); //O_o something
	int run_away(RobotState& S); //look at func name 
	int attack_mes(RobotState& S); //cheak messages and attack if 0 - no attack, else 1
	int sp_dir(RobotState& S); //maximized direction from units
	int yesican(RobotState& S, int d); //correct dirction
	int get_friends(RobotState& S); //number of robots-allys
	int get_friend_bases(RobotState& S); ////allys base to memory
	int get_enemies(RobotState& S); //number of robots-enemis
	int get_enemies_bases(RobotState& S); ////number of bases-enemys
	int near_enemy_tr(RobotState& S, int& x, int& y); //coordinates of neast enemy robot to robot S
	void near_enemy_tb(RobotState& S, int bx, int by, int *x, int *y); //cordinats of neast enemy robot to base on cordinats (bx,by)
	int near_enemy_base(RobotState& S, int& x, int& y); // return coordinates of near enemy base
	int trans(RobotState& S, int typ, int x, int y, int old); //transmit coordinates of enemy
	int trans_d(RobotState& S, int typ, int d); //transmin direction
	int trans_o(RobotState& S, int x, int y, int old, int type_o, int ind); //transmit coordinates of commander
	int tocenter(RobotState& S); //deriction to center
	int way(RobotState& S); //snake-like move
	int	rushmove(RobotState& S); //run away ally robots
	int way_call(RobotState& S); //way corection
	int number_of_neighbor(RobotState& S); //look at func name
	int robot_attack(RobotState& S, int x, int y); // if robot close to point(x,y) it well attack it, else it will move to point
	void Rushpower(RobotState& S); //somthing that isn't work
	void RaS(RobotState& S); //boot it self
	/*
	Memory using:
	0: x cord of point that robot move
	1: y cord of point that robot move
	2: have robot reach that point, yes - 1, no - 2
	3: I suppose nothing
	4: flag about command, 0 - no commander, 2 - this robot is comander, 1 - this robot is sub
	5: robot indentificator

	Messages using:
	0: type of message, 1 - enemy robot cords, 2 - enemy base cords, 3 - direction, 4 - order to obey
	1: x cord of enemy / direstion / x cord of order target / dirrection 
	2: y cord of enemy / y cord of order target
	3: age of message 
	4: type of order: 1 - move, 2 - attack, 3- direction
	*/

	void RaS(RobotState& S){ //new version
		int* mem = (int*)S.memory;
		S.transmit = false;
		int en = get_enemies(S);
		int al = get_friends(S);
		int en_b = get_enemies_bases(S);
		int *msg;
		if(mem[5]==0) mem[5] = rand() + 1;
		if(al>5){
			if(mem[4]==0) 
				mem[4] = 2;
			if(!S.messages.empty()){
				bool f = false;
				for (vector<void*>::iterator it = S.messages.begin(); it != S.messages.end() &&!f ; it++){
					msg = (int*)(*it);
					if(msg[0] == 4 && msg[5] > mem[5]){
						f = true;
						mem[4] = 1;
					}
				}
				if(!f)
					mem[4] = 2;
			}
			else 
				if(mem[4]!=1) 
					mem[4] = 2;
			if(mem[4] == 2){
				int targ_x, targ_y;
				if(en>0 && en_b ==0 || en>4 && en_b!=0){

					if (en>2*al || al < 8){
						run_away(S);
						trans_o(S,S.move_direction,0,1,3,mem[5]);
					}

					else{
						near_enemy_tr(S,targ_x, targ_y);
						robot_attack(S,targ_x, targ_y);
						trans_o(S, targ_x, targ_y, 0, 2,mem[5]);
					}
				}
				else {
					if(en_b>0){
						near_enemy_base(S,targ_x,targ_y);
						robot_attack(S,targ_x, targ_y);
						trans_o(S, targ_x, targ_y,0, 2,mem[5]);
					}
					else{
						int a_m = attack_mes(S);
						if(!a_m){
							if(number_of_neighbor(S)>0){
								S.move_direction = way_call(S);
								targ_x = mem[0];
								targ_y = mem[1];
							}
							else {
								targ_x = S.x;
								targ_y = S.y;
							}
							trans_o(S, targ_x, targ_y,0, 1,mem[5]);

						}
						else{
							targ_x = S.shoot_x;
							targ_y = S.shoot_y;
							trans_o(S, targ_x, targ_y,0, 2,mem[5]);

						}
					}


				}
			}
			else
				if(mem[4] == 1){
					bool f = false;
					for (vector<void*>::iterator it = S.messages.begin(); it != S.messages.end() &&!f ; it++){
						msg = (int*)(*it);
						if(msg[0] == 4 && msg[3] && msg[5]>mem[5]){
							f = true;
							switch(msg[4]){
						case 1: {
							if(en>0){
									int nx,ny;
									int u = near_enemy_tr(S,nx,ny);
									robot_attack(S,nx,ny);
							}
							else{
								int tx, ty;
								int att = attack_mes(S);
								if(!att|| dist(S.shoot_x,S.shoot_y,S.x,S.y) > SHOOT_RANGE)
									S.move_direction = get_dir(S.x,S.y,msg[1],msg[2]);
							}
								}break;
						case 2: {
							if(dist(S.x,S.y,msg[1],msg[2]) <= SHOOT_RANGE)
								robot_attack(S,msg[1],msg[2]);
							else
							{
								if(en>0){
									int nx,ny;
									int u = near_enemy_tr(S,nx,ny);
									robot_attack(S,nx,ny);
								}
								else 
									attack_mes(S);
							}

								} break;
						case 3: {S.move_direction  = msg[1];}break;
							}
							trans_o(S, msg[1], msg[2], msg[3], msg[4], msg[5]);
						}
					}
					if((!f || !(S.shoot)) && en>0 ){
						int nx,ny;
						int u = near_enemy_tr(S,nx,ny);
						robot_attack(S,nx,ny);
						trans(S,1,nx,ny,0);
					}
				}
		}
		else{
			int nx,ny;
			int u = near_enemy_tr(S,nx,ny);
			if(en>0){
				if (en>2*al){					
					run_away(S);
					trans(S,1,nx,ny,0);
				}
				else {
					robot_attack(S,nx,ny);
					trans(S,1,nx,ny,0);
				}
			}
			else{
				if(en_b>0){
					near_enemy_base(S,nx,ny);
					robot_attack(S,nx,ny);
					trans(S,2,nx,ny,0);
				}
				else{
					int a_m = attack_mes(S);
					if(!a_m) S.move_direction = way_call(S);
				}
			}
		}
	}

	//RaS 11.07
	/*
	void RaS(RobotState& S)
	{
	int* mem = (int*)S.memory;
	S.transmit = false;
	int en = get_enemies(S);
	int al = get_friends(S);
	int nx, ny, bx, by;
	bool f = false;
	if (!en){				//if no enemy - try attack base
	for (vector<Unit>::iterator it = S.bases.begin(); it != S.bases.end() && !f; it++)
	{
	if(S.team != it->team){
	f = true;
	nx = it->x;
	ny = it->y;
	if (dist(S.x, S.y, nx, ny)< SHOOT_RANGE && S.cooldown==0){
	S.shoot = true;
	S.shoot_x = nx;
	S.shoot_y = ny;
	}
	else 
	S.move_direction = get_dir(S.x,S.y,nx,ny);
	trans(S,2,nx,ny,0);
	}
	}
	if(!f){ //if no enemy move to near message
	if(al>=1){
	if(al > 3 && al<6) {
	S.move_direction = tocenter(S);
	trans_d(S,3,S.move_direction);
	}
	else{
	if(!S.messages.empty())
	{
	bool rb[2] = {false,false};
	int min[2] = {50,50};
	int nnx[2], nny[2], j, k;
	int* msg;
	for (vector<void*>::iterator it = S.messages.begin(); it != S.messages.end() ; it++){
	msg = (int*)(*it);
	if(msg[0]<3){
	j = dist(S.x,S.y,msg[1],msg[2]);
	if(msg[0]==1) k=0; //robot
	else if (msg[0]==2) k = 1; //base
	rb[k] = true;
	if(j<min[k]){
	min[k] = j;
	nnx[k] = msg[1];
	nny[k] = msg[2];
	}
	}
	}
	if(rb[1]){
	if(min[0]-min[1] <= SHOOT_RANGE && min[0]-min[1] >= -SHOOT_RANGE){
	S.move_direction = get_dir(S.x,S.y,nnx[0],nny[0]);
	mem[0] = nnx[0];
	mem[1] = nny[0];
	trans(S,1,nnx[0],nny[0],msg[3]);						
	}
	else {
	S.move_direction = get_dir(S.x,S.y,nnx[1],nny[1]);
	mem[0] = nnx[1];
	mem[1] = nny[1];
	trans(S,2,nnx[1],nny[1],msg[3]);	
	}
	}
	else 
	if(rb[0]){
	S.move_direction = get_dir(S.x,S.y,nnx[0],nny[0]);
	trans(S,1,nnx[0],nny[0],msg[3]);						
	}
	else {
	bool flag = false;
	for (vector<void*>::iterator it = S.messages.begin(); it != S.messages.end() &&!f ; it++){
	msg = (int*)(*it);
	if(msg[0]==3){
	S.move_direction = msg[1];
	flag = true;
	}
	}
	if(!f)	S.move_direction = way_call(S);
	}
	}
	else 
	S.move_direction = way_call(S);
	}
	}
	else S.move_direction = way_call(S);
	}
	}
	else{
	for (vector<Unit>::iterator it = S.bases.begin(); it != S.bases.end() && !f; it++)
	{
	if(S.team == it->team){
	f = true;
	bx = it->x;
	by = it->y;
	}
	}
	if(f){ //defend base at all cost
	near_enemy_tb(S,bx,by,&nx,&ny);
	if (dist(S.x, S.y, nx, ny)< SHOOT_RANGE && S.cooldown==0){
	S.shoot = true;
	S.shoot_x = nx;
	S.shoot_y = ny;
	trans(S,1,nx,ny,0);
	}
	else 
	S.move_direction = get_dir(S.x,S.y,nx,ny);
	}		
	else{
	int u = near_enemy_tr(S,&nx,&ny);
	if (en>2*al){ //run away
	if(u<=SHOOT_RANGE+1){
	int m_d = get_dir(S.x,S.y,nx,ny);
	if(m_d>3) m_d -= 3;
	else m_d +=3;
	S.move_direction = m_d;
	bool shoot = false;
	trans(S,1,nx,ny,0);
	}
	}
	else{ //attack near enemy
	int m_d = get_dir(S.x,S.y,nx,ny);
	if (dist(S.x, S.y, nx, ny)< SHOOT_RANGE && !(S.cooldown))
	{
	S.shoot = true;
	S.shoot_x = nx;
	S.shoot_y = ny;
	}
	else 
	S.move_direction = get_dir(S.x,S.y,nx,ny);
	trans(S,1,nx,ny,0);
	}
	}
	}
	}

	*/
int traectory(RobotState& S){
		int* mem = (int*)S.memory;
		int *nx = new int [VIEW_RANGE+1];
		int *ny = new int [VIEW_RANGE+1];
		nx[0] = S.x; ny[0] = S.y;
		int count=0, coord[7][2];
		for( int i=1; i<=6; i++)
			shift(S.x, S.y, i, coord[i][0], coord[i][1]);
		for(vector<Unit>::iterator it = S.robots.begin(); it != S.robots.end(); it++){
			for(int i = 1; i<=6; i++)
				if(it->x == coord[i][0] && it->y == coord[i][1])
					count++;
		}

		if(count>=6) 
			shift(nx[0],ny[0],get_dir(nx[0],ny[0],mem[0],mem[1]),nx[1],ny[1]);
		for(int i = 0; i <VIEW_RANGE && count < 6; i++){
			bool f;
			int d = get_dir(nx[i],ny[i],mem[0],mem[1]);
			do{
				f = false;
				shift(nx[i],ny[i],d,nx[i+1],ny[i+1]);
				for(int j = 0; j<S.robots.size() && !f; j++){
					if(S.robots[j].x==nx[i+1] && S.robots[j].y==ny[i+1])
						f = true;
				}
				for(int j = 0; j<S.bases.size() && !f; j++){
					if(S.bases[j].x==nx[i+1] && S.bases[j].y==ny[i+1])
						f = true;
				}
				if(f)d=(++d)%7;
			}while(f);
			for( int i=1; i<=6; i++)
				shift(nx[i+1], ny[i+1], i, coord[i][0], coord[i][1]);
			for(vector<Unit>::iterator it = S.robots.begin(); it != S.robots.end(); it++){
				for(int i = 1; i<=6; i++)
					if(it->x == coord[i][0] && it->y == coord[i][1])
						count++;
			}
		}
		int d = get_dir(S.x,S.y,nx[1],ny[1]);
		delete []nx;
		delete []ny;
		return d;
	}









	int run_away(RobotState& S){
		int targ_x,targ_y;
		int u = near_enemy_tr(S,targ_x,targ_y);
		int m_d = get_dir(S.x,S.y,targ_x, targ_y);
		if(m_d>3) m_d -= 3;
		else m_d +=3;
		S.move_direction = m_d;
		if(u>SHOOT_RANGE-1)
			S.shoot = false;
		else{
			if(S.cooldown==0)
			{
				S.shoot = true;
				S.shoot_x = targ_x;
				S.shoot_y = targ_y;
			}
			else 
				S.shoot = false;
		}
		return 0;
	}


	int attack_mes(RobotState& S){
		int* mem = (int*)S.memory;
		if(!S.messages.empty())
		{
			bool rb[2] = {false,false};
			int min[2] = {50,50};
			int nnx[2], nny[2], j, k;
			int* msg;
			for (vector<void*>::iterator it = S.messages.begin(); it != S.messages.end() ; it++){
				msg = (int*)(*it);
				if(msg[0]==1 || msg[0] == 2){
					j = dist(S.x,S.y,msg[1],msg[2]);
					if(msg[0]==1) k=0; //robot
					else if (msg[0]==2) k = 1; //base
					rb[k] = true;
					if(j<min[k]){
						min[k] = j;
						nnx[k] = msg[1];
						nny[k] = msg[2];
					}
				}
			}
			if(rb[1]){
				if(min[0]-min[1] <= SHOOT_RANGE && min[0]-min[1] >= -SHOOT_RANGE){
					if(nnx[0]>100 || nny[0]>100)
						S.x = S.x;
					robot_attack(S,nnx[0],nny[0]);
					mem[0] = nnx[0];
					mem[1] = nny[0];
					trans(S,1,nnx[0],nny[0],msg[3]);
					return 1;
				}
				else {
					robot_attack(S,nnx[1],nny[1]);
					if(nnx[0]>100 || nny[0]>100)
						S.x = S.x;
					mem[0] = nnx[1];
					mem[1] = nny[1];
					trans(S,2,nnx[1],nny[1],msg[3]);
					return 1;
				}
			}
			else 
				if(rb[0]){
					robot_attack(S,nnx[0],nny[0]);
					if(nnx[0]>100 || nny[0]>100)
						S.x = S.x;
					trans(S,1,nnx[0],nny[0],msg[3]);
					return 1;
				}
				else return 0;
		}
		else return 0;
	}


	int get_friends(RobotState& S)
	{
		int res=0;
		for(vector<Unit>::iterator it = S.robots.begin(); it != S.robots.end(); it++)
			if(S.team == it->team && dist(S.x,S.y,it->x,it->y) <= VIEW_RANGE)
				res++;
		return res;
	}

	int get_friend_bases(RobotState& S)
	{
		int* mem = (int*)S.memory;
		for(vector<Unit>::iterator it = S.bases.begin();
			it != S.bases.end(); it++)
			if(S.team == it->team){
				mem[0] = it->x;
				mem[1] = it->y;
			}
			return 0;
	}

	int get_enemies(RobotState& S)
	{
		int res=0;
		for(vector<Unit>::iterator it = S.robots.begin();it != S.robots.end(); it++)
			if(S.team != it->team)
				res++;
		return res;
	}

	int get_enemies_bases(RobotState& S)
	{
		int* mem = (int*)S.memory;
		int num = 0;
		for(vector<Unit>::iterator it = S.bases.begin(); it != S.bases.end(); it++)
			if(S.team != it->team)
				num++;
		return num;
	}

	int near_enemy_tr(RobotState& S, int& x, int& y){
		int min = VIEW_RANGE + 1;
		int j;
		for(vector<Unit>::iterator it = S.robots.begin(); it != S.robots.end(); it++){
			if(S.team != it->team){
				j = dist(S.x,S.y,it->x,it->y);
				if(j<min){
					min = j;
					x = it->x;
					y = it->y;
				}
			}
		}
		return min;
	}

	void near_enemy_tb(RobotState& S, int bx, int by, int *x, int *y){
		int min = VIEW_RANGE;
		int j;
		for(vector<Unit>::iterator it = S.robots.begin(); it != S.robots.end(); it++){
			if(S.team != it->team){
				j = dist(bx,by,it->x,it->y);
				if(j<min){
					min = j;
					*x = it->x;
					*y = it->y;
				}
			}
		}
	}

	int near_enemy_base(RobotState& S, int& x, int& y){
		int min = VIEW_RANGE + 1;
		int j;
		for(vector<Unit>::iterator it = S.bases.begin(); it != S.bases.end(); it++){
			if(S.team != it->team){
				j = dist(S.x,S.y,it->x,it->y);
				if(j<min){
					min = j;
					x = it->x;
					y = it->y;
				}
			}
		}
		return 0;
	}


	int trans(RobotState& S, int typ, int x, int y, int old){
		int max;

		(FIELD_HEIGHT > FIELD_WIDTH)? max = FIELD_HEIGHT : max = FIELD_WIDTH;
		max /= TRANSMIT_RANGE;
		if(old < 2*max){
			S.transmit = true;
			int* mes = (int*)S.message;
			mes[0] = typ;
			mes[1] = x;
			mes[2] = y;
			mes[3] = old + 1;
		}
		return 0;
	}

	int trans_d(RobotState& S, int typ, int d){
		S.transmit = true;
		int* mes = (int*)S.message;
		mes[0] = typ;
		mes[1] = d;
		return 0;
	}

	int trans_o(RobotState& S, int x, int y, int old, int type_o, int ind){
		int max;
		(FIELD_HEIGHT > FIELD_WIDTH)? max = FIELD_HEIGHT : max = FIELD_WIDTH;
		max /= TRANSMIT_RANGE;
		if(old < max){
			S.transmit = true;
			int* mes = (int*)S.message;
			if(type_o == 3){
				mes[0] = 4;
				mes[1] = x;
				mes[3] = old + 1;
				mes[4] = type_o;
				mes[5] = ind;
			}
			else{
				mes[0] = 4;
				mes[1] = x;
				mes[2] = y;
				mes[3] = old + 1;
				mes[4] = type_o;
				mes[5] = ind;
			}
		}
		else S.transmit = false;
		return 0;
	}

	int number_of_neighbor(RobotState& S){
		int count=0, coord[7][2];
		for( int i=1; i<=6; i++)
			shift(S.x, S.y, i, coord[i][0], coord[i][1]);
		for(vector<Unit>::iterator it = S.robots.begin(); it != S.robots.end(); it++){
			for(int i = 1; i<=6; i++)
				if(it->x == coord[i][0] && it->y == coord[i][1])
					count++;
		}
		return count;
	}


	int tocenter(RobotState& S){ 
		if(S.x <= FIELD_WIDTH/3){
			if(S.y <= FIELD_HEIGHT/3) return 2;
			if(S.y > FIELD_HEIGHT/3 && S.y < 2*FIELD_HEIGHT/3) return 1;
			if(S.y >= 2*FIELD_HEIGHT/3) return 6;
		}
		if(S.x > FIELD_WIDTH/3 && S.x < 2*FIELD_WIDTH/3){
			if(S.y <= FIELD_HEIGHT/3) { if(S.x <= FIELD_WIDTH/2) return 2; else return 3;}
			if(S.y > FIELD_HEIGHT/3 && S.y < 2*FIELD_HEIGHT/3) {
				if(S.x <= FIELD_WIDTH/2) {
					if(S.y <= FIELD_HEIGHT/2) return 6;
					else return 5;
				}
				else{
					if(S.y <= FIELD_HEIGHT/2) return 2;
					else return 3;
				}
			}
			if(S.y >= 2*FIELD_HEIGHT/3) { if(S.x <= FIELD_WIDTH/2) return 6; else return 5;}
		}
		if(S.x >= 2*FIELD_WIDTH/3){
			if(S.y <= FIELD_HEIGHT/3) return 3;
			if(S.y > FIELD_HEIGHT/3 && S.y < 2*FIELD_HEIGHT/3) return 4;
			if(S.y >= 2*FIELD_HEIGHT/3) return 5;
		}
		return 0;
	}

	int way(RobotState& S){
		int al = get_friends(S);
		int* mem = (int*)S.memory;
		if(mem[3]==0) mem[3] = 1;
		if(al<=3){
			if(S.x >= FIELD_WIDTH - VIEW_RANGE - 2) 
				mem[0] = VIEW_RANGE;
			else 
				if(S.x <=VIEW_RANGE+2) mem[0]= FIELD_WIDTH - VIEW_RANGE;
				else {
					if(rand()%2)mem[0]= FIELD_WIDTH - VIEW_RANGE;
					else mem[0] = VIEW_RANGE;
				}
				if(S.y >= FIELD_HEIGHT-VIEW_RANGE-2 || S.y <= VIEW_RANGE+2)
					mem[3]*=(-1);
				if(S.x >= FIELD_WIDTH - VIEW_RANGE -2 || S.x <= VIEW_RANGE +2) 
					mem[1]=S.y + mem[3]*VIEW_RANGE;
		}
		else{
			int d = sp_dir(S);
			shift(S.x,S.y,d,mem[0],mem[1]);
			for(int i = 0; i < VIEW_RANGE - 1 && mem[0] > 0 && mem[0] < FIELD_WIDTH -1 && mem[1]>0 && mem[1] < FIELD_HEIGHT -1; i++)
				shift(mem[0],mem[1],d,mem[0],mem[1]);
		}	
		return 0;
	}



	int yesican(RobotState& S, int d){
		int nx, ny;
		shift(S.x,S.y,d,nx,ny);
		if(nx > FIELD_WIDTH - 1) return 4;
		if(ny > FIELD_HEIGHT - 1) return 5;
		if(nx < 0) return 1;
		if(ny < 0) return 2;
		for(vector<Unit>::iterator it = S.robots.begin(); it != S.robots.end(); it++){
			if(it->x == nx && it->y == ny) {
				if(d>3) d -=3;
				else d+=3;
				return d;
			}
		}
		int bd,coord[7][2];
		for( int i=1; i<=6; i++)
			shift(S.x, S.y, i, coord[i][0], coord[i][1]);
		bool f = false;
		for(vector<Unit>::iterator it = S.bases.begin(); it != S.bases.end()&& !f; it++){
			if(S.team == it->team){
				for (int i=1; i<=6 &&!f; i++)
					if(it->x == coord[i][0] && it->y == coord[i][1]) {
						f = true;
						bd = --i;
					}
			}
			if(f){
				if(bd>3) bd -=3;
				else bd+=3;
				return bd;
			}
		}
		return d;
	}

	int sp_dir(RobotState& S){
		int nx,ny;
		int distans[7];
		for (int i = 1 ; i<= 6; i++){
			shift(S.x,S.y,i,nx,ny);
			if(yesican(S,i)){
				distans[i] = 0;
				for(int j = 0; j< S.robots.size(); j++)
					if(S.team == S.robots[j].team)
						distans[i]+=dist(nx,ny,S.robots[j].x,S.robots[j].y);
				for(int j = 0; j< S.bases.size(); j++)
					if(S.team == S.bases[j].team)
						distans[i]+=dist(nx,ny,S.bases[j].x,S.bases[j].y);
			}
			else
				distans[i] = -1;
		}
		int max = 1;
		for(int i = 2; i <= 6; i++)
			if(distans[i]>distans[max]) max = i;
		if(distans[max]!=-1) return yesican(S,max);
		else return yesican(S,tocenter(S));
	}

	int	rushmove(RobotState& S){
		int count=0, coord[7][2];
		for( int i=1; i<=6; i++)
			shift(S.x, S.y, i, coord[i][0], coord[i][1]);
		for(vector<Unit>::iterator it = S.robots.begin(); it != S.robots.end(); it++){
			for (int i=1; i<=6; i++)
				if(it->x == coord[i][0] && it->y == coord[i][1]) count++;
		}
		for(vector<Unit>::iterator it = S.bases.begin(); it != S.bases.end(); it++){
			if(S.team == it->team){
				for (int i=1; i<=6; i++)
					if(it->x == coord[i][0] && it->y == coord[i][1]) count++;
			}
		}
		if(count>1) 
			return sp_dir(S);
		else return 0;
	}

	int way_call(RobotState& S){
		int* mem = (int*)S.memory;
		int al = get_friends(S);
		if(mem[2]!=1 && mem[2]!=2) {
			mem[2]=2;
			int d = sp_dir(S);
			shift(S.x,S.y,d,mem[0],mem[1]);
			return d;
		}
		if(mem[2]==1 && dist(S.x, S.y, mem[0], mem[1]) <= VIEW_RANGE - 2 || al>7){ 
			mem[2]=2;
			/*if(mem[0]<mem[1]){
			if(mem[0]<field_width-3*view_range) mem[0]+=2*view_range-2;
			else mem[0]-=2*view_range-2;
			}
			else{
			if(mem[1]<field_height-3*view_range) mem[1]+=2*view_range-2;
			else mem[1]-=2*view_range-2;
			}*/
		}
		if (mem[2]==2)
		{
			if(number_of_neighbor(S)>3) S.move_direction = sp_dir(S);
			else {
				way(S); 
				mem[2]=1;
			}
		}
		return traectory(S);
	}


	void Rushpower(RobotState& S){
		S.transmit = false;
		bool m = false;
		for (vector<Unit>::iterator it = S.bases.begin(); it != S.bases.end() && !m; it++){
			if(S.team !=it->team){
				m = true;
				S.shoot = true;
				S.shoot_x = it->x;
				S.shoot_y = it->y;
				trans(S,2,it->x,it->y,0);
			}
		}
		if(!m){
			bool f = false;
			for (vector<Unit>::iterator it = S.robots.begin(); it != S.robots.end() && !f; it++){
				if(S.team !=it->team){
					f = true;
					S.shoot = true;
					S.shoot_x = it->x;
					S.shoot_y = it->y;
					trans(S,1,it->x,it->y, 0);
				}
			}
			if (!f)S.move_direction = rushmove(S);
		}
	}
	int robot_attack(RobotState& S, int x, int y){
		int* mem = (int*)S.memory;
		int m_d = get_dir(S.x,S.y,x,y);
		mem[0] = x;
		mem[1] = y;
		mem[2] = 2;
		bool f = false;
		for(int j = 0; j<S.robots.size() && !f; j++){
			if(S.robots[j].x==x && S.robots[j].y==y && S.robots[j].team!=S.team)
				f = true;
		}
		for(int j = 0; j<S.bases.size() && !f; j++){
			if(S.bases[j].x==x && S.bases[j].y==y && S.bases[j].team!=S.team)
				f = true;
		}
		if (dist(S.x, S.y, x, y)< SHOOT_RANGE-1 && !(S.cooldown) && f)
		{
			S.shoot = true;
			S.shoot_x = x;
			S.shoot_y = y;
		}
		else 
			S.move_direction = get_dir(S.x, S.y, x, y);
		return 0;
	}


}