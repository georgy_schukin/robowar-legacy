#include "Robot.h"
#include <iostream>
#include <stdlib.h>
#include <string.h>

using namespace std;
namespace DMBot
{
int get_friends(RobotState& S)
{
	int res=0;
	for(vector<Unit>::iterator it = S.robots.begin();
			it != S.robots.end(); it++)
		if(S.team == it->team)
			res++;
	return res;
}

int get_enemies(RobotState& S)
{
	int res=0;
	for(vector<Unit>::iterator it = S.robots.begin();
			it != S.robots.end(); it++)
		if(S.team != it->team)
			res++;
	return res;
}

int get_sect(int x,int y)
{
    int sect_width=FIELD_WIDTH/2,
        sect_height=FIELD_HEIGHT/2;
    if (x<=sect_width)
	if (y<=sect_height)
	    return 0;
	else
	    return 3;
    else
	if (y<=sect_height)
	    return 1;
	else
	    return 2;

    return 4;//no way

}

void DMBot(RobotState& S)
{

	S.transmit = false; int f = get_friends(S); int e =
	get_enemies(S); int key=1; Unit b,c; int* mem = (int*)S.message;
	int sect=get_sect(S.x,S.y);
	if (mem[4]==0)mem[4]=6;
	int sect1;
	if(ITER<(FIELD_WIDTH+FIELD_HEIGHT))
	{
	    switch(sect)
	    {
		case 0: 
		    mem[3]=random()%3;
		    break;
		    mem[4]=mem[3];
		case 1: 
		    mem[3]=random()%3+2;
		    mem[4]=mem[3];
		    break;
		case 2:
		    mem[3]=random()%3+4;
		    mem[4]=mem[3];
		    break;
		case 3:
		{
		    int temp=random()%2;
		    if (temp==0)
			mem[3]=random()%2+5;
		    else mem[3]=1;
		    mem[4]=mem[3];
		    break;
		}
		default: S.move_direction=random()%7;break;
	    }
	/*	if (ITER<(FIELD_WIDTH+FIELD_HEIGHT)/3)
	    {
		switch(sect)
		{
		case 0:
		    mem[3]=1;
		    break;
		case 2:
		    mem[3]=4;
		    break;
		default: break;
		}
	    }*/
	    
	}
	else mem[3]=random()%7;
	S.move_direction=mem[3];
	if (abs(sect1=get_sect(S.x,S.y)-sect)!=0)S.move_direction=mem[4];
	
	if(e > f)
	{ // runaway
		for(vector<Unit>::iterator it = S.robots.begin(); it != S.robots.end() && key==1; it++)
		{
		    b=*it;
		    if (S.team!=b.team )
			{
			    S.move_direction = (get_dir(S.x, S.y, (*it).x, (*it).y) + 2)%6;
			    key=0;
			}
		}
	}
	else if(e==0)
	{
	    int dx, dy;
	    
	    if(shift(S.x, S.y, S.move_direction, dx, dy)==false)
	        S.move_direction=(S.move_direction+2)%6;
			
				
	}//f>e
	    else{
		    for(vector<Unit>::iterator it = S.robots.begin(); it != S.robots.end() && key==1; it++)
		    {
		        b=*it;
		        if (S.team!=b.team )
				{
				    S.move_direction = get_dir(S.x, S.y, (*it).x, (*it).y);
				    key=0;
				}
		    }
		}
	if(S.robots.empty()==false)
	{
		for(vector<Unit>::iterator it = S.robots.begin(); it != S.robots.end() && key==0; it++)
		{
			b = *it;
			if(b.team != S.team)
			{
				if (dist(S.x,S.y,b.x,b.y)<=SHOOT_RANGE)
					S.move_direction=0;
				
				key=1;
				S.shoot = true;
				S.shoot_x = b.x;
				S.shoot_y = b.y;
				if(e>f) {//defense
					    if (S.bases.empty()==false)
					    for (vector<Unit>::iterator it = S.bases.begin(); it != S.bases.end()&& key==1 ;it++)
				    	    {
					        b = *it;
					        if (b.team==S.team)
					        {
						    S.move_direction=0;
						    key=0;
					        }
					    }
					//    else
					//    S.move_direction = get_dir(b.x, b.y, S.x, S.y);
					}
				S.transmit = true;
				int* mem = (int*)S.message;
				mem[0] = b.x;
				mem[1] = b.y;
				mem[2] = ITER;
				mem[3] = get_dir(S.x,S.y,b.x,b.y);
			}
		}
	}		
	if(S.bases.empty()==false)
	{
		for(vector<Unit>::iterator it = S.bases.begin(); it != S.bases.end(); it++)
		{
			b = *it;
			if(b.team != S.team)
			{
				S.shoot = true;
				S.shoot_x = b.x;
				S.shoot_y = b.y;
				if(get_friends(S)>1)
					S.move_direction = get_dir(S.x, S.y, b.x, b.y);
				S.transmit = true;
				int* mem = (int*)S.message;
				mem[0] = b.x;
				mem[1] = b.y;
				mem[2] = ITER;
				mem[3] = get_dir(S.x,S.y,b.x,b.y);
			}
		}
	}

	if(S.shoot==false && S.messages.empty()==false)
	{
		int* msg = (int*)S.messages[0];
		int tx = msg[0];
		int ty = msg[1];
		int tm = msg[3];
		S.move_direction=get_dir(S.x, S.y, tx, ty);
		if(!S.transmit && ITER-msg[2]<15)
		{
			memcpy(S.message, S.messages[0], MSG_SIZE);
			S.transmit = true;
		}
		    
	}
	
	
}}

using namespace DMBot;



extern int ITER;


