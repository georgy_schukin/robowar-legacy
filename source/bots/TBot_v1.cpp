#include "Robot.h"
#include <list>
#include <stdlib.h>
#include <string.h>

#include <stdio.h>

using namespace std;

extern int iter;

namespace TBot_v1
{

void ShootIt(RobotState& S,Unit &u)
{ S.shoot = true;
  S.shoot_x = u.x;
  S.shoot_y = u.y;
}

int Distance(int x1,int y1,int x2,int y2)
{ int d,dx,dy;
  //if(x1<0 || x2<0 || y1<0 || y2<0 || x1>=FIELD_WIDTH || x2>=FIELD_WIDTH || y1>=FIELD_HEIGHT || y2>=FIELD_HEIGHT) return -1;
  dx=abs(x2-x1);
  dy=abs(y2-y1);
  if      (x1<=x2 && dy%2==0) d = dy/2+1;
  else if (x1>=x2 && dy%2==1) d = dy/2+1;
  else                        d = dy/2;
  if (d>dx) d = dx;
  if (d>dy) d = dy;
  return dx+dy-d;
}


enum MessageType {mt_none=0,mt_born,mt_born_answer,mt_shoot,mt_move,mt_enemy};
enum StatusType {st_none=0,st_born,st_searcher,st_defender,st_trooper};

struct MessageInfo
{ int id;          // id
  int x,y;         // position
  MessageType act; // action
  int tx,ty;       // target position
  int d1,d2;       // some other data
};

void MessageInit(void *msg,int id,int x,int y)
{
  int *m=(int*)msg;
  m[0]=id; // robot id
  m[1]=x;  // robot position: x
  m[2]=y;  // robot position: y
  m[3]=0;  // end of submessage list
}

void MessageAppend(void *msg,MessageInfo &mi)
{ const int max_msg_size = 6;
  int total=4;
  int *m=(int*)msg+3;
  int size=m[0];
  while (size!=0) { m+=size; total+=size; size=m[0]; }
  if (total+max_msg_size > MSG_SIZE/sizeof(int)) return;
  m[1]=mi.act;
  switch(mi.act)
  { case mt_born: size=3; m[2]=mi.d1; break;
    case mt_born_answer: size=3; m[2]=mi.d1; break;
    case mt_shoot: size=5; m[2]=mi.tx; m[3]=mi.ty; m[4]=mi.d1; break;
    case mt_move: size=4; m[2]=mi.tx; m[3]=mi.ty; break;
    case mt_enemy: size=6; m[2]=mi.tx; m[3]=mi.ty; m[4]=mi.d1; m[5]=mi.d2; break;
  }
  m[0]=size;
  m[size]=0;
}

void MessageRead(void *msg,vector<MessageInfo> &minfo)
{ MessageInfo mi;
  int *m=(int *)msg;
  mi.id = m[0];
  mi.x  = m[1];
  mi.y  = m[2];
  m += 3;
  int size = m[0];
  while (size!=0)
  { mi.act = (MessageType)m[1];
    switch (mi.act)
    { case mt_born: 
        mi.d1 = m[2];
        break;
      case mt_born_answer:
        mi.d1 = m[2];
        break;
      case mt_shoot:
        mi.tx = m[2];
        mi.ty = m[3];
        mi.d1 = m[4];
        break;
      case mt_move:
        mi.tx = m[2];
        mi.ty = m[3];
        break;
      case mt_enemy:
        mi.tx = m[2];
        mi.ty = m[3];
        mi.d1 = m[4];
        mi.d2 = m[5];
        break;
    }
    minfo.push_back(mi);
    m += size;
    size = m[0];
  }
}

struct EnemyInfo
{ int x,y;    // position
  int dist;   // distance
  int attack; // attacked
  int base;   // is base
  int re;     // retranslation count
};

void PositionMove(int &x,int &y,int direction)
{ switch(direction)
  { case 1: x++; break;
    case 2: y++; if (y%2==0) x++; break;
    case 3: y++; if (y%2!=0) x--; break;
    case 4: x--; break;
    case 5: y--; if (y%2!=0) x--; break;
    case 6: y--; if (y%2==0) x++; break;
  }
}

int DirectionRandom() { return 1+rand()%6; }

int DirectionRandomRadius(RobotState &S,int cx,int cy,int dist) 
{ int tx,ty,dir=1+rand()%6;
  for (int i=0;i<100;i++)
  { tx=S.x; ty=S.y;
    PositionMove(tx,ty,dir);
    if (Distance(tx,ty,cx,cy)<=dist) return dir;
    dir=1+rand()%6;
  }
  return 0;
}

int DirectionTo(int x1,int y1,int x2,int y2)
{ if (y2>y1)      // going down
  { if (y1%2==0) if (x2>x1) return 2;
                 else       return 3;
    else         if (x2<x1) return 3;
                 else       return 2;
  }
  else if (y2<y1) // going up
  { if (y1%2==0) if (x2>x1) return 6;
                 else       return 5;
    else         if (x2<x1) return 5;
                 else       return 6;
  }
  else            // same y
  { if (x2>x1)      return 1;
    else if (x2<x1) return 4;
  }
  return 0;
}

int DirectionFrom(int x1,int y1,int x2,int y2)
{ if (y2>y1)      // going up
  { if (y1%2==0) if (x2>x1) return 5;
                 else       return 6;
    else         if (x2<x1) return 6;
                 else       return 5;
  }
  else if (y2<y1) // going down
  { if (y1%2==0) if (x2>x1) return 3;
                 else       return 2;
    else         if (x2<x1) return 2;
                 else       return 3;
  }
  else            // same y
  { if (x2>x1)      return 4;
    else if (x2<x1) return 1;
  }
  return 0;
}


void ShootDecision(RobotState &S,list<EnemyInfo> &enea)
{ if (enea.empty()) { S.shoot=false; return; }
  S.shoot=true;
  EnemyInfo ei; ei.dist=SHOOT_RANGE+1;
  for (list<EnemyInfo>::iterator eit=enea.begin();eit!=enea.end();eit++)
  { if (!(*eit).base && ei.base) ei=*eit;
    else if ((*eit).dist<ei.dist) ei=*eit;
    else if ((*eit).dist==ei.dist && (*eit).attack>ei.attack) ei=*eit;
  }
  S.shoot_x=ei.x;
  S.shoot_y=ei.y;
}

void MoveDecision(RobotState &S,list<EnemyInfo> &enea,list<EnemyInfo> &efar)
{ EnemyInfo ei;
  int good_range = SHOOT_RANGE;
  if (enea.empty())
  { if (efar.empty()) { S.move_direction=DirectionRandom(); return; } // General case of movement ------------------------------------------
    ei.dist=FIELD_HEIGHT+FIELD_WIDTH;
    for (list<EnemyInfo>::iterator eit=efar.begin();eit!=efar.end();eit++) if ((*eit).dist<ei.dist) ei=*eit; // get the nearest enemy
    S.move_direction = DirectionTo(S.x,S.y,ei.x,ei.y);                                                       // go to it!
  }
  else
  { ei.dist=SHOOT_RANGE+1;
    for (list<EnemyInfo>::iterator eit=enea.begin();eit!=enea.end();eit++) if ((*eit).dist<ei.dist) ei=*eit; // get the nearest enemy
    if (ei.dist<good_range) S.move_direction = DirectionFrom(S.x,S.y,ei.x,ei.y);                             // reach the good_range
    else if (ei.dist>good_range) S.move_direction = DirectionTo(S.x,S.y,ei.x,ei.y);
    else S.move_direction = 0;
  }
}

void TBot_v1(RobotState& S)
{
  const int re_max = TRANSMIT_RANGE;
  
  // self-identification:
  int id = ((int*)S.memory)[0]; while (id==0) id = rand();
  int num = ((int*)S.memory)[1];
  StatusType status = (StatusType)((int*)S.memory)[2];
  int *Data = (int*)S.memory + 4;
  vector<MessageInfo> minfo;
  MessageInit(S.message,id,S.x,S.y);
  S.transmit = true;
  MessageInfo mi;
  list<EnemyInfo> enea; // enemies, dist <= SHOOT_RANGE
  list<EnemyInfo> efar; // enemies, dist > SHOOT_RANGE
  EnemyInfo ei;

  // radio info:
  for (vector<void*>::iterator it=S.messages.begin();it!=S.messages.end();it++) MessageRead(*it,minfo);
  for (vector<MessageInfo>::iterator it=minfo.begin();it!=minfo.end();it++)
    if ((*it).act==mt_shoot || (*it).act==mt_enemy)
    { int was=0;
      int shoot = (*it).act==mt_shoot;
      int dis = Distance(S.x,S.y,(*it).tx,(*it).ty);
      if (dis<=SHOOT_RANGE)
      { for (list<EnemyInfo>::iterator eit=enea.begin();eit!=enea.end();eit++)
          if ((*eit).x==(*it).tx && (*eit).y==(*it).ty) { was=1; (*eit).attack+=shoot; break; }
        if (!was) { ei.x=(*it).tx; ei.y=(*it).ty; ei.dist=dis; ei.attack=shoot; ei.base=0; ei.re=(*it).d2; enea.push_back(ei); }
      }
      else
      {
        for (list<EnemyInfo>::iterator eit=efar.begin();eit!=efar.end();eit++)
          if ((*eit).x==(*it).tx && (*eit).y==(*it).ty) { was=1; (*eit).attack+=shoot; break; }
        if (!was) { ei.x=(*it).tx; ei.y=(*it).ty; ei.dist=dis; ei.attack=shoot; ei.base=0; ei.re=(*it).d2; efar.push_back(ei); }
      }
    }

  // view info:
  for (list<EnemyInfo>::iterator eit=enea.begin();eit!=enea.end();)
    if ((*eit).dist<=VIEW_RANGE)
    { int yes=0;
      for (vector<Unit>::iterator it=S.robots.begin();it!=S.robots.end();it++)
      { Unit &u=*it;
        int dis = Distance(S.x,S.y,u.x,u.y);
        if (dis<=SHOOT_RANGE && (*eit).x==u.x && (*eit).y==u.y) { yes=1; break; }
      }
      if (!yes) eit=enea.erase(eit); else eit++;
    }
  if (SHOOT_RANGE < VIEW_RANGE)
    for (list<EnemyInfo>::iterator eit=efar.begin();eit!=efar.end();)
      if ((*eit).dist<=VIEW_RANGE)
      { int yes=0;
        for (vector<Unit>::iterator it=S.robots.begin();it!=S.robots.end();it++)
        { Unit &u=*it;
          int dis = Distance(S.x,S.y,u.x,u.y);
          if (dis<=SHOOT_RANGE && (*eit).x==u.x && (*eit).y==u.y) { yes=1; break; }
        }
        if (!yes) eit=enea.erase(eit); else eit++;
      }

  int e_num = 0, e_dis = 0;
  int f_num = 0, f_dis = 0;
  for (vector<Unit>::iterator it=S.robots.begin();it!=S.robots.end();it++)
  { Unit &u = *it;
    int dis = Distance(S.x,S.y,u.x,u.y);
    if (u.team==S.team) { f_num++; if (f_dis>dis) f_dis=dis; }
    else 
    { e_num++; if (e_dis>dis) e_dis=dis;
      int was=0;
      if (dis<=SHOOT_RANGE)
      { for (list<EnemyInfo>::iterator eit=enea.begin();eit!=enea.end();eit++)
          if ((*eit).x==(*it).x && (*eit).y==(*it).y) { was=1; (*eit).re=0; break; }
        if (!was) { ei.x=(*it).x; ei.y=(*it).y; ei.dist=dis; ei.attack=0; ei.base=0; ei.re=0; enea.push_back(ei); }
      }
      else
      { for (list<EnemyInfo>::iterator eit=efar.begin();eit!=efar.end();eit++)
          if ((*eit).x==(*it).x && (*eit).y==(*it).y) { was=1; (*eit).re=0; break; }
        if (!was) { ei.x=(*it).x; ei.y=(*it).y; ei.dist=dis; ei.attack=0; ei.base=0; ei.re=0; efar.push_back(ei); }
      }
    }
  }

  for (vector<Unit>::iterator it=S.bases.begin();it!=S.bases.end();it++)
  { Unit &u = *it;
    int dis = Distance(S.x,S.y,u.x,u.y);
    if (u.team!=S.team) 
    { int was=0;
      if (dis<=SHOOT_RANGE)
      { for (list<EnemyInfo>::iterator eit=enea.begin();eit!=enea.end();eit++)
          if ((*eit).x==(*it).x && (*eit).y==(*it).y) { (*eit).base=1; (*eit).re=0; was=1; break; }
        if (!was) { ei.x=(*it).x; ei.y=(*it).y; ei.dist=dis; ei.attack=0; ei.base=1; ei.re=0; enea.push_back(ei); }
      }
      else
      { for (list<EnemyInfo>::iterator eit=efar.begin();eit!=efar.end();eit++)
          if ((*eit).x==(*it).x && (*eit).y==(*it).y) { (*eit).base=1; (*eit).re=0; was=1; break; }
        if (!was) { ei.x=(*it).x; ei.y=(*it).y; ei.dist=dis; ei.attack=0; ei.base=1; ei.re=0; efar.push_back(ei); }
      }
    }
  }

  // make decision
  switch (status)
  { case st_none: // just born
    { int was_message=0;
      int bx=S.x,by=S.y;
      for (vector<Unit>::iterator it=S.bases.begin();it!=S.bases.end();it++)
      { Unit &u=*it;
        if (Distance(u.x,u.y,bx,by)==1) { bx=u.x; by=u.y; break; }
      }
      Data[0]=bx;
      Data[1]=by;
      status=st_born;
      for (vector<MessageInfo>::iterator it=minfo.begin();it!=minfo.end();it++)
        if ((*it).act==mt_born)
        { num=(*it).d1+1;
          status=st_born;
          mi.act=mt_born_answer;
          mi.d1=(*it).id;
          MessageAppend(S.message,mi);
          was_message=1;
          break;
        }
      if (!was_message) // I am the first in the world! Adam?
      {
        num=1;
      }
      S.move_direction = DirectionRandomRadius(S,bx,by,TRANSMIT_RANGE-1);
      ShootDecision(S,enea);
      break;
    }
    case st_born: // waiting for the next born
    { int was_message=0;
      int bx=Data[0];
      int by=Data[1];
      for (vector<MessageInfo>::iterator it=minfo.begin();it!=minfo.end();it++)
        if ((*it).act==mt_born_answer && (*it).d1==id) { was_message=1; break; }
      if (was_message)
      {
        status = st_trooper;
        ShootDecision(S,enea);
        MoveDecision(S,enea,efar);
      }
      else
      {
        mi.act=mt_born;
        mi.d1=num;
        MessageAppend(S.message,mi);
        S.move_direction=DirectionRandomRadius(S,bx,by,TRANSMIT_RANGE);
        ShootDecision(S,enea);
      }
      break;
    }
    case st_trooper:
    default:
    {
      ShootDecision(S,enea);
      MoveDecision(S,enea,efar);
    }
  }

  if (S.shoot) // transmit my shoot
  { mi.act = mt_shoot;
    mi.tx = S.shoot_x;
    mi.ty = S.shoot_y;
    MessageAppend(S.message,mi);
  }
  for (list<EnemyInfo>::iterator eit=enea.begin();eit!=enea.end();eit++) // transmit nearby enemies
    if ((*eit).re < re_max)
    { mi.act = mt_enemy;
      mi.tx = (*eit).x;
      mi.ty = (*eit).y;
      mi.d1 = (*eit).base;
      mi.d2 = (*eit).re+1;
      MessageAppend(S.message,mi);
    }
  for (list<EnemyInfo>::iterator eit=efar.begin();eit!=efar.end();eit++) // transmit far enemies
    if ((*eit).re < re_max)
    { mi.act = mt_enemy;
      mi.tx = (*eit).x;
      mi.ty = (*eit).y;
      mi.d1 = (*eit).base;
      mi.d2 = (*eit).re+1;
      MessageAppend(S.message,mi);
    }

  // self-memorizing:
  ((int*)S.memory)[0] = id;
  ((int*)S.memory)[1] = num;
  ((int*)S.memory)[2] = status;

//  printf("%10d, %2d: ",id,num); for (int i=0;i<10;i++) printf("%10d ",((int*)S.message)[i]); printf("\n");
//  printf("%3d: %10d, %2d, %2d:",iter,id,num,status); for (vector<MessageInfo>::iterator it=minfo.begin();it!=minfo.end();it++) printf(" %d %d %d %d %d |",(*it).id,(*it).x,(*it).y,(*it).act,(*it).d1); printf("\n");

}

}