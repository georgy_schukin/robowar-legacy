#include <math.h>
#include <vector>
#include "MyMath.h"

using namespace std;

namespace mymath
{

vector<float> sin_table; // table for sin values
vector<float> cos_table; // table for cos values

float sin_table_coeff, cos_table_coeff;
int sin_table_sz, cos_table_sz;

void Init(unsigned int table_sz)
{
	sin_table_coeff = cos_table_coeff = 2*PI/float(table_sz);
	sin_table_sz = cos_table_sz = table_sz;

	sin_table.resize(table_sz);
	cos_table.resize(table_sz);

	for(unsigned int i = 0;i < table_sz;i++)
	{
		sin_table[i] = sin(i*sin_table_coeff);
		cos_table[i] = cos(i*cos_table_coeff);
	}

	sin_table_coeff = 1.0f/sin_table_coeff;
	cos_table_coeff = 1.0f/cos_table_coeff;
}

float Sin(float rad)
{
	return sin_table[int(rad*sin_table_coeff) % (sin_table_sz - 1)];
}

float Cos(float rad)
{
	return cos_table[int(rad*cos_table_coeff) % (cos_table_sz - 1)];
}

int Sign(float val)
{
	return (val >= 0.0f) ? 1 : -1;
}

}
