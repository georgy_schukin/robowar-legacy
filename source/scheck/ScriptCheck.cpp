#include "ScriptEngine.h"
#include <iostream>

using namespace std;

int main(int argc, char** argv)
{
	ScriptEngine s_engine;

	if(argc > 1)
	{
		s_engine.Init();

		for(int i = 1;i < argc;i++)
		{
			string script = string(argv[i]);

			if(s_engine.LoadScript(script, "Scripts/") != -1)
			{
				if(s_engine.CompileScript(script) != -1)
				{
					cout << "Script " << script << " is OK" << endl;
				}
				else
				{
					cout << "Errors in script " << script << endl;
				}
			}
			else
			{
				cout << "Failed to find script " << script;
			}
		}

		s_engine.Free();
	}
	else
	{
		cout << "Usage: scheck.exe <script1> [<script2> ... <scriptn>]" << endl;
	}

return 0;
}