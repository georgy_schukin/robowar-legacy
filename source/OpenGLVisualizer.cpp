//#include "OpenGL1.h"
#include "OpenGLVisualizer.h"
//#include <assert.h>

namespace robo_vis
{
	using namespace std;

	VisualizerOpenGLController::VisualizerOpenGLController(int mapWidth, int mapHeight, int teamAmount) : 
		VisualizerController(mapWidth, mapHeight, teamAmount), iteration(0)
	{
		drawerController = new VisualizerOpenGLDrawerController(mapWidth, mapHeight);
		localData = MinimapData(mapWidth, mapHeight, mapWidth, mapHeight, /*MINIMAP_WIDTH, MINIMAP_HEIGHT,*/ teamAmount);
	}
	
	void VisualizerOpenGLController::visualize(const vector<RobotData> &robots, const vector<BaseData> &bases)
	{
		//assert(drawerController != NULL);
		//localData = MinimapData(getMapWidth(), getMapHeight(), MINIMAP_WIDTH, MINIMAP_HEIGHT, getTeamAmount(), robots, bases);
		//localData.Init(getTeamAmount(), robots, bases);
		drawerController->sendUpdate(robots, bases, localData, iteration);
		++iteration;
	}
	
	VisualizerOpenGLDrawerController::VisualizerOpenGLDrawerController(int mapWidth, int mapHeight) : mapWidth(mapWidth), mapHeight(mapHeight)
	{
		//drawer = new VisualizerOpenGLDrawer(mapWidth, mapHeight);
	}

	void VisualizerOpenGLDrawerController::sendUpdate(const std::vector<RobotData>& robots, const std::vector<BaseData>& bases, const MinimapData& minimap, unsigned int iteration)
	{
		//assert(drawer != 0);
		::update(robots, bases, minimap, iteration);
	}

	VisualizerOpenGLDrawerController::~VisualizerOpenGLDrawerController()
	{
		/*if (drawer != 0)
		{
			delete drawer;
		}*/
	}

	VisualizerOpenGLController::~VisualizerOpenGLController()
	{
		if(drawerController)
		{
			delete drawerController;
		}
	}
}