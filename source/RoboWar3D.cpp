#include "RoboWar3D.h"
#include "Types.h"
#include "TextureLoader.h"
#include "Emulator.h"
#include "Robot.h"
#include "Sound.h"
#include "TerrainGen.h"
#include "NoiseGen.h"
#include "Animation.h"

#include <stdlib.h>
#include <time.h>
#include <math.h>

#include <pthread.h>
#include <list>
#include <map>
#include <set>

#include <fstream>
#include <iostream>

#include <GL/glext.h>
#include <GL/glut.h>

using namespace std;
using namespace robo_vis;
using namespace sound_engine;
using namespace animation;

extern int ROBOT_HP, BASE_HP, RELOAD_TIME, ROBOT_CONSTRUCTION_TIME;

extern vector<string> bot_names;

Rect3D world; // 3D scene cube

vector<Point3D> field_points; // centers of game field hexagons
unsigned int hex_num_x, hex_num_y; // dims of game field in hexagons

vector<RobotData> robots; // robot data
vector<BaseData> bases; // base data

vector<Point3D> robot_coords; // curr positions of robots (they are moving)

Point3D eye(0, -5, 1); // position of camera in the world
Point3D look(0, 1, 0); // where camera is looking

int window_width; // curr width of app window
int window_height; // curr height of app window

bool mouse_left = false; // if mouse left button is pressed
bool mouse_right = false; // if mouse right button is pressed

int mouse_pos_x, mouse_pos_y; // curr pos of mouse cursor

float hex_radius; // radius of 2d hex in the game world

vector<Point2D> hex2d_coords; // vertexes of unit 2d hex

TextureLoader tex_loader;

enum DisplayListName // names of display lists
{
	DL_GAME_FIELD = 0,
	DL_TERRAIN,
	DL_SKY_BOX,
	DL_HEX2D,
	DL_HEX3D,
	DL_BASE,
	DL_ROBOT,
	DL_LAVA
};

map<DisplayListName, GLubyte> display_lists; // display list for fast drawing
map<DisplayListName, int> display_lists_num; // num of display lists

enum TextureName // names of textures
{
	T_LANDSCAPE = 0,
	T_GROUND,
	T_STARS,
	T_ROCK,
	T_CRACKED,
	T_RUST,
	T_LAVA,
	T_SMOKE,
	T_FIRE
};

map<TextureName, glTexture> textures; // textures used

pthread_mutex_t data_mutex;
pthread_mutex_t anim_mutex;
pthread_cond_t anim_cond;
pthread_t emulator_thread;

bool free_to_next = true; // if is free to draw next iteration

bool update_view = true; // if update of camera view is needed
bool update_list = true; // if update of display lists is needed
bool scene_ready = false; // if is ready to draw scene

const GLfloat colors[][3] = 
	{{1.0f, 0.0f, 0.0f}, {1.0f, 1.0f, 0.0f}, {0.0f, 1.0f, 0.0f}, 
	{1.0f, 0.0f, 1.0f}, {0.0f, 1.0f, 1.0f}, {1.0f, 0.6f, 0.0f}, {1.0f, 0.8f, 0.6f},
	{0.0f, 0.5f, 0.9f}, {1.0f, 0.5f, 0.7f}, {0.5f, 0.7f, 0.7f}}; // colors of teams

const string team_prefix[2] = {"terran/m", "protoss/p"};
const int team_sound_data[2][3] = {{5, 17, 10}, {6, 23, 7}}; // ready, talk, death
const int sound_data[2] = {18, 9}; // fire, hit

SoundEngine sound, music;

list<BulletAnimation*> bullets;
list<MoveAnimation*> moves;
list<HitAnimation*> hits;
list<ParticleAnimation*> smoke;
list<ParticleAnimation*> fire;

GLfloat* particle_data_array = 0;
unsigned int particle_data_capacity = 0;
unsigned int smoke_particles_num = 0, fire_particles_num = 0; // num of particles in array

GLfloat* terrain_data_array = 0; // array of terrain vertexes and normals
GLushort* terrain_index_array = 0; // array of terrain indexes
unsigned int terrain_size_x, terrain_size_y;

GLfloat* field_data_array = 0; // game field hexes coord and normal array
GLuint* field_index_array = 0; // game field index array

GLfloat* life_vertex_array = 0; // coords for robot/base life bar
GLfloat* cooldown_vertex_array = 0; // coords for robot/base cooldown bar
unsigned int robot_lifebar_size = 0, robot_cooldown_size = 0;
unsigned int base_lifebar_size = 0, base_cooldown_size = 0;

const unsigned int life_scale = 10; // scale when showing life bar
const unsigned int cooldown_scale = 10; // scale when showing cooldown bar

int bases_rot_angle = 0;
int lava_move_step = 0;
int curr_iteration = 0;

GLubyte* minimap = 0; // minimap image

// VBO data and functions
enum VertexBufferName // name of buffers to greate
{
	VBO_TERRAIN_DATA = 1,	
	VBO_TERRAIN_INDEX,
	VBO_FIELD_DATA,
	VBO_FIELD_INDEX,
	VBO_PARTICLE_DATA,
	VBO_LIFE_DATA,	
	VBO_COOLDOWN_DATA
};

map<VertexBufferName, GLuint> vertex_buffers;

bool gl_vbo_support = false;
bool gl_point_sprite_support = false;

// Extension functions
PFNGLGENBUFFERSARBPROC glGenBuffersARB = 0;
PFNGLDELETEBUFFERSARBPROC glDeleteBuffersARB = 0;
PFNGLBINDBUFFERARBPROC glBindBufferARB = 0;
PFNGLBUFFERDATAARBPROC glBufferDataARB = 0;
PFNGLBUFFERSUBDATAARBPROC glBufferSubDataARB = 0;

PFNGLPOINTPARAMETERFARBPROC glPointParameterfARB = 0;		
PFNGLPOINTPARAMETERFVARBPROC glPointParameterfvARB = 0;

Config3D conf3d; // config 

void DrawHex2D(float r)
{	
	glBegin(GL_LINE_LOOP); // draw 2d hex
	for(int i = 0;i < 6;i++)
	{		
		glVertex3f(r*hex2d_coords[i].x, r*hex2d_coords[i].y, 0.0);
	}
	glEnd();
}

void CountNormal(const Vector3D& v1, const Vector3D& v2, Vector3D& normal) // count normal by vector prod
{
	normal.Set(v1.y*v2.z - v1.z*v2.y, v1.z*v2.x - v1.x*v2.z, v1.x*v2.y - v1.y*v2.x);
}

void GenerateTerrain() // generate detailed terrain around game field
{
	const int num_x = terrain_size_x; // num of quads by x
	const int num_y = terrain_size_y; // num of quads by y

	if(!terrain_data_array) terrain_data_array = new GLfloat[num_x*num_y*3*2]; // array of vertexes	and normals
	if(!terrain_index_array) terrain_index_array = new GLushort[(num_x - 1)*(num_y - 1)*6]; // indexes for quads

	GLfloat* terrain_vertex_array = terrain_data_array; // set offsets
	GLfloat* terrain_normal_array = terrain_vertex_array + num_x*num_y*3;

	const Point3D& beg = field_points[0]; // left top point
	const Point3D& end = field_points[hex_num_x*hex_num_y - 1]; // right bottom point

	const float bx = beg.x - 4*hex_radius;
	const float by = beg.y + 4*hex_radius;
	const float ex = end.x + 4*hex_radius;
	const float ey = end.y - 4*hex_radius;

	const float sz_x = fabs(ex - bx);
	const float sz_y = fabs(by - ey);

	const float max = (sz_x > sz_y) ? sz_x : sz_y; // get max size
	const float quad_sz = (world.max_x - world.min_x)/(num_x - 1);

	TerrainGenerator tgen;

	tgen.Init(num_x, num_y, quad_sz, quad_sz, Point2D(world.min_x, world.max_y)); // init generator
	tgen.GenFaultCircle(1000, quad_sz/4.0f, (max*0.48f)*sqrt(2.0f), world.max_x*0.9f); // generate terrain by Fault alg
	tgen.Smooth(0.8f);

	const Point2D& origin = tgen.Origin();

	const int j_beg = ((bx - quad_sz) - origin.x)/quad_sz;
	const int j_end = ((ex + quad_sz) - origin.x)/quad_sz;
	const int i_beg = (origin.y - (by + quad_sz))/quad_sz;
	const int i_end = (origin.y - (ey - quad_sz))/quad_sz;

	for(int i = i_beg;i <= i_end;i++) // post modify terrain to show game field
	for(int j = j_beg;j <= j_end;j++) 
	{				
		tgen.heights[i*num_x + j] = -0.2;//(float((rand() % 100))/100.0f)*float(hex_radius)*0.29f; // make low terrain
	}

	const vector<float>& heights = tgen.heights;

	int ind = 0;
	for(int i = 0;i < num_y;i++) // init vertex array
	for(int j = 0;j < num_x;j++) 
	{
		terrain_vertex_array[ind] = origin.x + j*quad_sz; // x coord
		terrain_vertex_array[ind + 1] = origin.y - i*quad_sz; // y coord
		terrain_vertex_array[ind + 2] = heights[i*num_x + j]; // z coord
		ind += 3;
	}

	Vector3D v[4], n[4]; // tmp data

	ind = 0;
	for(int i = 0;i < num_y;i++) // count normals
	for(int j = 0;j < num_x;j++)
	{		
		const float& h = heights[i*num_x + j];
		if(j > 0) 
			v[0].Set(-quad_sz, 0, heights[i*num_x + j - 1] - h); // vector to left
		else 
			v[0].Set(-quad_sz, 0, 0); // vector to left

		if(i > 0) 
			v[1].Set(0, quad_sz, heights[(i - 1)*num_x + j] - h); // vector to top
		else 
			v[1].Set(0, quad_sz, 0); // vector to top

		if(j < num_x - 1)
			v[2].Set(quad_sz, 0, heights[i*num_x + j + 1] - h); // vector to right
		else
			v[2].Set(quad_sz, 0, 0); // vector to right

		if(i < num_y - 1)
			v[3].Set(0, -quad_sz, heights[(i + 1)*num_x + j] - h); // vector to bottom
		else
			v[3].Set(0, -quad_sz, 0); // vector to bottom
				
		CountNormal(v[0], v[3], n[0]); CountNormal(v[3], v[2], n[1]);
		CountNormal(v[2], v[1], n[2]); CountNormal(v[1], v[0], n[3]);

		terrain_normal_array[ind] =		n[0].x + n[1].x + n[2].x + n[3].x; // init normal array
		terrain_normal_array[ind + 1] = n[0].y + n[1].y + n[2].y + n[3].y;
		terrain_normal_array[ind + 2] = n[0].z + n[1].z + n[2].z + n[3].z;
		ind += 3;
	}

	int ind2 = 0;
	for(int i = 0;i < num_y - 1;i++) // init indexes
	for(int j = 0;j < num_x - 1;j++)
	{
		ind = i*num_x + j;		
		// first triangle
		terrain_index_array[ind2] = GLushort(ind); // [i, j] point
		terrain_index_array[ind2 + 1] = GLushort(ind + 1); // [i, j + 1] point
		terrain_index_array[ind2 + 2] = GLushort(ind + num_x); // [i + 1, j] point

		// second triangle
		terrain_index_array[ind2 + 3] = GLushort(ind + 1); // [i, j + 1] point
		terrain_index_array[ind2 + 4] = GLushort(ind + num_x + 1); // [i + 1, j + 1] point
		terrain_index_array[ind2 + 5] = GLushort(ind + num_x); // [i + 1, j] point
		ind2 += 6;
	}
}

void DrawTerrain()
{
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_LIGHTING);
	glEnable(GL_COLOR_MATERIAL);

	const int num_x = terrain_size_x; // num of quads by x
	const int num_y = terrain_size_y; // num of quads by y

	GLfloat* terrain_vertex_array = terrain_data_array; // set offsets
	GLfloat* terrain_normal_array = terrain_vertex_array + num_x*num_y*3;

	glEnable(GL_TEXTURE_GEN_S); // set tex coord auto generation
	glEnable(GL_TEXTURE_GEN_T);

	glBindTexture(GL_TEXTURE_2D, textures[T_ROCK].TextureID); // draw terrain
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	const float scale = 5.0f/world.max_x;

	GLfloat param_s[4] = {scale, 0.0f, 0.0f, 0.0f};
	GLfloat param_t[4] = {0.0f, scale, 0.0f, 0.0f};
	
	glTexGenfv(GL_S, GL_OBJECT_PLANE, param_s);
	glTexGenfv(GL_T, GL_OBJECT_PLANE, param_t);

	glTexGenf(GL_S, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);
	glTexGenf(GL_T, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);

	glColor3f(0.6f, 0.6f, 0.6f); // set base under color for terrain

	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);

	if(gl_vbo_support) // use VBO
	{
		glBindBufferARB(GL_ARRAY_BUFFER_ARB, vertex_buffers[VBO_TERRAIN_DATA]); // bind data buffer				
		glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, vertex_buffers[VBO_TERRAIN_INDEX]); // bind index buffer		

		glVertexPointer(3, GL_FLOAT, 0, 0);
		glNormalPointer(GL_FLOAT, 0, (char*)0 + ((char*)terrain_normal_array - (char*)terrain_vertex_array));
		glDrawElements(GL_TRIANGLES, (num_x - 1)*(num_y - 1)*6, GL_UNSIGNED_SHORT, 0); // draw terrain		

		glBindBufferARB(GL_ARRAY_BUFFER_ARB, 0); // unbind
		glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, 0);
	}
	else // use only arrays
	{
		glVertexPointer(3, GL_FLOAT, 0, terrain_vertex_array);
		glNormalPointer(GL_FLOAT, 0, terrain_normal_array);		
		glDrawElements(GL_TRIANGLES, (num_x - 1)*(num_y - 1)*6, GL_UNSIGNED_SHORT, terrain_index_array); // draw terrain
	}

	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);

	glDisable(GL_TEXTURE_GEN_S);
	glDisable(GL_TEXTURE_GEN_T);

	glDisable(GL_TEXTURE_2D);
	glDisable(GL_LIGHTING);
	glDisable(GL_COLOR_MATERIAL);
}

void DrawSkyBox()
{
	glEnable(GL_TEXTURE_2D);

	glColor3f(1.0f, 1.0f, 1.0f);

	glBindTexture(GL_TEXTURE_2D, textures[T_LANDSCAPE].TextureID); // draw landscape

	glBegin(GL_QUADS);
		glTexCoord2f(0.05f, 0.0f); glVertex3f(world.min_x, world.min_y, world.min_z);
		glTexCoord2f(0.5f, 0.0f); glVertex3f(world.min_x, world.max_y, world.min_z);
		glTexCoord2f(0.5f, 1.0f); glVertex3f(world.min_x, world.max_y, world.max_z);
		glTexCoord2f(0.05f, 1.0f); glVertex3f(world.min_x, world.min_y, world.max_z);

		glTexCoord2f(0.5f, 0.0f); glVertex3f(world.min_x, world.max_y, world.min_z);
		glTexCoord2f(0.995f, 0.0f); glVertex3f(world.max_x, world.max_y, world.min_z);
		glTexCoord2f(0.995f, 1.0f); glVertex3f(world.max_x, world.max_y, world.max_z);
		glTexCoord2f(0.5f, 1.0f); glVertex3f(world.min_x, world.max_y, world.max_z);

		glTexCoord2f(0.5f, 0.0f); glVertex3f(world.max_x, world.min_y, world.min_z);
		glTexCoord2f(0.995f, 0.0f); glVertex3f(world.max_x, world.max_y, world.min_z);
		glTexCoord2f(0.995f, 1.0f); glVertex3f(world.max_x, world.max_y, world.max_z);
		glTexCoord2f(0.5f, 1.0f); glVertex3f(world.max_x, world.min_y, world.max_z);

		glTexCoord2f(0.05f, 0.0f); glVertex3f(world.min_x, world.min_y, world.min_z);
		glTexCoord2f(0.5f, 0.0f); glVertex3f(world.max_x, world.min_y, world.min_z);
		glTexCoord2f(0.5f, 1.0f); glVertex3f(world.max_x, world.min_y, world.max_z);
		glTexCoord2f(0.05f, 1.0f); glVertex3f(world.min_x, world.min_y, world.max_z);
	glEnd();  

	glBindTexture(GL_TEXTURE_2D, textures[T_STARS].TextureID); // draw stars
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glBegin(GL_QUADS);				
		glTexCoord2d(0, 0); glVertex3f(world.min_x, world.min_y, world.max_z);
		glTexCoord2d(5, 0); glVertex3f(world.max_x, world.min_y, world.max_z);
		glTexCoord2d(5, 5); glVertex3f(world.max_x, world.max_y, world.max_z);
		glTexCoord2d(0, 5); glVertex3f(world.min_x, world.max_y, world.max_z);		
	glEnd();
	
	glDisable(GL_TEXTURE_2D);
}

void DrawLava()
{
	glEnable(GL_TEXTURE_2D);	

	glBindTexture(GL_TEXTURE_2D, textures[T_LAVA].TextureID); // draw lava
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glBegin(GL_QUADS); // draw as one textured quad
		glTexCoord2f(0, 0); glVertex3f(world.min_x, world.min_y, 0);
		glTexCoord2f(0, 15); glVertex3f(world.min_x, world.max_y, 0);
		glTexCoord2f(15, 15); glVertex3f(world.max_x, world.max_y, 0);
		glTexCoord2f(15, 0); glVertex3f(world.max_x, world.min_y, 0);
	glEnd();	

	glDisable(GL_TEXTURE_2D);	
}

void GenerateGameField(float r, float height) // generate arrays for game field draw
{
	const unsigned int sz = field_points.size();

	if(!field_data_array) field_data_array = new GLfloat[(sz*7 + sz*12 + sz*6)*3 + (sz*7 + sz*12)*3]; // points	and normals
	if(!field_index_array) field_index_array = new GLuint[sz*6*3 + sz*6*4 + sz*6*2]; // 6 trngs, 6 quads, 6 lines for each hex

	GLfloat* field_vertex_array = field_data_array; // set offsets
	GLfloat* field_normal_array = field_vertex_array + (sz*7 + sz*12 + sz*6)*3;

	int src = 0, dst = 0;

	vector<Point2D> hex2d(6);
	for(int i = 0;i < 6;i++) hex2d[i].Set(r*hex2d_coords[i].x, r*hex2d_coords[i].y); // init tmp points

	for(vector<Point3D>::const_iterator it = field_points.begin();it != field_points.end();it++) // init vertexes for top hex side
	{				
		field_vertex_array[dst] = (*it).x; // center point
		field_vertex_array[dst + 1] = (*it).y; 
		field_vertex_array[dst + 2] = height; 
		dst += 3;
		for(int i = 0;i < 6;i++)
		{						
			field_vertex_array[dst] = (*it).x + hex2d[i].x; // side point
			field_vertex_array[dst + 1] = (*it).y + hex2d[i].y; 
			field_vertex_array[dst + 2] = height; 
			dst += 3;
		}
	}
	for(vector<Point3D>::const_iterator it = field_points.begin();it != field_points.end();it++) // init vertexes for hex sides
	{				
		for(int i = 0;i < 6;i++)
		{						
			field_vertex_array[dst] = (*it).x + hex2d[i].x; // top side point
			field_vertex_array[dst + 1] = (*it).y + hex2d[i].y; 
			field_vertex_array[dst + 2] = height; 
			dst += 3;
			field_vertex_array[dst] = (*it).x + hex2d[i].x; // bottom side point
			field_vertex_array[dst + 1] = (*it).y + hex2d[i].y; 
			field_vertex_array[dst + 2] = 0; 
			dst += 3;
		}
	}
	for(vector<Point3D>::const_iterator it = field_points.begin();it != field_points.end();it++) // init vertexes for hex border line
	{				
		for(int i = 0;i < 6;i++)
		{						
			field_vertex_array[dst] = (*it).x + hex2d[i].x; // top line point
			field_vertex_array[dst + 1] = (*it).y + hex2d[i].y; 
			field_vertex_array[dst + 2] = height*1.05f; 
			dst += 3;	
		}
	}

	dst = 0;
	for(unsigned int ind = 0;ind < sz*7;ind++) // init normals for top side
	{
		src = dst;
		field_normal_array[dst] = field_vertex_array[src];
		field_normal_array[dst + 1] = field_vertex_array[src + 1];
		field_normal_array[dst + 2] = field_vertex_array[src + 2] + 10.0f;
		dst += 3;
	}
	for(unsigned int ind = 0;ind < sz;ind++) // init normals for sides
	{
		for(int i = 0;i < 6;i++)
		{						
			field_normal_array[dst] = hex2d[i].x; // normal for top side point
			field_normal_array[dst + 1] = hex2d[i].y; 
			field_normal_array[dst + 2] = 0; 
			dst += 3;
			field_normal_array[dst] = hex2d[i].x; // normal for bottom side point
			field_normal_array[dst + 1] = hex2d[i].y; 
			field_normal_array[dst + 2] = 0; 
			dst += 3;
		}
	}

	dst = 0;
	for(unsigned int ind = 0;ind < sz;ind++) // init indices for top side
	{
		src = ind*7; // source - top points
		for(int i = 0;i < 6;i++) // 6 triangles
		{			
			field_index_array[dst] = src; // center point
			field_index_array[dst + 1] = src + (i % 6) + 1; // side points
			field_index_array[dst + 2] = src + ((i + 1) % 6) + 1;
			dst += 3;
		}
	}
	for(unsigned int ind = 0;ind < sz;ind++) // init indices for sides
	{
		src = sz*7 + ind*12; // source - side points
		for(int i = 0;i < 6;i++) // 6 quads
		{			
			field_index_array[dst] = src + i*2; // top point
			field_index_array[dst + 1] = src + i*2 + 1; // bottom point
			field_index_array[dst + 2] = src + (i*2 + 3)%12; // next bottom point
			field_index_array[dst + 3] = src + (i*2 + 2)%12; // next top point
			dst += 4;
		}
	}
	for(unsigned int ind = 0;ind < sz;ind++) // init indices for border line
	{
		src = sz*7 + sz*12 + ind*6; // source - border line points
		for(int i = 0;i < 6;i++) // 6 lines
		{						
			field_index_array[dst] = src + i;
			field_index_array[dst + 1] = src + (i + 1)%6;
			dst += 2;
		}
	}
}

void DrawGameField()
{
	glEnable(GL_LIGHTING);
	glEnable(GL_COLOR_MATERIAL);

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_TEXTURE_GEN_S); // set tex coord auto generation
	glEnable(GL_TEXTURE_GEN_T);

	glBindTexture(GL_TEXTURE_2D, textures[T_CRACKED].TextureID); // draw terrain	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	
	const float scale = 0.05f;

	GLfloat param_s[4] = {scale, 0.0f, scale, 0.0f};
	GLfloat param_t[4] = {0.0f, scale, scale, 0.0f};
	
	glTexGenfv(GL_S, GL_OBJECT_PLANE, param_s);
	glTexGenfv(GL_T, GL_OBJECT_PLANE, param_t);

	glTexGenf(GL_S, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);
	glTexGenf(GL_T, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);

	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);

	const unsigned int sz = hex_num_x*hex_num_y;

	const GLfloat* field_vertex_array = field_data_array; // set offsets
	const GLfloat* field_normal_array = field_vertex_array + (sz*7 + sz*12 + sz*6)*3;

	glColor3f(0.0f, 0.0f, 0.95f);

	if(gl_vbo_support)
	{
		glBindBufferARB(GL_ARRAY_BUFFER_ARB, vertex_buffers[VBO_FIELD_DATA]); // bind data buffer				
		glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, vertex_buffers[VBO_FIELD_INDEX]); // bind index buffer		

		glVertexPointer(3, GL_FLOAT, 0, 0);
		glNormalPointer(GL_FLOAT, 0, (char*)0 + ((char*)field_normal_array - (char*)field_vertex_array));
		glDrawElements(GL_TRIANGLES, sz*6*3, GL_UNSIGNED_INT, 0); // draw hex tops				
		glDrawElements(GL_QUADS, sz*6*4, GL_UNSIGNED_INT, (char*)0 + sz*6*3*sizeof(GLuint)); // draw hex sides
	}
	else
	{
		glVertexPointer(3, GL_FLOAT, 0, field_vertex_array);
		glNormalPointer(GL_FLOAT, 0, field_normal_array);
		glDrawElements(GL_TRIANGLES, sz*6*3, GL_UNSIGNED_INT, field_index_array); // draw hex tops	
		glDrawElements(GL_QUADS, sz*6*4, GL_UNSIGNED_INT, field_index_array + sz*6*3); // draw hex sides
	}
	
	glDisableClientState(GL_NORMAL_ARRAY);

	glDisable(GL_TEXTURE_GEN_S); 
	glDisable(GL_TEXTURE_GEN_T);
	glDisable(GL_TEXTURE_2D);

	glDisable(GL_LIGHTING);
	glDisable(GL_COLOR_MATERIAL);

	glLineWidth(1.0f);
	glColor3f(0.3f, 0.0f, 0.6f);

	if(gl_vbo_support)
	{		
		glDrawElements(GL_LINES, sz*6*2, GL_UNSIGNED_INT, (char*)0 + (sz*6*3 + sz*6*4)*sizeof(GLuint)); // draw hex border lines

		glBindBufferARB(GL_ARRAY_BUFFER_ARB, 0); // unbind
		glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, 0);
	}
	else
	{
		glDrawElements(GL_LINES, sz*6*2, GL_UNSIGNED_INT, field_index_array + sz*6*3 + sz*6*4); // draw hex border lines
	}
	
	glDisableClientState(GL_VERTEX_ARRAY);
}

void InitGL()
{
	glShadeModel(GL_SMOOTH);
	glEnable(GL_DEPTH_TEST);

	//glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	//glEnable(GL_POLYGON_SMOOTH);
	//glEnable(GL_LINE_SMOOTH);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);	
	glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);	

	//glEnable(GL_MULTISAMPLE);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glEnable(GL_NORMALIZE);

	//glDisable(GL_COLOR_MATERIAL);

	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);	

	glFrontFace(GL_CW);

	//glEnable(GL_CULL_FACE);
	glCullFace(GL_CCW);

	particle_data_capacity = 2000; // start with space for 2000 particles
	particle_data_array = new GLfloat[(3 + 4)*particle_data_capacity]; // for particle vertexes	and colors

	terrain_size_x = 201; // make fixes size terrain (but no more then 65365 points)
	terrain_size_y = 201;	
}

void InitGLExt() // init additional function
{
	gl_vbo_support = (strstr((char*)glGetString(GL_EXTENSIONS), "GL_ARB_vertex_buffer_object") != 0); // check support for VBO
	if(!gl_vbo_support)
	{
		cerr << "Warning: GL vertex buffers aren't supported" << endl;
	}
	else // init vbo functions
	{
		glGenBuffersARB = (PFNGLGENBUFFERSARBPROC)wglGetProcAddress("glGenBuffersARB");
		glDeleteBuffersARB = (PFNGLDELETEBUFFERSARBPROC)wglGetProcAddress("glDeleteBuffersARB");
		glBindBufferARB = (PFNGLBINDBUFFERARBPROC)wglGetProcAddress("glBindBufferARB");
		glBufferDataARB = (PFNGLBUFFERDATAARBPROC)wglGetProcAddress("glBufferDataARB");
		glBufferSubDataARB = (PFNGLBUFFERSUBDATAARBPROC)wglGetProcAddress("glBufferSubDataARB");

		if(!glGenBuffersARB || !glDeleteBuffersARB || !glBindBufferARB || !glBufferDataARB || !glBufferSubDataARB)
		{
			cerr << "Error: failed to initialize GL vertex buffer functions" << endl;			
			gl_vbo_support = false;
		}
	}
	if(!gl_vbo_support)
	{
		cerr << "Warning: can't use GL vertex buffers" << endl;
	}

	gl_point_sprite_support = (strstr((char*)glGetString(GL_EXTENSIONS), "GL_ARB_point_sprite") != 0);
	if(!gl_point_sprite_support)
	{
		cerr << "Warning: GL point sprites aren't supported" << endl;
	}
	else
	{
		glPointParameterfARB = (PFNGLPOINTPARAMETERFARBPROC)wglGetProcAddress("glPointParameterfARB");
		glPointParameterfvARB = (PFNGLPOINTPARAMETERFVARBPROC)wglGetProcAddress("glPointParameterfvARB"); 

		if(!glPointParameterfARB || !glPointParameterfvARB)
		{
			cerr << "Error: failed to initialize GL point sprites functions" << endl;			
			gl_point_sprite_support = false;
		}
	}
	if(!gl_point_sprite_support)
	{
		cerr << "Warning: can't use GL point sprites. Particle effects are turned off" << endl;		
	}
}

void InitGLLight()
{
	GLfloat l_pos[] = {0.0f, 0.0f, world.max_z/2.0f, 0.0f};
	GLfloat l_diff[] = {0.3f, 0.3f, 0.3f, 0.0f};
	GLfloat l_spec[] = {0.95f, 0.95f, 0.95f, 0.0f};

	glLightfv(GL_LIGHT0, GL_POSITION, l_pos);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, l_diff);
	glLightfv(GL_LIGHT0, GL_SPECULAR, l_spec);
}

void InitGLMat()
{	
	GLfloat m_spec[] = {1.0f, 1.0f, 1.0f, 1.0f};
	GLfloat m_emis[] = {0.1f, 0.1f, 0.1f, 1.0f};
	GLfloat m_shine = 10;
	
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, m_spec);
	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, m_emis);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, m_shine);
}

void InitGLView()
{
	glViewport(0, 0, window_width, window_height);
	glMatrixMode(GL_PROJECTION);
		glLoadIdentity();				
		GLfloat aspect = GLfloat(window_width)/GLfloat(window_height);
		gluPerspective(60, aspect, 0.5f, 2.5*world.max_x);
	glMatrixMode(GL_MODELVIEW);		
		glLoadIdentity();		
		gluLookAt(eye.x, eye.y, eye.z, eye.x + look.x, eye.y + look.y, look.z, 0, 0, 1);
	glMatrixMode(GL_MODELVIEW);		
}

void InitGLTextures()
{
	tex_loader.SetTextureFilter(txAnisotrophic);
	tex_loader.SetMipMapping(TRUE);

	tex_loader.LoadTextureFromDisk("Textures/landscape.jpg", &textures[T_LANDSCAPE]);	
	tex_loader.LoadTextureFromDisk("Textures/stars.jpg", &textures[T_STARS]);
	tex_loader.LoadTextureFromDisk("Textures/rock.jpg", &textures[T_ROCK]);
	tex_loader.LoadTextureFromDisk("Textures/cracked.jpg", &textures[T_CRACKED]);	
	tex_loader.LoadTextureFromDisk("Textures/lava.jpg", &textures[T_LAVA]);

	NoiseGenerator n_gen;
	n_gen.Init(128, 128);

	n_gen.GenPerlin(7, 0.7f, true);
	tex_loader.LoadProcTexture(128, 128, &(n_gen.noise[0]), &textures[T_SMOKE]); // gen smoke texture

	n_gen.GenPerlin(3, 1.0f, true);
	tex_loader.LoadProcTexture(128, 128, &(n_gen.noise[0]), &textures[T_FIRE]); // gen fire texture
}

void ClearGLTextures()
{
	for(map<TextureName, glTexture>::iterator it = textures.begin();it != textures.end();it++)
	{
		tex_loader.FreeTexture(&((*it).second));
	}
	textures.clear();
}

void ClearGLArrays() // clear vertex arrays
{
	if(field_data_array) delete[] field_data_array; // data already is in list - delete unused arrays
	if(field_index_array) delete[] field_index_array;

	field_data_array = 0;	
	field_index_array = 0;

	if(terrain_data_array) delete[] terrain_data_array; // list is created, delete arrays
	if(terrain_index_array) delete[] terrain_index_array;

	terrain_data_array = 0; // set arrays to 0
	terrain_index_array = 0;	

	if(life_vertex_array) delete[] life_vertex_array;
	if(cooldown_vertex_array) delete[] cooldown_vertex_array;
	life_vertex_array = 0;
	cooldown_vertex_array = 0;
}

void InitGLDisplayLists()
{
	display_lists_num[DL_HEX2D] = 1;
	display_lists[DL_HEX2D] = glGenLists(display_lists_num[DL_HEX2D]);
	glNewList(display_lists[DL_HEX2D], GL_COMPILE);
		DrawHex2D(hex_radius*0.5f);
	glEndList();

	display_lists_num[DL_SKY_BOX] = 1; // create sky box list
	display_lists[DL_SKY_BOX] = glGenLists(display_lists_num[DL_SKY_BOX]);
	glNewList(display_lists[DL_SKY_BOX], GL_COMPILE);
		DrawSkyBox();
	glEndList();

	display_lists_num[DL_LAVA] = 1; // create lava list
	display_lists[DL_LAVA] = glGenLists(display_lists_num[DL_LAVA]);
	glNewList(display_lists[DL_LAVA], GL_COMPILE);
		DrawLava();
	glEndList();

	display_lists_num[DL_BASE] = 1;
	display_lists[DL_BASE] = glGenLists(display_lists_num[DL_BASE]);
	glNewList(display_lists[DL_BASE], GL_COMPILE);
		glutSolidTeapot(hex_radius); // draw base (as teapot)
	glEndList();

	display_lists_num[DL_ROBOT] = ROBOT_HP + 1; // several lists for robots
	display_lists[DL_ROBOT] = glGenLists(display_lists_num[DL_ROBOT]);
	for(int i = 0;i <= ROBOT_HP;i++) // separate list for each hitpoint value
	{
		glNewList(display_lists[DL_ROBOT] + i, GL_COMPILE);
		int n = (10*(i + 1))/(ROBOT_HP + 1) + 5;
			glutSolidSphere(hex_radius/2.0, n, n); // draw robot (as sphere)
		glEndList();
	}
}

void ClearGLDisplayLists()
{
	for(map<DisplayListName, GLubyte>::iterator it = display_lists.begin();it != display_lists.end();it++)
	{
		glDeleteLists((*it).second, display_lists_num[(*it).first]);
	}
	display_lists.clear();
	display_lists_num.clear();
}

void InitGLVertexBuffers() // gen vertex buffers for gl
{	
	glGenBuffersARB(1, &vertex_buffers[VBO_TERRAIN_DATA]);
	glGenBuffersARB(1, &vertex_buffers[VBO_TERRAIN_INDEX]);
	glGenBuffersARB(1, &vertex_buffers[VBO_FIELD_DATA]);
	glGenBuffersARB(1, &vertex_buffers[VBO_FIELD_INDEX]);
	glGenBuffersARB(1, &vertex_buffers[VBO_PARTICLE_DATA]);		
	glGenBuffersARB(1, &vertex_buffers[VBO_LIFE_DATA]);			
	glGenBuffersARB(1, &vertex_buffers[VBO_COOLDOWN_DATA]);		
}

void ClearGLVertexBuffers()
{	
	for(map<VertexBufferName, GLuint>::iterator it = vertex_buffers.begin();it != vertex_buffers.end();it++)
	{
		GLuint arr[1] = {(*it).second};
		glDeleteBuffersARB(1, arr);
	}
	vertex_buffers.clear();
}

void UploadGLVertexBuffers() // upload terrain and field data to VBO
{
	const unsigned int num_x = terrain_size_x;
	const unsigned int num_y = terrain_size_y;
	
	// load terrain vbo
	glBindBufferARB(GL_ARRAY_BUFFER_ARB, vertex_buffers[VBO_TERRAIN_DATA]); // bind data buffer		
	glBufferDataARB(GL_ARRAY_BUFFER_ARB, num_x*num_y*3*2*sizeof(GLfloat), terrain_data_array, GL_STATIC_DRAW_ARB); // load coords and normals	
	glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, vertex_buffers[VBO_TERRAIN_INDEX]); // bind index buffer
	glBufferDataARB(GL_ELEMENT_ARRAY_BUFFER_ARB, (num_x - 1)*(num_y - 1)*6*sizeof(GLushort), terrain_index_array, GL_STATIC_DRAW_ARB); // load index array				

	const unsigned int sz = hex_num_x*hex_num_y;
		
	// load game field vbo
	glBindBufferARB(GL_ARRAY_BUFFER_ARB, vertex_buffers[VBO_FIELD_DATA]); // bind data buffer		
	glBufferDataARB(GL_ARRAY_BUFFER_ARB, ((sz*7 + sz*12 + sz*6)*3 + (sz*7 + sz*12)*3)*sizeof(GLfloat), field_data_array, GL_STATIC_DRAW_ARB); // load coords and normals
	glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, vertex_buffers[VBO_FIELD_INDEX]); // bind index buffer
	glBufferDataARB(GL_ELEMENT_ARRAY_BUFFER_ARB, (sz*6*3 + sz*6*4 + sz*6*2)*sizeof(GLuint), field_index_array, GL_STATIC_DRAW_ARB); // load index array			

	glBindBufferARB(GL_ARRAY_BUFFER_ARB, vertex_buffers[VBO_LIFE_DATA]); // bind data buffer		
    glBufferDataARB(GL_ARRAY_BUFFER_ARB, (robot_lifebar_size + base_lifebar_size)*3*sizeof(GLfloat), life_vertex_array, GL_STATIC_DRAW_ARB); // load life points

	glBindBufferARB(GL_ARRAY_BUFFER_ARB, vertex_buffers[VBO_COOLDOWN_DATA]); // bind data buffer		
	glBufferDataARB(GL_ARRAY_BUFFER_ARB, (robot_cooldown_size + base_cooldown_size)*3*sizeof(GLfloat), cooldown_vertex_array, GL_STATIC_DRAW_ARB); // load life points

	glBindBufferARB(GL_ARRAY_BUFFER_ARB, 0); // unbind data buffer		
	glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, 0); // unbind index buffer	
}

void InitCircleArray(GLfloat* dt, int num, float r)
{
	const float step = 2.0f*PI/(num - 1);
	int ind = 0;
	for(int i = 0;i < num;i++)
	{
		dt[ind] = r*Cos(i*step);
		dt[ind + 1] = r*Sin(i*step);
		dt[ind + 2] = 0;
		ind += 3;
	}
}

void InitLifeVertexArray(int robot_hp, int base_hp, float robot_r, float base_r)
{	
    robot_lifebar_size = robot_hp*life_scale + 1;
    base_lifebar_size = base_hp*life_scale + 1;

    life_vertex_array = new GLfloat[(robot_lifebar_size + base_lifebar_size)*3];

    InitCircleArray(life_vertex_array, robot_lifebar_size, robot_r);
    InitCircleArray(life_vertex_array + robot_lifebar_size*3, base_lifebar_size, base_r);
}

void InitCooldownVertexArray(int robot_cooldown, int base_cooldown, float robot_r, float base_r)
{	
	robot_cooldown_size = robot_cooldown*cooldown_scale + 2;
	base_cooldown_size = base_cooldown*cooldown_scale + 2;

	cooldown_vertex_array = new GLfloat[(robot_cooldown_size + base_cooldown_size)*3];

	cooldown_vertex_array[0] = 0; // center point for triangle fan
	cooldown_vertex_array[1] = 0;
	cooldown_vertex_array[2] = 0;
	InitCircleArray(cooldown_vertex_array + 3, robot_cooldown_size - 1, robot_r);

	cooldown_vertex_array[robot_cooldown_size*3] = 0; // center point for triangle fan
	cooldown_vertex_array[robot_cooldown_size*3 + 1] = 0;
	cooldown_vertex_array[robot_cooldown_size*3 + 2] = 0;
	InitCircleArray(cooldown_vertex_array + robot_cooldown_size*3 + 3, base_cooldown_size - 1, base_r);
}

void InitMath()
{
	for(int i = 0;i < 6;i++) // count points of hexagon
	{
		hex2d_coords.push_back(Point2D(sin((float(i)/6.0)*2*PI), cos((float(i)/6.0)*2*PI)));
	}
}

void InitScene(int hex_x, int hex_y)
{
	hex_num_x = hex_x;
	hex_num_y = hex_y;

	hex_radius = 1.0;

	const float rx = hex_radius*sqrt(3.0)/2.0;

	const float sx = hex_num_x*rx*2 + (hex_num_y > 1 ? rx : 0);
	const float sy = (hex_radius + rx/2.0)*hex_num_y + (hex_radius - rx/2.0);

	Rect2D inner_field, field;
	const float ss = (sx > sy) ? sx : sy;
	inner_field.Set(-sx/2, sx/2, sy/2, -sy/2);

	field.Set(inner_field.left*1.05, inner_field.right*1.05, inner_field.top*1.05, inner_field.bottom*1.05);

	const float asp = float(textures[T_LANDSCAPE].Width)/(float(textures[T_LANDSCAPE].Height));
	const float max_sz = field.right > field.top ? field.right*4.0 : field.top*4.0;

	world.Set(-max_sz, max_sz, -max_sz, max_sz, -max_sz/asp, max_sz/asp);

	field_points.reserve(hex_num_x*hex_num_y);
	field_points.resize(hex_num_x*hex_num_y);

	float step, x, y;
	for(unsigned int i = 0;i < hex_num_y;i++)
	{
		step = (i%2 == 0 ? rx : 0.0f); // move odd row to right
		for(unsigned int j = 0;j < hex_num_x;j++)
		{
			x = inner_field.left + rx + rx*j*2 + step;
			y = inner_field.top - hex_radius - i*(hex_radius + rx*0.5f);
			field_points[i*hex_num_x + j].Set(x, y, 0);
		}
	}

	eye.Set(0, (-ss*0.5f)*1.5f, (ss*0.5f)/1.5f);
	look.Set(-eye.x/4.0f, -eye.y/4.0f, eye.z/1.5f);
}

void InitMinimap(GLubyte* mini, const MinimapData& m_data, const vector<RobotData>& rs, const vector<BaseData>& bs)
{	
    for(unsigned int i = 0;i < hex_num_y*hex_num_x*4;i++)
	{         
		mini[i] = (GLubyte)0;
    }
	
	int team, tmp;
	for(vector<RobotData>::const_iterator it = rs.begin();it != rs.end();it++) // draw robots
	{
		if((*it).hitpoints > 0)
		{
			team = (*it).team;
			tmp = ((*it).y*hex_num_x + (*it).x)*4;
				
			mini[tmp] = colors[team][0]*0.6*255;
			mini[tmp + 1] = colors[team][1]*0.6*255;
			mini[tmp + 2] = colors[team][2]*0.6*255;
			mini[tmp + 3] = 255; // no transparency
		}
	}

	for(vector<BaseData>::const_iterator it = bs.begin();it != bs.end();it++) // draw bases
	{
		team = (*it).team;
		tmp = ((*it).y*hex_num_x + (*it).x)*4;

		if(team != -1)
		{
			mini[tmp] = colors[team][0]*255;
			mini[tmp + 1] = colors[team][1]*255;
			mini[tmp + 2] = colors[team][2]*255;
			mini[tmp + 3] = 255;
		}
		else
		{
			mini[tmp] = 255;
			mini[tmp + 1] = 255;
			mini[tmp + 2] = 255;
			mini[tmp + 3] = 255;
		}
	}
}

void Reshape(int width, int height)
{
	window_width = width;
	window_height = height;
	
	InitGLView();
}

void DrawRobots(const vector<RobotData>& rs)
{
	glEnable(GL_LIGHTING); 	
	glEnable(GL_COLOR_MATERIAL);

	glMatrixMode(GL_MODELVIEW);	
	int i = 0;
	for(vector<RobotData>::const_iterator it = rs.begin();it != rs.end();it++, i++)
	{
		if((*it).hitpoints == 0) continue;

		const float scale = float((*it).hitpoints)/float(ROBOT_HP);
		const int team = (*it).team;
		
		const Point3D& p = robot_coords[i];		
		glPushMatrix(); // draw robot		
			glTranslatef(p.x, p.y, hex_radius*0.5f);
			glColor3f(colors[team][0]*scale, colors[team][1]*scale, colors[team][2]*scale);
			glCallList(display_lists[DL_ROBOT] + (*it).hitpoints);						
		glPopMatrix();		
	}
	glDisable(GL_LIGHTING);
	glDisable(GL_COLOR_MATERIAL);
}

void DrawRobotsInfo(const vector<RobotData>& rs)
{
	//glDisable(GL_LIGHTING);
	//glDisable(GL_TEXTURE_2D);

	glMatrixMode(GL_MODELVIEW);	

	float scale;
	int team, ind;

	glLineWidth(1.0f);
	for(vector<RobotData>::const_iterator it = rs.begin();it != rs.end();it++) // draw 2d hexes on prev robot place
	{
		if((*it).hitpoints == 0) continue;

		scale = (float((*it).hitpoints)/float(ROBOT_HP));
		team = (*it).team;
		
		const Point3D& fp = field_points[(*it).y*hex_num_x + (*it).x];		
		glPushMatrix(); // draw robot hex
		glTranslatef(fp.x, fp.y, fp.z + hex_radius*0.31);
			glColor3f(colors[team][0]*scale, colors[team][1]*scale, colors[team][2]*scale);			
			glCallList(display_lists[DL_HEX2D]);
		glPopMatrix();
	}

	glEnableClientState(GL_VERTEX_ARRAY);

	if(gl_vbo_support) // bind cooldown bar buffer
	{
		glBindBufferARB(GL_ARRAY_BUFFER_ARB, vertex_buffers[VBO_COOLDOWN_DATA]);
		glVertexPointer(3, GL_FLOAT, 0, 0); 
	}
	else
	{
		glVertexPointer(3, GL_FLOAT, 0, cooldown_vertex_array); 
	}

	ind = 0;

	glColor3f(0.8f, 0.8f, 0.8f);
	for(vector<RobotData>::const_iterator it = rs.begin();it != rs.end();it++, ind++) // draw robot cooldowns
	{
		if((*it).hitpoints == 0) continue;
		if((*it).cooldown > 0)
		{
			const Point3D& p = robot_coords[ind];
			glPushMatrix(); // draw robot info
				glTranslatef(p.x, p.y, hex_radius*0.75f);									
					glDrawArrays(GL_TRIANGLE_FAN, 0, (*it).cooldown*cooldown_scale + 2); // draw cooldown			
			glPopMatrix();
		}
	}

	if(gl_vbo_support) // bind life bar buffer
	{
		glBindBufferARB(GL_ARRAY_BUFFER_ARB, vertex_buffers[VBO_LIFE_DATA]);
		glVertexPointer(3, GL_FLOAT, 0, 0); 
	}
	else
	{
		glVertexPointer(3, GL_FLOAT, 0, life_vertex_array); 
	}

	ind = 0;

	glLineWidth(1.5f);
	for(vector<RobotData>::const_iterator it = rs.begin();it != rs.end();it++, ind++) // draw robot lifes
	{
		if((*it).hitpoints == 0) continue;

		team = (*it).team;
		glColor3f(colors[team][0]*0.8f, colors[team][1]*0.8f, colors[team][2]*0.8f);			

		const Point3D& p = robot_coords[ind];
		glPushMatrix(); // draw robot info
			glTranslatef(p.x, p.y, hex_radius*0.75f);									
			glDrawArrays(GL_LINE_STRIP, 0, (*it).hitpoints*life_scale + 1); // draw life bar	
		glPopMatrix();		
	}

	if(gl_vbo_support)
	{
		glBindBufferARB(GL_ARRAY_BUFFER_ARB, 0); // unbind buffer
	}
	glDisableClientState(GL_VERTEX_ARRAY);
}

void DrawBases(const vector<BaseData>& bs)
{
	glEnable(GL_LIGHTING);
	glEnable(GL_COLOR_MATERIAL);
	glMatrixMode(GL_MODELVIEW);

	float scale;
	int team;

	for(vector<BaseData>::const_iterator it = bs.begin();it != bs.end();it++)
	{
		scale = float((*it).hitpoints)/float(BASE_HP);
		team = (*it).team;

		if(team != -1)
		{
			glColor3f(colors[team][0]*scale, colors[team][1]*scale, colors[team][2]*scale);
		}
		else
		{
			glColor3f(scale, scale, scale);
		}
			
		const Point3D& p = field_points[(*it).y*hex_num_x + (*it).x];
		glPushMatrix();			
			glTranslatef(p.x, p.y, hex_radius); // draw base
			glRotatef(90.0f, 1, 0, 0);				
			glRotatef((float)bases_rot_angle, 0, 1, 0);							
			glCallList(display_lists[DL_BASE]);			
		glPopMatrix();
	}

	glDisable(GL_LIGHTING);
	glDisable(GL_COLOR_MATERIAL);
}

void DrawBasesInfo(const vector<BaseData>& bs)
{
	//glDisable(GL_LIGHTING);
	//glDisable(GL_TEXTURE_2D);

	glMatrixMode(GL_MODELVIEW);	

	float scale;		
	int team;

	glEnableClientState(GL_VERTEX_ARRAY);
	if(gl_vbo_support) // bind cooldown bar buffer
	{
		glBindBufferARB(GL_ARRAY_BUFFER_ARB, vertex_buffers[VBO_COOLDOWN_DATA]);
		glVertexPointer(3, GL_FLOAT, 0, (char*)0 + robot_cooldown_size*3*sizeof(GLfloat)); 
	}
	else
	{
		glVertexPointer(3, GL_FLOAT, 0, cooldown_vertex_array + robot_cooldown_size*3); 
	}

	glColor3f(0.8f, 0.8f, 0.8f);
	for(vector<BaseData>::const_iterator it = bs.begin();it != bs.end();it++) // draw base cooldowns
	{
		if((*it).cooldown > 0)
		{
			const Point3D& p = field_points[(*it).y*hex_num_x + (*it).x];
			glPushMatrix(); // draw robot info
				glTranslatef(p.x, p.y, hex_radius*1.5f);									
				glDrawArrays(GL_TRIANGLE_FAN, 0, (*it).cooldown*cooldown_scale + 2); // draw cooldown			
			glPopMatrix();
		}
	}

	if(gl_vbo_support) // bind life bar buffer
	{
		glBindBufferARB(GL_ARRAY_BUFFER_ARB, vertex_buffers[VBO_LIFE_DATA]);
        glVertexPointer(3, GL_FLOAT, 0, (char*)0 + robot_lifebar_size*3*sizeof(GLfloat));
	}
	else
	{
        glVertexPointer(3, GL_FLOAT, 0, life_vertex_array + robot_lifebar_size*3);
	}

	glLineWidth(2.0f);
	for(vector<BaseData>::const_iterator it = bs.begin();it != bs.end();it++) // draw base lifes
	{
		const Point3D& p = field_points[(*it).y*hex_num_x + (*it).x];

		scale = float((*it).hitpoints)/float(BASE_HP);
		team = (*it).team;
	
		if(team != -1)
		{
			glColor3f(colors[team][0]*0.8f, colors[team][1]*0.8f, colors[team][2]*0.8f);			
		}
		else // free base color
		{
			glColor3f(1.0f, 1.0f, 1.0f);			
		}

		glPushMatrix();
			glTranslatef(p.x, p.y, hex_radius*1.5f);						
			glDrawArrays(GL_LINE_STRIP, 0, (*it).hitpoints*life_scale + 1); // draw life bar	
		glPopMatrix();		
	}

	if(gl_vbo_support)
	{
		glBindBufferARB(GL_ARRAY_BUFFER_ARB, 0); // unbind buffer
	}
	glDisableClientState(GL_VERTEX_ARRAY);
}

void DrawBullets()
{
	glEnable(GL_LIGHTING);
	glEnable(GL_COLOR_MATERIAL);

	glMatrixMode(GL_MODELVIEW);
	for(list<BulletAnimation*>::const_iterator it = bullets.begin();it != bullets.end();it++)
	{
		//if(!(*it)->IsActive()) continue;
		glColor3f(colors[(*it)->team][0], colors[(*it)->team][1], colors[(*it)->team][2]);
		glPushMatrix();
		glTranslatef((*it)->curr_pos.x, (*it)->curr_pos.y, hex_radius/2.0);
		glRotatef(90, 0, 1, 0);
		glRotatef((*it)->angle, 1, 0, 0);
		glutSolidCone(hex_radius/4.0, hex_radius, 8, 8);
		glPopMatrix();
	}

	glDisable(GL_LIGHTING);
	glDisable(GL_COLOR_MATERIAL);
}

void DrawHits()
{
	glEnable(GL_LIGHTING);
	glEnable(GL_COLOR_MATERIAL);

	glMatrixMode(GL_MODELVIEW);
	for(list<HitAnimation*>::const_iterator it = hits.begin();it != hits.end();it++)
	{
		//if(!(*it)->IsActive()) continue;
		if((*it)->team == -1)
		{
			glColor3f(0.7f, 0.7f, 0.7f);
		}
		else
		{
			glColor3f(colors[(*it)->team][0]*0.7f, colors[(*it)->team][1]*0.7f, colors[(*it)->team][2]*0.7f);
		}	
		int i = 0;
		for(vector<Point3D>::const_iterator it2 = (*it)->particles.begin();it2 != (*it)->particles.end();it2++, i++)
		{
			glPushMatrix();
			glTranslatef((*it2).x, (*it2).y, (*it2).z);
			glutSolidSphere((*it)->p_rads[i], 5, 5);
			glPopMatrix();
		}
	}

	glDisable(GL_LIGHTING);
	glDisable(GL_COLOR_MATERIAL);
}

void CountParticlesData() // load particle data to arrays
{
	static bool init_buf = true; // flag to init VBO

	const unsigned int smoke_sz = smoke.size();
	const unsigned int fire_sz = fire.size();

	smoke_particles_num = smoke_sz; // init sizes
	fire_particles_num = fire_sz;

	if(particle_data_capacity < (smoke_sz + fire_sz)) // no free space - realloc arrays
	{
		delete[] particle_data_array;
		particle_data_capacity = (smoke_sz + fire_sz)*2; // increase by 2 to no realloc much in future
		particle_data_array = new GLfloat[(3 + 4)*particle_data_capacity]; // realloc
		init_buf = true; // signal to reinit VBO
	}

	GLfloat* particle_color_array = particle_data_array; // set offsets
	GLfloat* particle_vertex_array = particle_color_array + (smoke_sz + fire_sz)*4;
	
	int src = 0;
	for(list<ParticleAnimation*>::const_iterator it = smoke.begin();it != smoke.end();it++) // init particle colors for smoke
	{		
		particle_color_array[src] = (*it)->color; // grey color
		particle_color_array[src + 1] = (*it)->color;
		particle_color_array[src + 2] = (*it)->color;
		particle_color_array[src + 3] = (*it)->transparency;	
		src += 4;
	}	
	for(list<ParticleAnimation*>::const_iterator it = fire.begin();it != fire.end();it++) // color for fire
	{
		particle_color_array[src] = (*it)->color; // yellow color
		particle_color_array[src + 1] = (*it)->color;
		particle_color_array[src + 2] = 0.0f;
		particle_color_array[src + 3] = (*it)->transparency;	
		src += 4;
	}

	src = 0;
	for(list<ParticleAnimation*>::const_iterator it = smoke.begin();it != smoke.end();it++) // coords for smoke
	{	
		particle_vertex_array[src] = (*it)->curr_pos.x;
		particle_vertex_array[src + 1] = (*it)->curr_pos.y;
		particle_vertex_array[src + 2] = (*it)->curr_pos.z;
		src += 3;
	}	
	for(list<ParticleAnimation*>::const_iterator it = fire.begin();it != fire.end();it++) // coords for fire
	{
		particle_vertex_array[src] = (*it)->curr_pos.x;
		particle_vertex_array[src + 1] = (*it)->curr_pos.y;
		particle_vertex_array[src + 2] = (*it)->curr_pos.z;
		src += 3;
	}

	if(gl_vbo_support) // load data to VBO
	{
		glBindBufferARB(GL_ARRAY_BUFFER_ARB, vertex_buffers[VBO_PARTICLE_DATA]); // bind data buffer
		if(init_buf) // init buf first time or reinit buf
		{
			glBufferDataARB(GL_ARRAY_BUFFER_ARB, (3 + 4)*(particle_data_capacity)*sizeof(GLfloat), 0, GL_DYNAMIC_DRAW_ARB); // get space for buf
			init_buf = false;
		}		
		glBufferSubDataARB(GL_ARRAY_BUFFER_ARB, 0, (3 + 4)*(smoke_sz + fire_sz)*sizeof(GLfloat), particle_data_array); // load coords and colors
	}
}

void DrawParticles() // draw smoke and fire
{	
	glPushAttrib(GL_COLOR_BUFFER_BIT | GL_CURRENT_BIT | GL_DEPTH_BUFFER_BIT);

	glEnable(GL_TEXTURE_2D);

    glEnable(GL_BLEND);    
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glDepthMask(GL_FALSE); // turn off depth buffer

    static float quadratic[] = {1.0f, 0.0f, 0.01f};
	glPointParameterfvARB(GL_POINT_DISTANCE_ATTENUATION_ARB, quadratic);

	float max_sz = 0.0f;
	glGetFloatv(GL_POINT_SIZE_MAX_ARB, &max_sz);

	if(max_sz > 100.0f) max_sz = 100.0f;

    glPointParameterfARB(GL_POINT_SIZE_MAX_ARB, max_sz);
    glPointParameterfARB(GL_POINT_SIZE_MIN_ARB, 1.0f);
	//glPointParameterfARB(GL_POINT_FADE_THRESHOLD_SIZE_ARB, 60.0f);
    glTexEnvf(GL_POINT_SPRITE_ARB, GL_COORD_REPLACE_ARB, GL_TRUE);

	glTexEnvf(GL_TEXTURE_2D, GL_TEXTURE_ENV_MODE, GL_MODULATE);

    glEnable(GL_POINT_SPRITE_ARB); // enable point sprites to draw particles

	const unsigned int smoke_sz = smoke_particles_num; // get sizes, set by CountParticlesData
	const unsigned int fire_sz = fire_particles_num;

	GLfloat* particle_color_array = particle_data_array; // set offsets
	GLfloat* particle_vertex_array = particle_color_array + (smoke_sz + fire_sz)*4;

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);
	
	if(gl_vbo_support) // use vertex buffers and vertex arrays
	{
		glBindBufferARB(GL_ARRAY_BUFFER_ARB, vertex_buffers[VBO_PARTICLE_DATA]); // bind data buffer
		
		unsigned int v_step = (char*)particle_vertex_array - (char*)particle_color_array;

		glColorPointer(4, GL_FLOAT, 0, 0); // set smoke data	
		glVertexPointer(3, GL_FLOAT, 0, (char*)0 + v_step);
		
		glPointSize(max_sz*0.7f);
		glBindTexture(GL_TEXTURE_2D, textures[T_SMOKE].TextureID);
		glDrawArrays(GL_POINTS, 0, smoke_sz);

		glColorPointer(4, GL_FLOAT, 0, (char*)0 + smoke_sz*4*sizeof(GLfloat)); // set fire data
		glVertexPointer(3, GL_FLOAT, 0, (char*)0 + v_step + smoke_sz*3*sizeof(GLfloat)); 

		glPointSize(max_sz*0.5f);
		glBindTexture(GL_TEXTURE_2D, textures[T_FIRE].TextureID);
		glDrawArrays(GL_POINTS, 0, fire_sz);

		glBindBufferARB(GL_ARRAY_BUFFER_ARB, 0); // unbind
	}
	else // use only vertex arrays
	{
		glColorPointer(4, GL_FLOAT, 0, particle_color_array); // draw smoke
		glVertexPointer(3, GL_FLOAT, 0, particle_vertex_array);

		glPointSize(max_sz*0.7f);
		glBindTexture(GL_TEXTURE_2D, textures[T_SMOKE].TextureID);
		glDrawArrays(GL_POINTS, 0, smoke_sz);

		glColorPointer(4, GL_FLOAT, 0, particle_color_array + smoke_sz*4); // draw fire
		glVertexPointer(3, GL_FLOAT, 0, particle_vertex_array + smoke_sz*3);

		glPointSize(max_sz*0.5f);
		glBindTexture(GL_TEXTURE_2D, textures[T_FIRE].TextureID);
		glDrawArrays(GL_POINTS, 0, fire_sz);
	}

	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_COLOR_ARRAY);

	glTexEnvf(GL_POINT_SPRITE_ARB, GL_COORD_REPLACE_ARB, GL_FALSE);
	glDisable(GL_POINT_SPRITE_ARB);

	glDepthMask(GL_TRUE); // turn on depth buffer

	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);

	glPopAttrib();
}

void DrawScreen(int x1, int x2, int y1, int y2, float transp)
{	
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glColor4f(0.05f, 0.1f, 0.15f, transp); // draw transparent screen
	glBegin(GL_QUADS);
		glVertex2i(x1, y1);
		glVertex2i(x1, y2);
		glVertex2i(x2, y2);
		glVertex2i(x2, y1);
	glEnd();

	glDisable(GL_BLEND);

	glColor3f(1.0f, 1.0f, 1.0f);
	glLineWidth(2.0f);
	glBegin(GL_LINE_LOOP);
		glVertex2i(x1, y1);
		glVertex2i(x1, y2);
		glVertex2i(x2, y2);
		glVertex2i(x2, y1);
	glEnd();
	glLineWidth(1.0f);
}

void DrawMinimap(GLubyte* mini)
{
	if(mini)
	{
		const int mx = (hex_num_x > hex_num_y) ? hex_num_x : hex_num_y;
		const float scale = (mx > 200) ? 1.0f : float(200/mx);		

		if(conf3d.show_minimap > 1)
		{
			DrawScreen(0, scale*hex_num_x + 8, 0, scale*hex_num_y + 8, 0.6f);
		}

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glPixelZoom(scale, -scale);
		glRasterPos2i(4, scale*hex_num_y + 4);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

		glDrawPixels(hex_num_x, hex_num_y, GL_RGBA, GL_UNSIGNED_BYTE, mini);

		glPixelZoom(1, 1);
		glDisable(GL_BLEND);
	}
}

void DrawString(int x, int y, const string& s, void* font = GLUT_BITMAP_9_BY_15)
{	
	glRasterPos2i(x, y);
	for(unsigned int i = 0;i < s.size();i++)
	{
		glutBitmapCharacter(font, s[i]);		
	}
}

int GetStringLength(const string& s, void* font = GLUT_BITMAP_9_BY_15)
{
	return glutBitmapLength(font, (const unsigned char*)s.c_str());
}

void DrawScore(const vector<RobotData>& rs, const vector<BaseData>& bs)
{
	map<int, int> r_num;
	map<int, int> b_num;

	const unsigned int teams = bot_names.size();

	for(vector<RobotData>::const_iterator it = rs.begin();it != rs.end();it++)
	{
		if((*it).hitpoints != 0) 
		{
			r_num[(*it).team]++;		
		}
	}
	for(vector<BaseData>::const_iterator it = bs.begin();it != bs.end();it++)
	{
		if((*it).team != -1)
		{
			b_num[(*it).team]++;			
		}
	}

	if(conf3d.show_score > 1)
	{
		DrawScreen(window_width - 350, window_width, window_height - 30*teams - 30, window_height, 0.6f);		
	}

	static char str[100];
	for(unsigned int i = 0;i < teams;i++) // for each team
	{	
		if((r_num[i] == 0) && (b_num[i] == 0))
		{
			sprintf(str, "%+8s: Terminated", bot_names[i].c_str());
		}
		else
		{
			sprintf(str, "%+8s: Robots: %4d  Bases: %4d", bot_names[i].c_str(), r_num[i], b_num[i]);
		}

		glColor3f(colors[i][0], colors[i][1], colors[i][2]);

		DrawString(window_width - 340, window_height - i*30 - 32, string(str));
	}
}

void DrawInfo() // draw some app info
{	
	vector<string> info;
	static char str[100];	

	sprintf(str, "Iteration: %d", curr_iteration);
		info.push_back(string(str));

	//(conf3d.enable_sound) ? info.push_back("Sound: on") : info.push_back("Sound: off");
	(conf3d.enable_music) ? info.push_back("Music: on") : info.push_back("Music: off");
	(conf3d.show_particles) ? info.push_back("Particles: on") : info.push_back("Particles: off");
				
	sprintf(str, "Frames per animaton: %d", conf3d.frames_per_anim);
		info.push_back(string(str));
	sprintf(str, "Refresh delay: %d", conf3d.refresh_delay);
		info.push_back(string(str));

	info.push_back(""); // separator
	info.push_back("RoboWar3D v1.0 beta2");
	info.push_back("Press F1 for help");
	
	glColor3f(0.85f, 0.85f, 0.85f);

	unsigned int max = 0;
	for(vector<string>::const_iterator it = info.begin();it != info.end();it++) // find max length of str
	{
		if((*it).size() > max) max = (*it).size();
	}

	int i = 0;
	for(vector<string>::reverse_iterator it = info.rbegin();it != info.rend();it++, i++)
	{
		DrawString(window_width - (max + 2)*5, 10 + i*14, (*it), GLUT_BITMAP_HELVETICA_10);
	}
}

void DrawHelp() // show help
{
	static vector<string> help;
	static vector<int> length;
	static bool inited = false;

	if(!inited)
	{
		help.push_back("F1 - turn help screen on/off");
		//help.push_back("s - turn sound on/off");
		help.push_back("d - turn music on/off");
		help.push_back("q - decrease frames per animation");
		help.push_back("w - increase frames per animation");
		help.push_back("e - decrease refresh delay");
		help.push_back("r - increase refresh delay");
		help.push_back("p - turn particles effects on/off");
		help.push_back("m - switch minimap mode");
		help.push_back("t - switch scores mode");
		help.push_back("i - switch info on/off");
		help.push_back("Left mouse button - move/rotate camera left/right");
		help.push_back("Right mouse button - move/rotate camera up/down");
		help.push_back("Esc - exit");		

		for(int i = help.size() - 1;i >= 0;i--) // save length of strings
		{
			length.push_back(GetStringLength(help[i], GLUT_BITMAP_HELVETICA_18));
		}
		inited = true;
	}

	const int help_w = int(window_width*0.4f);
	const int help_h = int(window_height*0.6f);

	const int help_x = window_width/2 - help_w/2;
	const int help_y = window_height/2 - help_h/2;

	DrawScreen(help_x, help_x + help_w, help_y, help_y + help_h, 0.6f);
	
	int i = 0;	
	for(vector<string>::reverse_iterator it = help.rbegin();it != help.rend();it++, i++)
	{		
		DrawString(help_x + help_w/2 - length[i]/2, help_y + (help_h/2 - (help.size()*18)) + i*36, (*it), GLUT_BITMAP_HELVETICA_18);
	}
}

void DrawHUD() // draw 2D interface
{
	glPushAttrib(GL_COLOR_BUFFER_BIT | GL_CURRENT_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix(); // push matrix for 3d

	glLoadIdentity();
	gluOrtho2D(0, window_width, 0, window_height); // set 2D 

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();	
	glDisable(GL_DEPTH_TEST);
	
	glPointSize(1.0f);
	glBegin(GL_POINTS); // to fix bug with text display after particles draw			
		glVertex2i(-1, -1);
	glEnd();

	if(conf3d.show_minimap > 0)	
		DrawMinimap(minimap);
	if(conf3d.show_score > 0) 
		DrawScore(robots, bases);
	if(conf3d.show_info)
		DrawInfo();
	if(conf3d.show_help)
		DrawHelp();

	glEnable(GL_DEPTH_TEST);
	glMatrixMode(GL_MODELVIEW); // return old modelview
	glPopMatrix(); 

	glMatrixMode(GL_PROJECTION); // return 3d
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);

	glPopAttrib();
}

void DrawLoadingScreen()
{
	glMatrixMode(GL_PROJECTION);
	glPushMatrix(); // push matrix for 3d

	glLoadIdentity();
	gluOrtho2D(0, window_width, 0, window_height); // set 2D 

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glDisable(GL_DEPTH_TEST);

	static const string str = "Loading...";
	static int len = GetStringLength(str, GLUT_BITMAP_HELVETICA_18);

	glColor3f(1.0f, 1.0f, 1.0f);
	DrawString(window_width/2 - len/2, window_height/2, str, GLUT_BITMAP_HELVETICA_18);

	glEnable(GL_DEPTH_TEST);

	glMatrixMode(GL_MODELVIEW); // return old modelview
	glPopMatrix(); 
	glMatrixMode(GL_PROJECTION); // return 3d
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
}

void Draw(void) // total scene draw
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	bool ok = false;

	pthread_mutex_lock(&data_mutex);

		if(update_view)
		{
			//InitGLView();
			//InitGLLight();
			update_view = false;
		}

		if(update_list)
		{
			ClearGLDisplayLists();
			InitGLDisplayLists();
			if(gl_vbo_support)
			{
				UploadGLVertexBuffers(); // load static data to vbo
				ClearGLArrays(); // clear temporary arrays; don't clear when not using VBO or display lists			
			}
			update_list = false;
			scene_ready = true; // ready to draw
		}
		ok = scene_ready;

	pthread_mutex_unlock(&data_mutex);

	InitGLView();
	InitGLLight();

	glDisable(GL_LIGHTING);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_COLOR_MATERIAL);

	if(ok) // ready to draw scene
	{
		glEnable(GL_MULTISAMPLE);
	
			glCallList(display_lists[DL_SKY_BOX]); // draw sky box	
			glEnable(GL_CULL_FACE);
			//glCallList(display_lists[DL_TERRAIN]); // draw terrain		
			DrawTerrain();

			float l = float(lava_move_step)/500.0f; // lava move step
	
			glMatrixMode(GL_TEXTURE); // draw lava
				glPushMatrix();
				glLoadIdentity();
				glTranslatef(l, l, 0);		
					glCallList(display_lists[DL_LAVA]);
				glPopMatrix();
			glMatrixMode(GL_MODELVIEW);

			glLightModeli(GL_LIGHT_MODEL_COLOR_CONTROL, GL_SEPARATE_SPECULAR_COLOR);			
				//glCallList(display_lists[DL_GAME_FIELD]); // draw game field		
				DrawGameField();
			glDisable(GL_CULL_FACE);
			glLightModeli(GL_LIGHT_MODEL_COLOR_CONTROL, GL_SINGLE_COLOR);

		//glEnable(GL_COLOR_MATERIAL);
		//glDisable(GL_TEXTURE_2D);	

		pthread_mutex_lock(&data_mutex);

			DrawRobots(robots);
			DrawBases(bases);

		pthread_mutex_lock(&anim_mutex);

			DrawBullets();
			DrawHits();

	glDisable(GL_MULTISAMPLE);

		glEnable(GL_LINE_SMOOTH);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);		

			DrawRobotsInfo(robots);
			DrawBasesInfo(bases);

		glDisable(GL_LINE_SMOOTH);
		glDisable(GL_BLEND);

			if(conf3d.show_particles) 
				DrawParticles();

		pthread_mutex_unlock(&anim_mutex);

		//glDisable(GL_TEXTURE_2D);
		//glDisable(GL_LIGHTING);
		//glDisable(GL_COLOR_MATERIAL);
	
			DrawHUD();

		pthread_mutex_unlock(&data_mutex);
	}
	else
	{
		DrawLoadingScreen(); // draw loading screen for a while
	}
	glutSwapBuffers();  
}

void Mouse(int button, int state, int x, int y)
{
	if(button == GLUT_LEFT_BUTTON)
	{
		mouse_left = (state == GLUT_DOWN);
	}
	if(button == GLUT_RIGHT_BUTTON)
	{
		mouse_right = (state == GLUT_DOWN);
	}

	mouse_pos_x = x;
	mouse_pos_y = y;
}

void MouseMove(int x, int y)
{
	bool change = false;
	if(mouse_left)
	{
		eye.x -= look.x*float(mouse_pos_y - y)/100.0;
		eye.y -= look.y*float(mouse_pos_y - y)/100.0;
		
		float c = cos(float(mouse_pos_x - x)/100.0);
		float s = sin(float(mouse_pos_x - x)/100.0);

		float nx = look.x*c - look.y*s;
		float ny = look.x*s + look.y*c;

		look.x = nx;
		look.y = ny;

		change = true;
	}
	if(mouse_right)
	{
		eye.z += float(world.max_z/window_height)*(mouse_pos_y - y)/2.0f;
		look.z += float(world.max_z/window_height)*(mouse_pos_y - y)/2.0f + float(world.max_z/window_width)*(mouse_pos_x - x)/2.0f;

		change = true;
	}

	if(eye.x > world.max_x - 0.1) eye.x = world.max_x - 0.1;
	if(eye.x < world.min_x + 0.1) eye.x = world.min_x + 0.1;
	if(eye.y > world.max_y - 0.1) eye.y = world.max_y - 0.1;
	if(eye.y < world.min_y + 0.1) eye.y = world.min_y + 0.1;
	if(eye.z > world.max_z) eye.z = world.max_z;
	if(eye.z < 0.05) eye.z = 0.05;

	mouse_pos_x = x;
	mouse_pos_y = y;

	if(change)
	{		
		update_view = true;
		glutPostRedisplay();
	}
}

void Keyboard(unsigned char key, int x, int y)
{
	switch(key)
	{
		/*case 's':		
		{
			conf3d.enable_sound = !conf3d.enable_sound;
			sound.Enable(conf3d.enable_sound);
			if(!sound.IsEnabled())
			{
				sound.Clear(true);
			}						
			break;
		}*/
		case 'd':		
		{
			conf3d.enable_music = !conf3d.enable_music;
			music.Enable(conf3d.enable_music);
			if(!music.IsEnabled())
			{
				music.Clear(true);
			}		
			else
			{
				music.Play("theme.mp3");
			}
			break;
		}
		case 'q':
		{
			if(conf3d.frames_per_anim > 2) conf3d.frames_per_anim--;
			break;
		}
		case 'w':
		{
			conf3d.frames_per_anim++;
			break;
		}
		case 'e':
		{
			if(conf3d.refresh_delay > 5) conf3d.refresh_delay -= 5;
			break;
		}
		case 'r':
		{
			conf3d.refresh_delay += 5;
			break;
		}
		case 'm':
		{
			conf3d.show_minimap = (conf3d.show_minimap + 1) % 3;
			glutPostRedisplay();
			break;
		}
		case 't':
		{
			conf3d.show_score = (conf3d.show_score + 1) % 3;
			glutPostRedisplay();
			break;
		}
		case 'i':
		{
			conf3d.show_info = !conf3d.show_info;
			glutPostRedisplay();
			break;
		}
		case 'p':
		{
			if(gl_point_sprite_support) conf3d.show_particles = !conf3d.show_particles;			
			break;
		}
		case 27: // Esc
		{
			cout << "Exiting..." << endl;
			//glutLeaveGameMode();
			exit(0);
			break;
		}
	}
}

void KeyboardSpecial(int key, int x, int y)
{
	switch(key)
	{
		case GLUT_KEY_F1: // F1 for help
		{
			conf3d.show_help = !conf3d.show_help;
			glutPostRedisplay();
			break;
		}
	}
}

void InitRobotCoords(const vector<RobotData>& rs)
{
	robot_coords.reserve(rs.size());
	robot_coords.resize(rs.size());

	for(unsigned int i = 0;i < rs.size();i++)
	{
		robot_coords[i].Set(field_points[rs[i].y*hex_num_x + rs[i].x]);
	}
}

bool DetectHit(const RobotData& r, const vector<RobotData>& rs, const vector<BaseData>& bs, bool& is_robot, int& index) // detect if robot hit something
{
	if(!r.shoot) return false;

	int i = 0;
	for(vector<RobotData>::const_iterator it = rs.begin();it != rs.end();it++, i++)
	{
		if(((*it).x == r.shoot_x) && ((*it).y == r.shoot_y))
		{
			is_robot = true;
			index = i;
			return true;
		}
	}
	i = 0;
	for(vector<BaseData>::const_iterator it = bs.begin();it != bs.end();it++, i++)
	{
		if(((*it).x == r.shoot_x) && ((*it).y == r.shoot_y))
		{
			is_robot = false;
			index = i;
			return true;
		}
	}
return false;
}

void InitAnimation(const vector<RobotData>& rs, const vector<BaseData>& bs)
{
	const int fpa = conf3d.frames_per_anim;

	for(unsigned int i = 0;i < rs.size();i++)
	{
		if(rs[i].hitpoints == 0) // dead robot
		{
			Point3D& p = robot_coords[i];
				HitAnimation* hit = new HitAnimation(i, rs[i].team, 0, fpa);
				hit->Generate(Point3D(p.x, p.y, hex_radius/2.0), 50, hex_radius, 4*hex_radius);
				hit->death = true;
			hits.push_back(hit); // dead animation as hit ahimation			
		}
		if(rs[i].shoot)
		{
			Point3D& beg_point = robot_coords[i];
			Point3D& end_point = field_points[rs[i].shoot_y*hex_num_x + rs[i].shoot_x];			

			const Vector3D move((end_point.x - beg_point.x)/float(fpa/2), (end_point.y - beg_point.y)/(fpa/2), 0);

			bullets.push_back(new BulletAnimation(beg_point, move, rs[i].team, 0, fpa/2));

			bool is_robot = false;
			int index;
			if(DetectHit(rs[i], rs, bs, is_robot, index))
			{
				if(is_robot) // hit robot
				{
					Point3D& p = robot_coords[index];
						HitAnimation* hit = new HitAnimation(index, rs[index].team, fpa/2, fpa);
						hit->Generate(Point3D(p.x, p.y, hex_radius/2.0), 10, hex_radius/4.0, 2*hex_radius); 						
					hits.push_back(hit);					
				}
				else // hit base
				{
					Point3D& p = field_points[bs[index].y*hex_num_x + bs[index].x];
						HitAnimation* hit = new HitAnimation(index, bs[index].team, fpa/2, fpa);
						hit->Generate(Point3D(p.x, p.y, hex_radius), 30, hex_radius/2.0, 4*hex_radius); 
					hits.push_back(hit);					
				}
			}
		}	
		if(rs[i].move_direction > 0) // 0: stay, 1: E, 2: SE, 3: SW, 4: W, 5: NW, 6: NE
		{
			int mx, my;
			if(!shift(rs[i].x, rs[i].y, rs[i].move_direction, mx, my)) continue;
			
			Point3D& beg_point = robot_coords[i];
			Point3D& end_point = field_points[my*hex_num_x + mx];			

			const Vector3D move((end_point.x - beg_point.x)/float(fpa), (end_point.y - beg_point.y)/(fpa), 0);

			moves.push_back(new MoveAnimation(i, rs[i].team, beg_point, move, 0, fpa));
		}
	}
	/*for(vector<BaseData>::const_iterator it = bs.begin();it != bs.end();it++)
	{
		if((*it).team != -1) // active base
		{
			if((*it).cooldown == 0) // make new robot
			{
				sound.Play(sound.GetSoundFile("ready", team_prefix[(*it).team % 2], team_sound_data[(*it).team % 2][0]));
			}			
		}
	}*/
}

void InitParticleAnimation(const vector<BaseData>& bs)
{
	const int fpa = conf3d.frames_per_anim;
	const float anim_c = 1.0f/(fpa*5);
	for(vector<BaseData>::const_iterator it = bs.begin();it != bs.end();it++)
	{
		if((*it).team != -1) // active base
		{			
			const Point3D& p = field_points[(*it).y*hex_num_x + (*it).x];
			Point3D bp(p.x, p.y, p.z + hex_radius);
			Point3D ep;
			Vector3D move;
			const int s_num = 5 + rand() % 6; // smoke clouds per base
			const float c1 = hex_radius*0.15f;
			const float c2 = hex_radius*0.1f;		
			for(int i = 0;i < s_num;i++) // make several smokes
			{								
				ep.Set(
					bp.x + float((rand() % 20) - 10)*c1, 
					bp.y + float((rand() % 20) - 10)*c1, 
					bp.z + float((rand() % 80) + 10)*c2);
				move.Set((ep.x - bp.x)*anim_c, (ep.y - bp.y)*anim_c, (ep.z - bp.z)*anim_c);
				smoke.push_back(new ParticleAnimation(bp, move, 0.2f + float(rand() % 80)/100.0f, 0.3f + 0.2f*float(rand() % 100)/100.0f, 1.0f, 0, 5*fpa)); // smoke anim				
			}
		}
	}

	const int f_num = 1 + rand() % 4; // num of new fire sources
	const float quad_sz = (world.max_x - world.min_x)/(200);
	float x, y;
	Point3D bp, ep;
	Vector3D move;
	for(int i = 0;i < f_num;i++) // gen fire
	{		
		x = world.min_x + (world.max_x - world.min_x)*float(rand() % 1000)*0.001f; // random point in the world
		y = world.min_y + (world.max_y - world.min_y)*float(rand() % 1000)*0.001f;

		bp.Set(x, y, 0.0f);

		const int n = 3 + (rand() % 5); // several fire splashes
		for(int j = 0;j < n;j++)
		{
			ep.Set(x, y, float((rand() % 10) + 1)*quad_sz*0.25f);
			move.Set((ep.x - bp.x)*anim_c, (ep.y - bp.y)*anim_c, (ep.z - bp.z)*anim_c);
			fire.push_back(new ParticleAnimation(bp, move, 0.5 + float(rand() % 100)/200.0f, 0.2f + 0.2f*float(rand() % 100)/100.0f, 1.0f, 0, 5*fpa)); // fire anim			
		}
	}
}

void ProcessAnimation()
{
	/*if(enable_sound)
	{
		for(list<BulletAnimation*>::const_iterator it = bullets.begin();it != bullets.end();it++)
		{
			if((*it)->IsStart()) // make fire sound
			{			
				sound.Play(sound.GetSoundFile("fire", "", sound_data[0]));
			}		
		}	

		int team = -1;
		for(list<MoveAnimation*>::const_iterator it = moves.begin();it != moves.end();it++)
		{
			if((*it)->IsStart()) // make some talk sound
			{			
				if((rand() % (2*moves.size() + 10)*200) == 0) team = (*it)->team;
			}				
		}	
		if(team != -1)
		{
			if(rand() % bases.size() == 0) sound.Play(sound.GetSoundFile("talk", team_prefix[team % 2], team_sound_data[team % 2][1]));
		}

		for(list<HitAnimation*>::const_iterator it = hits.begin();it != hits.end();it++)
		{
			if((*it)->IsStart())
			{
				if(!(*it)->death) // play hit sound
				{
					sound.Play(sound.GetSoundFile("hit", "", sound_data[1]));
				}
				else // play death sound
				{
					sound.Play(sound.GetSoundFile("death", team_prefix[(*it)->team % 2], team_sound_data[(*it)->team % 2][2]));
				}
			}		
		}
	}*/

	ProcessBulletAnimationList(bullets);
	ProcessMoveAnimationList(moves);
	ProcessHitAnimationList(hits);
	ProcessParticleAnimationList(smoke);
	ProcessParticleAnimationList(fire);

	for(list<MoveAnimation*>::const_iterator it = moves.begin();it != moves.end();it++)
	{
		robot_coords[(*it)->index] = (*it)->curr_pos;		
	}	
}

void Timer(int t)
{
	pthread_mutex_lock(&anim_mutex);
	if(bullets.empty() && moves.empty() && hits.empty())
	{
		free_to_next = true;
		pthread_cond_signal(&anim_cond);
	}
	//else
	{		
		ProcessAnimation();	
		if(conf3d.show_particles) 
		{
			InitParticleAnimation(bases);		
			CountParticlesData(); // load processed particle animation to arrays
		}
	}
	pthread_mutex_unlock(&anim_mutex);

	bases_rot_angle = (bases_rot_angle + 1) % 360;
	lava_move_step = (lava_move_step + 1) % 500;

	glutPostRedisplay();

	glutTimerFunc(conf3d.refresh_delay, Timer, 0);
}

void Idle()
{
	glutPostRedisplay();
}

void OnExit()
{
	sound.Clear(true);
	music.Clear(true);

	if(minimap) delete[] minimap;

	if(particle_data_array) delete[] particle_data_array; // delete arrays	

	ClearGLArrays();

	ClearGLTextures();
	ClearGLDisplayLists();
	if(gl_vbo_support) ClearGLVertexBuffers();

	//pthread_join(emulator_thread, NULL);

	pthread_mutex_destroy(&data_mutex);
	pthread_mutex_destroy(&anim_mutex);
	pthread_cond_destroy(&anim_cond);

	glutLeaveGameMode();
}

void update(const vector<RobotData>& rs, const vector<BaseData>& bs, const MinimapData& minimap_data, unsigned int iteration)
{
	pthread_mutex_lock(&anim_mutex);
	if(sound.IsEnabled())
		sound.Clear(false); // clear some old sounds
	if(!free_to_next)
		pthread_cond_wait(&anim_cond, &anim_mutex);
	pthread_mutex_unlock(&anim_mutex);

	pthread_mutex_lock(&data_mutex);

	robots = rs;
	bases = bs;
  
	if(iteration == 0) // game begins
	{
		sound.Clear(true); // delete all old sounds
		music.Clear(true);

		ClearGLArrays(); // delete all prev created arrays (if not deleted early)

		srand(time(NULL)); // init rnd

		InitScene(minimap_data.width(), minimap_data.height());	

		if(minimap) delete[] minimap;
		minimap = new GLubyte[hex_num_x*hex_num_y*4];
		
		music.Play("theme.mp3"); // play music

		InitLifeVertexArray(ROBOT_HP, BASE_HP, hex_radius/1.5f, hex_radius*1.5f);
		InitCooldownVertexArray(RELOAD_TIME - 1, ROBOT_CONSTRUCTION_TIME - 1, hex_radius/1.6f, hex_radius*1.4f);

		GenerateTerrain(); // generate terrain
		GenerateGameField(hex_radius*0.85f, hex_radius*0.3f); // generate hex field

		update_view = true;
		update_list = true; // command to update lists
		scene_ready = false; // wait when scene is ready
	}

	curr_iteration = iteration + 1; //conv to emulator iterations

	InitMinimap(minimap, minimap_data, robots, bases);

	//pthread_mutex_lock(&anim_mutex);
		InitRobotCoords(robots);
		InitAnimation(robots, bases);
		free_to_next = false;		
	pthread_mutex_unlock(&anim_mutex);

	pthread_mutex_unlock(&data_mutex); 

	glutPostRedisplay();

	//Sleep(iteration_delay);
}

void SetDefaultConfig(Config3D& c)
{
	c.show_particles = 1;
	c.show_minimap = 2;
	c.show_score = 1;
	c.show_info = true;
	c.show_help = false;
	c.frames_per_anim = 2;
	c.refresh_delay = 1;
	c.enable_music = false;
	c.enable_sound = false;
}

int LoadConfig(const string& file, Config3D& c) // load config for 3D
{
	fstream f_in(file.c_str());

	if(!f_in.is_open())
	{
		cerr << "Error: failed to open config " << file << endl;
		return -1;
	}

	while(!f_in.eof())
	{
		string tag, val;
		f_in >> tag >> val;

		if(tag == "") continue;
		if(tag[0] == '%') {char tmp[250]; f_in.getline(tmp, 250); continue;} // skip comment

		int value = atoi(val.c_str());		

		if(tag == "show_particles") c.show_particles = (value != 0);
		else if(tag == "show_minimap") c.show_minimap = value % 3;
		else if(tag == "show_score") c.show_score = value % 3;
		else if(tag == "show_info") c.show_info = (value != 0);
		else if(tag == "frames_per_anim") c.frames_per_anim = value;
		else if(tag == "refresh_delay") c.refresh_delay = value;
		else if(tag == "enable_music") c.enable_music = (value != 0);
		else if(tag == "enable_sound") c.enable_sound = (value != 0);
		else
		{
			cerr << "Error: Config3D: invalid or unknown tag: " << tag << endl;
		}					
	}

	f_in.close();
return 0;
}

int main(int argc, char *argv[])
{	
	pthread_attr_t emulator_thread_attr;
	pthread_attr_init(&emulator_thread_attr);
	pthread_attr_setdetachstate(&emulator_thread_attr, PTHREAD_CREATE_DETACHED);

	pthread_mutex_init(&data_mutex, NULL);
	pthread_mutex_init(&anim_mutex, NULL);
	pthread_cond_init(&anim_cond, NULL);

	glutInit(&argc, argv);
	//glutInitWindowSize(800, 800);
	//glutInitWindowPosition(100, 100);

	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH | GLUT_MULTISAMPLE);		

	glutGameModeString("1920x1200:32@75"); //screen settings 
	glutEnterGameMode(); // set full screen

	InitGLExt(); // init GL extensions

	SetDefaultConfig(conf3d); // set config
	LoadConfig("config3D.txt", conf3d); // read config

	//gl_point_sprite_support = false;
	if(!gl_point_sprite_support) conf3d.show_particles = false; // turn off particles
    
	glutReshapeFunc(Reshape); // set callbacks
	glutDisplayFunc(Draw);
	glutMouseFunc(Mouse);
	glutMotionFunc(MouseMove);
	glutPassiveMotionFunc(MouseMove);
	glutKeyboardFunc(Keyboard);
	glutSpecialFunc(KeyboardSpecial);	
	
	srand((unsigned)time(NULL)); // init rand

	mymath::Init(); // init math library

	InitMath(); // precalc some values

	InitGL(); // set some GL params	

	if(gl_vbo_support) InitGLVertexBuffers(); // init VBO buffers if supported	

	InitGLLight(); // set light
	InitGLMat(); // set materials props
	InitGLTextures(); // load or gen textures	

	glClearColor(0, 0, 0, 0);

	sound.SetPrefix("Sounds/");
	music.SetPrefix("Music/");

	sound.Enable(conf3d.enable_sound);
	music.Enable(conf3d.enable_music);	

	update_list = false; // don't update lists without need	
	
	glutTimerFunc(conf3d.refresh_delay, Timer, 0); // start timer (animation, etc.)

	pthread_create(&emulator_thread, &emulator_thread_attr, emuThreadFunc, NULL); // run emulator and wait for update

	atexit(OnExit); // reg exit callback
	
	glutMainLoop(); // run engine

	return 0;
}
