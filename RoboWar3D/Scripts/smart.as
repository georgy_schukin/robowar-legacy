// Smart bot

int get_friends(RobotState& s) // get num of friends in view range
{
	int res = 0;
	uint r_num = s.robots_num();
	for(uint i = 0;i < r_num;i++)
	{
		if(s.robot(i).team == s.team) res++;
	}
return res;
}


int get_enemies(RobotState& s) // get num of enemies in view range
{
	int res = 0;
	uint r_num = s.robots_num();
	for(uint i = 0;i < r_num;i++)
	{
		if(s.robot(i).team != s.team) res++;
	}
return res;
}

void Execute(RobotState& rs, const Environment& e)
{	
	rs.transmit = false;

	uint r_num = rs.robots_num(); // num of robots in view range
	uint b_num = rs.bases_num(); // num of bases in view range
	uint m_num = rs.messages_num(); // num of income messages

	int fr = get_friends(rs); // num of friends
	int en = r_num - fr; // num of enemies
		
	rs.move_direction = rand() % 7; // choose random move direction
	
	for(uint i = 0;i < b_num;i++) // for each base in view
	{
		Unit b = rs.base(i);
		if(b.team != rs.team) // base is not in our team
		{
			rs.shoot = true; // shoot in it
			rs.shoot_x = b.x;
			rs.shoot_y = b.y;
			if(fr > 1) 
				rs.move_direction = e.get_dir(rs.x, rs.y, b.x, b.y); // move to base
			else
				rs.move_direction = 0; // stay in place
			rs.transmit = true; // transmit base coords to others
			rs.set_msg_int(0, b.x); 
			rs.set_msg_int(1, b.y);
			rs.set_msg_int(2, e.iter);
		}
	}

	for(uint i = 0;i < r_num;i++) // for each robot in view
	{
		Unit r = rs.robot(i);
		if(r.team != rs.team) // robot is not in our team
		{
			rs.shoot = true; // shoot in it
			rs.shoot_x = r.x;
			rs.shoot_y = r.y;
			rs.move_direction = 0;
			if(fr > en + 1)
				rs.move_direction = e.get_dir(rs.x, rs.y, r.x, r.y); // move to robot
			else if(en > fr)
				rs.move_direction = e.get_dir(r.x, r.y, rs.x, rs.y); // move from robot
			rs.transmit = true; // transmit robot coords
			rs.set_msg_int(0, r.x);
			rs.set_msg_int(1, r.y);
			rs.set_msg_int(2, e.iter);
		}
	}

	if((rs.shoot == false) && (m_num > 0)) // if is idle and have income messages
	{	
		int tx = rs.get_msgs_int(0, 0); // get target coords from first message
		int ty = rs.get_msgs_int(0, 1);
		int it = rs.get_msgs_int(0, 2);
		rs.move_direction = e.get_dir(rs.x, rs.y, tx, ty); // move to target
		if((!rs.transmit) && (e.iter - it < 5)) // message is rather new
		{
			rs.copy_to_msg(0); // retransmit message to others
			rs.transmit = true;
		}
	}
}
