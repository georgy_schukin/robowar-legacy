// Simple bot

void Execute(RobotState& rs, const Environment& e)
{
	rs.move_direction = rand() % 7; // choose random move direction	

	uint b_num = rs.bases_num(); // num of bases in view range
	uint r_num = rs.robots_num(); // num of robots in view range

	for(uint i = 0;i < b_num;i++) // for each base in view
	{
		Unit b = rs.base(i);
		if(b.team != rs.team) // base is not in our team
		{
			rs.shoot = true; // shoot in it
			rs.shoot_x = b.x;
			rs.shoot_y = b.y;
			rs.move_direction = 0;
		}
	}

	for(uint i = 0;i < r_num;i++) // for each robot in view
	{
		Unit r = rs.robot(i);
		if(r.team != rs.team) // robot is not in our team
		{
			rs.shoot = true; // shoot in it
			rs.shoot_x = r.x;
			rs.shoot_y = r.y;
			rs.move_direction = 0;
		}
	}

}
