#pragma once

#include "Robot.h"
#include "ScriptEngine.h"
#include "IVisualizer.h"

#include <vector>
#include <set>
#include <map>

using namespace robo;
using namespace robo_vis;
using namespace std;

namespace emulator
{

class XUnit : public Unit
{
public:
	bool base;
	unsigned int id;
	int hitpoints;
	int cooldown;
	int damage;
	int attacker;
	bool attacker_dirty;
	RobotState* state;

public:
	XUnit() : state(0), base(false), hitpoints(0), cooldown(0), damage(0), attacker(-1), attacker_dirty(false), id(0) {}
	~XUnit() {if(state) delete state;}
};

//////////////////////////// Emulator ///////////////////////////////
class Emulator
{
protected:
	ScriptEngine script_engine;
	
	set<XUnit*> bases;
	set<XUnit*> robots;
	XUnit** field;
	size_t* move_request;

	vector<string> scripts; // scripts for robots
	vector<string> teams; // teams on the field
	vector<RobotAction> actions;
	map<int, int> death_time;

	VisualizerController* visualizer;

	map<unsigned int, RobotData> robot_data; // data for robot visualization
	map<unsigned int, BaseData> base_data; // data for base visualization

	unsigned int last_robot_id;
	unsigned int last_base_id;

protected:
	map<string, RobotAction> registered_bots; // registered c++ bots

protected:
	Environment environment;

protected:
	void Clear();

	void RegisterBots();

	bool ValidBaseLocation(int x, int y);
	void InitRandomMap(int width, int height, int b_num, int t_num);
	
	int LoadMap(const string& file);

	bool SpawnRobot(int x, int y, int team);
	
	void IterateRobotSpawn();	
	void IterateRobotView();
	void IterateRobotActions();
	void IterateRobotDamage();
	void IterateBaseDamage();
	void IterateRobotMessages();
	void IterateRobotMoves();

	void GetDeadRobots(set<XUnit*>& dead); 
	
	void Iterate();

	bool Finished();

	void GetRobotData(vector<RobotData>& res);
	void GetBaseData(vector<BaseData>& res);
	
	void UpdateRobotData(const set<XUnit*>& dt, int stage); // update vis data for robots
	void UpdateBaseData(const set<XUnit*>& dt, int stage); // update vis data for bases

public:
	Emulator();
	~Emulator();

	void SetEnvironment(const Environment& e) {environment = e;}
	void SetVisualizer(VisualizerController* vis) {visualizer = vis;} // for visualization

	const Environment& Env() const {return environment;}

	int LoadConfig(const string& file, Environment& env); // read env from file
	int LoadGame(const string& file); // load game data from file
	
	int AddRobot(const string& name, const string& script); // add script bot to game
	int AddRobot(const string& name, RobotAction ac); // add c++ bot to game
	int AddMap(const string& file); // set game map

	void GetTeams(vector<string>& t) const {t = teams;}

	int Emulate(); // start emulation
};

}

void *emuThreadFunc(void *arg);
