#pragma once

#include "IVisualizer.h"

namespace robo_vis
{
	class VisualizerOpenGLController: public VisualizerController
	{
		private:
			IVisualizerDrawerController* drawerController;
			unsigned int iteration;
	protected:
			MinimapData localData;
		public:
			VisualizerOpenGLController(int mapWidth, int mapHeight, int teamAmount);

			virtual void visualize(const std::vector<RobotData>& robots, const std::vector<BaseData>& bases);

			virtual ~VisualizerOpenGLController();
	};

	/*class VisualizerOpenGLDrawer: public IVisualizerDrawer
	{
		private:
			VisualizerOpenGLDrawer();
			int mapWidth;
			int mapHeight;
		public:
			VisualizerOpenGLDrawer(int mapWidth, int mapHeight) : mapWidth(mapWidth), mapHeight(mapHeight) {}
			virtual void update(const std::vector<RobotData>& robots, const std::vector<BaseData>& bases, const MinimapData& minimap, unsigned int iteration);
	};*/

	class VisualizerOpenGLDrawerController: public IVisualizerDrawerController
	{
		private:
			IVisualizerDrawer* drawer;
			int mapWidth;
			int mapHeight;			
			VisualizerOpenGLDrawerController() {}
		public:
			VisualizerOpenGLDrawerController(int mapWidth, int mapHeight);
			virtual void sendUpdate(const std::vector<RobotData>& robots, const std::vector<BaseData>& bases, const MinimapData& minimap, unsigned int iteration);

			virtual ~VisualizerOpenGLDrawerController();
	};
}

