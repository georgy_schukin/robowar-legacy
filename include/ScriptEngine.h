#pragma once

#include <string>
#include <map>
#include "Robot.h"
#include "angelscript.h"

using namespace std;
using namespace robo;

//////////////////// Script Engine ////////////////////////////////
class ScriptEngine // class for loading and executing robot scripts
{
protected:
	struct ScriptInfo
	{
		asIScriptModule* module;
		asIScriptContext* context;
		int func_id;

		ScriptInfo() : module(0), context(0), func_id(0) {}
		~ScriptInfo() {}
	};

protected:
	asIScriptEngine* engine;
	map<string, ScriptInfo> scripts;

protected:
	int ReadFile(const string& file, string& res);
	void MessageCallback(const asSMessageInfo* msg); 

public:
	ScriptEngine() : engine(0) {}
	~ScriptEngine() {}

	int Init();
	int Free();

	bool Exists(const string& name);

	int LoadScript(const string& name, const string& path = ""); // load script 
	int CompileScript(const string& name); // compile script
	int ExecScript(const string& name, RobotState* rs, Environment* env); // execute script for robot
	int DeleteScript(const string& name);
};
