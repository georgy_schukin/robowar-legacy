#pragma once

#include <string>
#include <list>
#include <time.h>

using namespace std;

namespace sound_engine
{

struct SoundInfo
{
	//string file;
	string alias;
	time_t start_time;
	time_t time_to_live;

	SoundInfo(const string& al, time_t st, time_t tl) : 
		alias(al), start_time(st), time_to_live(tl) {}
};

class SoundEngine
{
protected:
	string prefix;
	list<SoundInfo> curr_sounds;
	bool is_enabled;

protected:
	string IntToStr(int num);

public:
	SoundEngine() : prefix(""), is_enabled(true) {}
	~SoundEngine() {Clear(true);}

	void Play(const string& file, unsigned int time_to_live = 10);
	void Clear(bool all = false); // close some sounds

	void SetPrefix(const string& p) {prefix = p;}
	void Enable(bool enable = true) {is_enabled = enable;}

	bool IsEnabled() const {return is_enabled;}

	string GetSoundFile(const string& base_str, const string& pref_str, int suffix); // get sound file
};

}
