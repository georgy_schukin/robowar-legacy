#pragma once

#include <vector>
#include "scriptarray.h"

using namespace std;

namespace robo
{
////////////////////////// Data Types for Emulation //////////////////////
class RefCounter // help class to enable work with AS scripts
{
private:
	int ref_cnt;
public:
	RefCounter() : ref_cnt(1) {}
	void AddRef() {ref_cnt++;}
	void Release() {if(--ref_cnt == 0) delete this;}
};

class Unit /*: public RefCounter*/
{
public:
	int x, y;
	int team;
	int dist;

	Unit() : x(0), y(0), team(0), dist(0) {}
	Unit(int xx, int yy, int t, int d) : x(xx), y(yy), team(t), dist(d) {}
	Unit(const Unit& u) : x(u.x), y(u.y), team(u.team), dist(u.dist) {}
	~Unit() {}
};

/*class Buffer : public RefCounter // class for safe buffer
{
protected:
	void* data;
	unsigned int size;

public:
	Buffer() : data(0), size(0) {}
	~Buffer() {if(data) delete[] data;}

	void* Data() {return data;}
	unsigned int Size() {return size;}

	void Init(unsigned int sz); // init buf
	void Copy(void* dt, unsigned int sz); // copy data

	void SetInt(unsigned int pos, int val); // write int val
	int GetInt(unsigned int pos); // read val
};*/

class RobotState : public RefCounter
{
protected:
	unsigned int mem_size, msg_size;
public:
	// INPUT STATE

	// Robot coordinates
	int x, y;
	int team;
	int hitpoints;
	int cooldown; // ticks unable to shoot

	// Visible robots and bases
	vector<Unit> robots;
	vector<Unit> bases;

	vector<void*> messages; // each entity points to a memory block of size MSG_SIZE

	// INPUT-OUTPUT STATE
	void* memory; // pointer to the data of local robot memory (block size is MEMORY_SIZE)

	// output state
	int move_direction; // 0: stay, 1: E, 2: SE, 3: SW, 4: W, 5: NW, 6: NE
	bool shoot;
	int shoot_x, shoot_y; // global coords
	bool transmit;

	void* message; // message points to a memory block of size MSG_SIZE

	RobotState();
	RobotState(unsigned int mem_sz, unsigned int msg_sz);
	~RobotState();

	void AddMessage(const void* src/*, unsigned int msg_sz*/); // ass message to messages
	void CopyToMessage(unsigned int msg/*, unsigned int msg_sz*/); // copy from messages to msg

	// functions for scripts
	unsigned int GetMessagesNum() const {return messages.size();} 
	unsigned int GetRobotsNum() const {return robots.size();} 
	unsigned int GetBasesNum() const {return bases.size();}

	void SetMemoryInt(unsigned int pos, int val);
	void SetMessageInt(unsigned int pos, int val);
	void SetMessagesInt(unsigned int msg, unsigned int pos, int val);

	int GetMemoryInt(unsigned int pos) const;
	int GetMessageInt(unsigned int pos) const;
	int GetMessagesInt(unsigned int msg, unsigned int pos) const;

	//const Unit* GetRobot(unsigned int num) const {return &(robots[num]);}
	//const Unit* GetBase(unsigned int num) const {return &(bases[num]);}
	Unit GetRobot(unsigned int num) const {return robots[num];}
	Unit GetBase(unsigned int num) const {return bases[num];}
};

////////////////////// Environment //////////////////////////////
class Environment : public RefCounter // data about field, consts, etc,
{
public:
	int bases_count;
	int field_width; // cells
	int field_height; // cells
	int msg_size; //bytes
	int memory_size; // bytes
	int shoot_range; // cells
	int robot_hp; // shoots
	int base_hp; // shoots
	int reload_time; // ticks
	int view_range; // cells
	int transmit_range; // cells
	int robot_construction_time; // ticks
	int last_iter;
	int iter;
	int timeout;

protected:
	float FDistance(int x1, int y1, int x2, int y2) const;

public:
	Environment() : iter(0), last_iter(0) {}
	~Environment() {}

	bool IsOnField(int x, int y) const; // check that coord belongs to field
	bool Shift(int x, int y, int dir, int& dx, int& dy) const; // count new pos
	int GetDirection(int x1, int y1, int x2, int y2) const;	// get dir from (x1,y1) to (x2,y2)
	int Distance(int x1, int y1, int x2, int y2) const; // get distance between (x1,y1) and (x2,y2)
	bool IsInRange(int x1, int y1, int x2, int y2, int range); // simple check for range

	/*int BasesCount() const {return bases_count;} // functions for scripts
	int FieldWidth() const {return field_width;} // cells
	int FieldHeight() const {return field_height;} // cells
	int MsgSize() const {return msg_size;} //bytes
	int MemorySize() const {return memory_size;} // bytes
	int ShootRange() const {return shoot_range;} // cells
	int RobotHp() const {return robot_hp;} // shoots
	int BaseHp() const {return base_hp;} // shoots
	int ReloadTime() const {return reload_time;} // ticks
	int ViewRange() const {return view_range;} // cells
	int TransmitRange() const {return transmit_range;} // cells
	int RobotConstructionTime() const {return robot_construction_time;} // ticks
	int LastIter() const {return last_iter;}
	int Iter() const {return iter;}
	int Timeout() const {return timeout;}*/
};

typedef void(*RobotAction)(RobotState&);

}

/////////////////// Model parameters for compatibility with old emu ///////////////////////
extern int BASES_COUNT;
extern int FIELD_WIDTH; // cells
extern int FIELD_HEIGHT; // cells
extern int MSG_SIZE; //bytes
extern int MEMORY_SIZE; // bytes
extern int SHOOT_RANGE; // cells
extern int ROBOT_HP; // shoots
extern int BASE_HP; // shoots
extern int RELOAD_TIME; // ticks
extern int VIEW_RANGE; // cells
extern int TRANSMIT_RANGE; // cells
extern int ROBOT_CONSTRUCTION_TIME; // ticks
extern int LAST_ITER;
extern int ITER;
extern int TIMEOUT;

///////////////// Functions for compatibility with old emu //////////////////////////
bool shift(int x, int y, int dir, int& dx, int& dy); 
int get_dir(int x1, int y1, int x2, int y2);
int dist(int x1, int y1, int x2, int y2);

#ifdef _WIN32
#include <Windows.h>
#define usleep(x) Sleep(x/1000)
#define random() rand()
#endif

using namespace robo;