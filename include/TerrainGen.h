#pragma once

#include "Types.h"
#include <vector>

using namespace std;

////////////////////// Terrain Generator ////////////////////////
class TerrainGenerator // class to generate terrain for OpenGL
{
public:
	vector<float> heights; // use only heights for terrain grid

protected:
	int size_x; // num of points by x
	int size_y; // num of points by y
	float step_x; // step by x
	float step_y; // step by y
	Point2D begin; // top left point

public:
	TerrainGenerator() {}
	~TerrainGenerator() {}

	void Init(int sz_x, int sz_y, float s_x, float s_y, const Point2D& beg); // init

	void GenFault(int iter_num, float max_height); // gen with Fault algorithm
	void GenFaultCircle(int iter_num, float max_height, float min_rad, float max_rad); // gen circle terrain with Fault algorithm
	void Normalize(); // lower to min height
	void Inverse(); // conv negative height to positive
	void Move(float h); // conv negative height to positive
	void Smooth(float coeff);

	int SizeX() const {return size_x;}
	int SizeY() const {return size_y;}
	float StepX() const {return step_x;}
	float StepY() const {return step_y;}
	const Point2D& Origin() const {return begin;}
};