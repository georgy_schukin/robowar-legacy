#pragma once

#include "Types.h"
#include "MyMath.h"

#include <list>

using namespace mymath;
using namespace std;

namespace animation
{

	/////////////////////////// Animation ///////////////////////////
class Animation
{
public:
	int start_frame;
	int end_frame;
	int curr_frame;

public:
	Animation() : end_frame(0), curr_frame(0) {}
	Animation(int sf, int ef) : start_frame(sf), end_frame(ef), curr_frame(0) {}

	inline bool IsStart() const {return (curr_frame == start_frame);}
	inline bool IsActive() const {return ((curr_frame >= start_frame) && (curr_frame < end_frame));}
	inline bool IsEnded() const {return (curr_frame >= end_frame);}
};

class BulletAnimation : public Animation
{
public:
	//Point3D end_pos;
	Point3D curr_pos;
	Vector3D move;
	float angle;
	int team;

public:
	BulletAnimation() {}
	BulletAnimation(const Point3D& cp,/*, const Point3D& ep,*/ const Vector3D& mv, int t, int sf, int ef) : 
		curr_pos(cp), team(t), move(mv), Animation(sf, ef) 
	{		
		//Vector3D v(ep.x - cp.x, ep.y - cp.y, ep.z - cp.z);
		float l = sqrt(mv.x*mv.x + mv.y*mv.y);
		angle = acos(float(mv.x/l))*(180.0f/PI); // angle with X
		if(mv.y > 0) angle = -angle;
	}

	//void Process();	
};

class MoveAnimation : public Animation
{
public:
	int index;
	int team;
	Point3D curr_pos;
	Vector3D move;	

public:
	MoveAnimation() {}
	MoveAnimation(int ind, int t, const Point3D& cp, const Vector3D& mv, int sf, int ef) : 
		index(ind), team(t), curr_pos(cp), move(mv), Animation(sf, ef) {}

	//void Process();	
};

class HitAnimation : public Animation
{
public:
	int index;
	int team;
	bool death;
	vector<Point3D> particles;
	vector<Point3D> p_moves;
	vector<float> p_rads;

public:
	HitAnimation() {}
	HitAnimation(int i, int t, int sf, int ef) : 
		index(i), team(t), Animation(sf, ef), death(false) {}

	void Generate(const Point3D& pos, int p_num, float max_rad, float max_len);
	//void Process();	
};

class ParticleAnimation : public Animation // smoke/fire animation
{
public:	
	Point3D curr_pos;
	Vector3D move;
	float color;
	float size;
	float transparency;
	float size_dec, trnp_dec;


public:
	ParticleAnimation() {}
	ParticleAnimation(const Point3D& cp, const Point3D& mv, float clr, float sz, float trnp, int sf, int ef) : 
		curr_pos(cp), move(mv), color(clr), size(sz), transparency(trnp), Animation(sf, ef) 
		{
			size_dec = 0.5f/(end_frame - start_frame);
			trnp_dec = -0.9f/(end_frame - start_frame);
		}

	//void Process();	
};

void ProcessBulletAnimationList(list<BulletAnimation*>&);
void ProcessMoveAnimationList(list<MoveAnimation*>&);
void ProcessHitAnimationList(list<HitAnimation*>&);
void ProcessParticleAnimationList(list<ParticleAnimation*>&);

template<class T> void ClearAnimationList(typename list<T*>& anim_list) // delete old animation from list
{
	for(typename std::list<T*>::iterator it = anim_list.begin();it != anim_list.end();)
	{		
		if((*it)->curr_frame >= (*it)->end_frame) // delete animation
		{
			typename std::list<T*>::iterator it2 = it++;
			delete (*it2);			
			anim_list.erase(it2);
		}
		else it++;
	}
}

}