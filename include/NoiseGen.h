#pragma once

#include <vector>

using namespace std;

/////////////////////////// Noise Generator ////////////////////////////////
class NoiseGenerator
{
public:
	vector<float> noise;

protected:
	int size_x;
	int size_y;

protected:
	float Interpolate(float a, float b, float x);	
	//float Noise(int x, int y);
	void SmoothNoise(int oct, const vector<float>& base, vector<float>& res); // generate smooth noise for octave
	//float InterpolatedNoise(float x, float y);

public:
	NoiseGenerator() : size_x(0), size_y(0) {}	
	~NoiseGenerator() {}

	void Init(int sz_x, int sz_y);
	void GenPerlin(int oct_num, float persistence, bool circle = false); // gen perlin noise

	int SizeX() const {return size_x;}
	int SizeY() const {return size_y;}
};
