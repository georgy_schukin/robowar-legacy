#pragma once

#include <vector>

using namespace std;

namespace robo_vis
{
	struct RobotData
	{
		int x, y;
		int team;
		int hitpoints;
		int cooldown;
		int move_direction; // 0: stay, 1: E, 2: SE, 3: SW, 4: W, 5: NW, 6: NE
		bool shoot;
		int shoot_x, shoot_y;
	};

	struct BaseData
	{
		int x, y;
		int team; //-1 - no team
		int hitpoints;
		int cooldown;
	};
	
	struct Point
	{
		unsigned int x;
		unsigned int y;
		
		Point() : x(0), y(0) {}

		Point(unsigned int x, unsigned int y)
		{
			this->x = x;
			this->y = y;
		}
	};

	struct Rectangle
	{
		Point p1;
		Point p2;

		Rectangle(const Point& p1, const Point& p2)
		{
			this->p1 = p1;
			this->p2 = p2;
		}

		Rectangle(unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2)
		{
			p1 = Point(x1, y1);
			p2 = Point(x2, y2);
		}

		unsigned int width() const
		{
			return (p2.x - p1.x + 1);
		}

		unsigned int height() const
		{
			return (p2.y - p1.y + 1);
		}

		unsigned int square() const
		{
			return width()*height();
		}

		bool contains(Point p) const
		{
			if (p.x >= p1.x && p.y >= p1.y && p.x <= p2.x && p.y <= p2.y)
			{
				return true;
			}
			return false;
		}
	};

	struct MinimapPoint
	{
		std::vector<unsigned int> crowds;
	};
	
	class ISerializable
	{
		public:
			virtual size_t serialize(void *buf, size_t buf_size) const =0;
			virtual size_t requiredSerializationSize() const =0;
			virtual ~ISerializable() {}
	};

	class DeserializationException: public std::exception
	{
	};

	struct MinimapData: public ISerializable
	{
		int mini_width, mini_height;
		int map_width, map_height;
		std::vector<std::vector<MinimapPoint> > points;
		
		MinimapData() {}

		MinimapData(void *buf, size_t buf_size);// throw (DeserializationException);
		MinimapData(int mapWidth, int mapHeight, int minimapWidth, int minimapHeight, int teamAmount/*, const std::vector<RobotData>& robots, const std::vector<BaseData>& bases*/);
		
		MinimapData& merge(const MinimapData& other);

		MinimapData& operator+=(const MinimapData& other);

		size_t width() const;
		size_t height() const;

		virtual size_t serialize(void *buf, size_t buf_size) const;
		virtual size_t requiredSerializationSize() const;

		void Init(int teamAmount, const std::vector<RobotData>& robots, const std::vector<BaseData>& bases);
	};

	struct ScreenCoordinates: public ISerializable
	{
		int x;
		int y;
		
		ScreenCoordinates(void *buf, size_t buf_size);// throw (DeserializationException);

		virtual size_t serialize(void *buf, size_t buf_size) const;
		virtual size_t requiredSerializationSize() const;
	};

	class IVisualizerDrawerController
	{
		public:
			virtual void sendUpdate(const std::vector<RobotData>& robots, const std::vector<BaseData>& bases, const MinimapData& minimap, unsigned int iteration) =0;
			virtual ~IVisualizerDrawerController() {}
	};

	class VisualizerController
	{
		private:
			VisualizerController() {}
			int mapWidth;
			int mapHeight;
			int teamAmount;
		protected:
			int getMapWidth() const {return mapWidth;}
			int getMapHeight() const {return mapHeight;}
			int getTeamAmount() const {return teamAmount;}
			IVisualizerDrawerController* drawerController;
			static const int MINIMAP_WIDTH = 200;
			static const int MINIMAP_HEIGHT = 200;
		public:
			VisualizerController(int mapWidth, int mapHeight, int teamAmount) :	
			  mapWidth(mapWidth), mapHeight(mapHeight), teamAmount(teamAmount), drawerController(NULL) {}
			virtual void visualize(const std::vector<RobotData>& robots, const std::vector<BaseData>& bases)=0;
			virtual ~VisualizerController() {}
	};

	class IVisualizerDrawer
	{
		public:
			virtual void update(const std::vector<RobotData>& robots, const std::vector<BaseData>& bases, const MinimapData& minimap, unsigned int iteration)=0;
			virtual ~IVisualizerDrawer() {}
	};
	
	class IVisualizerDrawerHelper
	{
		public:
			virtual void coordsUpdated(const ScreenCoordinates& coords) =0;
			virtual ~IVisualizerDrawerHelper() {}
	};
	
	class IServerGateway
	{
		public:
			virtual void send(int id, void *data, size_t data_size)=0;
			virtual size_t receive(int id, void *buf, size_t buf_size)=0;
			virtual size_t probe(int id)=0;
			virtual ~IServerGateway() {}
	};

	class IClientGateway
	{
		public:
			virtual void send(void *data, size_t data_size)=0;
			virtual size_t receive(void *buf, size_t buf_size)=0;
			virtual size_t probe()=0;
			virtual ~IClientGateway() {}
	};
}

void update(const vector<robo_vis::RobotData>& rs, const vector<robo_vis::BaseData>& bs, const robo_vis::MinimapData& minimap_data, unsigned int iteration);