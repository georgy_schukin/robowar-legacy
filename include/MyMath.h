#pragma once

namespace mymath
{

const float PI = 3.1415926535897932384626433832795f;

/////////////////// Optimized Math Functions /////////////////////////
void Init(unsigned int table_sz = 1000); // init inner data

float Sin(float rad);
float Cos(float rad);

int Sign(float val);

}
