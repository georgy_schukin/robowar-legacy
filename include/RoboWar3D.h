#pragma once

#include "IVisualizer.h"

//void update(const std::vector<robo_vis::RobotData>& robots, const std::vector<robo_vis::BaseData>& bases, const robo_vis::MinimapData& minimap_data, unsigned int iteration);

/////////////// Config 3D /////////////////
class Config3D // config for visualizer
{
public:	
	int frames_per_anim; // num of frames per animation (between iterations)
	int refresh_delay; // delay between animation frames (msec)

	bool enable_sound; // play sounds
	bool enable_music; // play music

	int show_minimap; // show minimap mode
	int show_score; // score show mode

	bool show_info; // show info
	bool show_help; // show onscreen help

	bool show_particles; // show particle effects (smoke/fire)

	Config3D() {}
	~Config3D() {}
};